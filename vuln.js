(function(b, a) {
    var f = function(b, a, e) {
        var c;
        return function() {
            var d = this,
                g = arguments;
            c ? clearTimeout(c) : e && b.apply(d, g);
            c = setTimeout(function() {
                e || b.apply(d, g);
                c = null
            }, a || 500)
        }
    };
    jQuery.fn[a] = function(b) {
        return b ? this.bind("resize", f(b)) : this.trigger(a)
    }
})(jQuery, "smartresize");
(function(b, a) {
    var f, c;
    b.responsive = {
        mobileLayoutWidth: b.preferences.mobileLayoutWidth,
        initialized: !1,
        init: function() {
            this.initialized || (f = a("body"), c = a(".navigation"), c.find(".navigation_header").click(function() {
                a(this).toggleClass("expanded").siblings(".level_1_list").toggle()
            }), f.width() <= this.mobileLayoutWidth && b.responsive.enableMobileNav(), screen.width > this.mobileLayoutWidth - 1 && a(window).smartresize(function() {
                jQuery("body").width() <= b.responsive.mobileLayoutWidth ? b.responsive.enableMobileNav() :
                    b.responsive.disableMobileNav()
            }), this.initialized = !0)
        },
        enableMobileNav: function() {
            c && c.find(".level_1_list\x3eli\x3ea:not(.non_expandable)").click(function() {
                if (0 < jQuery(this).siblings().length && !jQuery(this).siblings().is(":visible")) return jQuery(this).append('\x3cspan class\x3d"subnavigation_close"\x3eclose\x3c/span\x3e').children("span").click(function() {
                    jQuery(this).parent().siblings().hide();
                    jQuery(this).parents(".level_1_list_item").toggleClass("expanded");
                    jQuery(this).remove();
                    return !1
                }).parent().siblings().show(),
                    jQuery(this).parent().toggleClass("expanded"), c.trigger("responsivesubmenu.opened", {
                    target: jQuery(this)
                }), !1
            })
        },
        disableMobileNav: function() {
            c && (c.find(".level_1_list").removeAttr("style"), c.find(".navigation_dropdown").removeAttr("style"), c.find(".subnavigation_close").remove())
        },
        toggleGridWideTileView: function() {
            0 == jQuery(".toggle_grid").length && (jQuery(".results_hits").prepend('\x3ca class\x3d"toggle_grid" href\x3d"' + location.href + '"\x3e# \x26equiv;\x3c/a\x3e'), jQuery(".toggle_grid").click(function() {
                jQuery(".search_result_content").toggleClass("wide_tiles");
                return !1
            }))
        }
    };
    a(document).ready(function() {
        b.responsive.init()
    })
})(window.app = window.app || {}, jQuery);
(function(b) {
    function a() {}
    var f = /xyz/.test(function() {
        xyz
    }) ? /\b_super\b/ : /.*/;
    a.extend = function(b) {
        var c = this.prototype,
            e = Object.create(c),
            g;
        for (g in b) e[g] = "function" === typeof b[g] && "function" == typeof c[g] && f.test(b[g]) ? function(b, a) {
            return function() {
                var e = this._super;
                this._super = c[b];
                var d = a.apply(this, arguments);
                this._super = e;
                return d
            }
        }(g, b[g]) : b[g];
        b = "function" === typeof e.init ? e.hasOwnProperty("init") ? e.init : function() {
            c.init.apply(this, arguments)
        } : function() {};
        b.prototype = e;
        e.constructor = b;
        b.extend = a.extend;
        return b
    };
    b.Class = a
})(this);
(function(b) {
    var a = Class.extend({
        init: function(b) {
            if (!b.element) throw Error("Element is mandatory");
            this.element = $(b.element);
            this.element.data("instance", this);
            this.selectors = {};
            this.state = {};
            this.defaultOptions = $.extend({}, {
                classNames: {}
            }, this.getOptions());
            this.options = $.extend({}, this.defaultOptions, b.options, this.element.data("component-options"));
            this.initCache();
            this.initState();
            this.bindEvents();
            this.afterInit()
        },
        getOptions: function() {
            return {}
        },
        initCache: function() {},
        initState: function() {},
        bindEvents: function() {},
        afterInit: function() {}
    });
    b.Component = a
})(window.app = window.app || {}, jQuery);
(function(b, a) {
    function f(a, e) {
        var d, g, h, f = a.nodeName.toLowerCase();
        return "area" === f ? (d = a.parentNode, g = d.name, a.href && g && "map" === d.nodeName.toLowerCase() ? (h = b("img[usemap\x3d#" + g + "]")[0], !!h && c(h)) : !1) : (/input|select|textarea|button|object/.test(f) ? !a.disabled : "a" === f ? a.href || e : e) && c(a)
    }

    function c(a) {
        return b.expr.filters.visible(a) && !b(a).parents().addBack().filter(function() {
            return "hidden" === b.css(this, "visibility")
        }).length
    }
    var d = 0,
        e = /^ui-id-\d+$/;
    b.ui = b.ui || {};
    b.extend(b.ui, {
        version: "1.10.3",
        keyCode: {
            BACKSPACE: 8,
            COMMA: 188,
            DELETE: 46,
            DOWN: 40,
            END: 35,
            ENTER: 13,
            ESCAPE: 27,
            HOME: 36,
            LEFT: 37,
            NUMPAD_ADD: 107,
            NUMPAD_DECIMAL: 110,
            NUMPAD_DIVIDE: 111,
            NUMPAD_ENTER: 108,
            NUMPAD_MULTIPLY: 106,
            NUMPAD_SUBTRACT: 109,
            PAGE_DOWN: 34,
            PAGE_UP: 33,
            PERIOD: 190,
            RIGHT: 39,
            SPACE: 32,
            TAB: 9,
            UP: 38
        }
    });
    b.fn.extend({
        focus: function(a) {
            return function(c, e) {
                return "number" == typeof c ? this.each(function() {
                    var a = this;
                    setTimeout(function() {
                        b(a).focus();
                        e && e.call(a)
                    }, c)
                }) : a.apply(this, arguments)
            }
        }(b.fn.focus),
        scrollParent: function() {
            var a;
            return a =
                b.ui.ie && /(static|relative)/.test(this.css("position")) || /absolute/.test(this.css("position")) ? this.parents().filter(function() {
                    return /(relative|absolute|fixed)/.test(b.css(this, "position")) && /(auto|scroll)/.test(b.css(this, "overflow") + b.css(this, "overflow-y") + b.css(this, "overflow-x"))
                }).eq(0) : this.parents().filter(function() {
                    return /(auto|scroll)/.test(b.css(this, "overflow") + b.css(this, "overflow-y") + b.css(this, "overflow-x"))
                }).eq(0), /fixed/.test(this.css("position")) || !a.length ? b(document) : a
        },
        zIndex: function(c) {
            if (c !==
                a) return this.css("zIndex", c);
            if (this.length) {
                var e, d;
                for (c = b(this[0]); c.length && c[0] !== document;) {
                    if (e = c.css("position"), ("absolute" === e || "relative" === e || "fixed" === e) && (d = parseInt(c.css("zIndex"), 10), !isNaN(d) && 0 !== d)) return d;
                    c = c.parent()
                }
            }
            return 0
        },
        uniqueId: function() {
            return this.each(function() {
                this.id || (this.id = "ui-id-" + ++d)
            })
        },
        removeUniqueId: function() {
            return this.each(function() {
                e.test(this.id) && b(this).removeAttr("id")
            })
        }
    });
    b.extend(b.expr[":"], {
        data: b.expr.createPseudo ? b.expr.createPseudo(function(a) {
            return function(c) {
                return !!b.data(c,
                    a)
            }
        }) : function(a, c, e) {
            return !!b.data(a, e[3])
        },
        focusable: function(a) {
            return f(a, !isNaN(b.attr(a, "tabindex")))
        },
        tabbable: function(a) {
            var c = b.attr(a, "tabindex"),
                e = isNaN(c);
            return (e || 0 <= c) && f(a, !e)
        }
    });
    b("\x3ca\x3e").outerWidth(1).jquery || b.each(["Width", "Height"], function(c, e) {
        function d(a, c, e, d) {
            return b.each(g, function() {
                c -= parseFloat(b.css(a, "padding" + this)) || 0;
                e && (c -= parseFloat(b.css(a, "border" + this + "Width")) || 0);
                d && (c -= parseFloat(b.css(a, "margin" + this)) || 0)
            }), c
        }
        var g = "Width" === e ? ["Left", "Right"] : ["Top", "Bottom"],
            h = e.toLowerCase(),
            f = {
                innerWidth: b.fn.innerWidth,
                innerHeight: b.fn.innerHeight,
                outerWidth: b.fn.outerWidth,
                outerHeight: b.fn.outerHeight
            };
        b.fn["inner" + e] = function(c) {
            return c === a ? f["inner" + e].call(this) : this.each(function() {
                b(this).css(h, d(this, c) + "px")
            })
        };
        b.fn["outer" + e] = function(a, c) {
            return "number" != typeof a ? f["outer" + e].call(this, a) : this.each(function() {
                b(this).css(h, d(this, a, !0, c) + "px")
            })
        }
    });
    b.fn.addBack || (b.fn.addBack = function(b) {
        return this.add(null == b ? this.prevObject : this.prevObject.filter(b))
    });
    b("\x3ca\x3e").data("a-b", "a").removeData("a-b").data("a-b") && (b.fn.removeData = function(a) {
        return function(c) {
            return arguments.length ? a.call(this, b.camelCase(c)) : a.call(this)
        }
    }(b.fn.removeData));
    b.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase());
    b.support.selectstart = "onselectstart" in document.createElement("div");
    b.fn.extend({
        disableSelection: function() {
            return this.bind((b.support.selectstart ? "selectstart" : "mousedown") + ".ui-disableSelection", function(b) {
                b.preventDefault()
            })
        },
        enableSelection: function() {
            return this.unbind(".ui-disableSelection")
        }
    });
    b.extend(b.ui, {
        plugin: {
            add: function(a, c, e) {
                var d;
                a = b.ui[a].prototype;
                for (d in e) a.plugins[d] = a.plugins[d] || [], a.plugins[d].push([c, e[d]])
            },
            call: function(a, b, c) {
                var e = a.plugins[b];
                if (e && a.element[0].parentNode && 11 !== a.element[0].parentNode.nodeType)
                    for (b = 0; e.length > b; b++) a.options[e[b][0]] && e[b][1].apply(a.element, c)
            }
        },
        hasScroll: function(a, c) {
            if ("hidden" === b(a).css("overflow")) return !1;
            c = c && "left" === c ? "scrollLeft" : "scrollTop";
            var e = !1;
            return 0 < a[c] ? !0 : (a[c] = 1, e = 0 < a[c], a[c] = 0, e)
        }
    })
})(jQuery);
(function(b, a) {
    var f = 0,
        c = Array.prototype.slice,
        d = b.cleanData;
    b.cleanData = function(a) {
        for (var c, e = 0; null != (c = a[e]); e++) try {
            b(c).triggerHandler("remove")
        } catch (k) {}
        d(a)
    };
    b.widget = function(c, d, h) {
        var e, g, f, n, t = {},
            r = c.split(".")[0];
        c = c.split(".")[1];
        e = r + "-" + c;
        h || (h = d, d = b.Widget);
        b.expr[":"][e.toLowerCase()] = function(a) {
            return !!b.data(a, e)
        };
        b[r] = b[r] || {};
        g = b[r][c];
        f = b[r][c] = function(b, c) {
            return this._createWidget ? (arguments.length && this._createWidget(b, c), a) : new f(b, c)
        };
        b.extend(f, g, {
            version: h.version,
            _proto: b.extend({}, h),
            _childConstructors: []
        });
        n = new d;
        n.options = b.widget.extend({}, n.options);
        b.each(h, function(c, e) {
            return b.isFunction(e) ? (t[c] = function() {
                var a = function() {
                        return d.prototype[c].apply(this, arguments)
                    },
                    b = function(a) {
                        return d.prototype[c].apply(this, a)
                    };
                return function() {
                    var c, d = this._super,
                        g = this._superApply;
                    return this._super = a, this._superApply = b, c = e.apply(this, arguments), this._super = d, this._superApply = g, c
                }
            }(), a) : (t[c] = e, a)
        });
        f.prototype = b.widget.extend(n, {
            widgetEventPrefix: g ? n.widgetEventPrefix : c
        }, t, {
            constructor: f,
            namespace: r,
            widgetName: c,
            widgetFullName: e
        });
        g ? (b.each(g._childConstructors, function(a, c) {
            a = c.prototype;
            b.widget(a.namespace + "." + a.widgetName, f, c._proto)
        }), delete g._childConstructors) : d._childConstructors.push(f);
        b.widget.bridge(c, f)
    };
    b.widget.extend = function(e) {
        for (var d, h, f = c.call(arguments, 1), l = 0, m = f.length; m > l; l++)
            for (d in f[l]) h = f[l][d], f[l].hasOwnProperty(d) && h !== a && (e[d] = b.isPlainObject(h) ? b.isPlainObject(e[d]) ? b.widget.extend({}, e[d], h) : b.widget.extend({}, h) : h);
        return e
    };
    b.widget.bridge = function(e, d) {
        var g = d.prototype.widgetFullName || e;
        b.fn[e] = function(h) {
            var f = "string" == typeof h,
                k = c.call(arguments, 1),
                n = this;
            return h = !f && k.length ? b.widget.extend.apply(null, [h].concat(k)) : h, f ? this.each(function() {
                var c, d = b.data(this, g);
                return d ? b.isFunction(d[h]) && "_" !== h.charAt(0) ? (c = d[h].apply(d, k), c !== d && c !== a ? (n = c && c.jquery ? n.pushStack(c.get()) : c, !1) : a) : b.error("no such method '" + h + "' for " + e + " widget instance") : b.error("cannot call methods on " + e + " prior to initialization; attempted to call method '" +
                    h + "'")
            }) : this.each(function() {
                var a = b.data(this, g);
                a ? a.option(h || {})._init() : b.data(this, g, new d(h, this))
            }), n
        }
    };
    b.Widget = function() {};
    b.Widget._childConstructors = [];
    b.Widget.prototype = {
        widgetName: "widget",
        widgetEventPrefix: "",
        defaultElement: "\x3cdiv\x3e",
        options: {
            disabled: !1,
            create: null
        },
        _createWidget: function(a, c) {
            c = b(c || this.defaultElement || this)[0];
            this.element = b(c);
            this.uuid = f++;
            this.eventNamespace = "." + this.widgetName + this.uuid;
            this.options = b.widget.extend({}, this.options, this._getCreateOptions(),
                a);
            this.bindings = b();
            this.hoverable = b();
            this.focusable = b();
            c !== this && (b.data(c, this.widgetFullName, this), this._on(!0, this.element, {
                remove: function(a) {
                    a.target === c && this.destroy()
                }
            }), this.document = b(c.style ? c.ownerDocument : c.document || c), this.window = b(this.document[0].defaultView || this.document[0].parentWindow));
            this._create();
            this._trigger("create", null, this._getCreateEventData());
            this._init()
        },
        _getCreateOptions: b.noop,
        _getCreateEventData: b.noop,
        _create: b.noop,
        _init: b.noop,
        destroy: function() {
            this._destroy();
            this.element.unbind(this.eventNamespace).removeData(this.widgetName).removeData(this.widgetFullName).removeData(b.camelCase(this.widgetFullName));
            this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled ui-state-disabled");
            this.bindings.unbind(this.eventNamespace);
            this.hoverable.removeClass("ui-state-hover");
            this.focusable.removeClass("ui-state-focus")
        },
        _destroy: b.noop,
        widget: function() {
            return this.element
        },
        option: function(c, d) {
            var e, g, f, m = c;
            if (0 === arguments.length) return b.widget.extend({}, this.options);
            if ("string" == typeof c)
                if (m = {}, e = c.split("."), c = e.shift(), e.length) {
                    g = m[c] = b.widget.extend({}, this.options[c]);
                    for (f = 0; e.length - 1 > f; f++) g[e[f]] = g[e[f]] || {}, g = g[e[f]];
                    if (c = e.pop(), d === a) return g[c] === a ? null : g[c];
                    g[c] = d
                } else {
                    if (d === a) return this.options[c] === a ? null : this.options[c];
                    m[c] = d
                }
            return this._setOptions(m), this
        },
        _setOptions: function(a) {
            for (var b in a) this._setOption(b, a[b]);
            return this
        },
        _setOption: function(a, b) {
            return this.options[a] =
                b, "disabled" === a && (this.widget().toggleClass(this.widgetFullName + "-disabled ui-state-disabled", !!b).attr("aria-disabled", b), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")), this
        },
        enable: function() {
            return this._setOption("disabled", !1)
        },
        disable: function() {
            return this._setOption("disabled", !0)
        },
        _on: function(c, d, h) {
            var e, g = this;
            "boolean" != typeof c && (h = d, d = c, c = !1);
            h ? (d = e = b(d), this.bindings = this.bindings.add(d)) : (h = d, d = this.element, e = this.widget());
            b.each(h, function(h,
                               f) {
                function k() {
                    return c || !0 !== g.options.disabled && !b(this).hasClass("ui-state-disabled") ? ("string" == typeof f ? g[f] : f).apply(g, arguments) : a
                }
                "string" != typeof f && (k.guid = f.guid = f.guid || k.guid || b.guid++);
                var l = h.match(/^(\w+)\s*(.*)$/);
                h = l[1] + g.eventNamespace;
                (l = l[2]) ? e.delegate(l, h, k): d.bind(h, k)
            })
        },
        _off: function(a, b) {
            b = (b || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace;
            a.unbind(b).undelegate(b)
        },
        _delay: function(a, b) {
            var c = this;
            return setTimeout(function() {
                return ("string" == typeof a ?
                    c[a] : a).apply(c, arguments)
            }, b || 0)
        },
        _hoverable: function(a) {
            this.hoverable = this.hoverable.add(a);
            this._on(a, {
                mouseenter: function(a) {
                    b(a.currentTarget).addClass("ui-state-hover")
                },
                mouseleave: function(a) {
                    b(a.currentTarget).removeClass("ui-state-hover")
                }
            })
        },
        _focusable: function(a) {
            this.focusable = this.focusable.add(a);
            this._on(a, {
                focusin: function(a) {
                    b(a.currentTarget).addClass("ui-state-focus")
                },
                focusout: function(a) {
                    b(a.currentTarget).removeClass("ui-state-focus")
                }
            })
        },
        _trigger: function(a, c, d) {
            var e, g = this.options[a];
            if (d = d || {}, c = b.Event(c), c.type = (a === this.widgetEventPrefix ? a : this.widgetEventPrefix + a).toLowerCase(), c.target = this.element[0], a = c.originalEvent)
                for (e in a) e in c || (c[e] = a[e]);
            return this.element.trigger(c, d), !(b.isFunction(g) && !1 === g.apply(this.element[0], [c].concat(d)) || c.isDefaultPrevented())
        }
    };
    b.each({
        show: "fadeIn",
        hide: "fadeOut"
    }, function(a, c) {
        b.Widget.prototype["_" + a] = function(e, d, g) {
            "string" == typeof d && (d = {
                effect: d
            });
            var h, f = d ? !0 === d || "number" == typeof d ? c : d.effect || c : a;
            d = d || {};
            "number" == typeof d &&
            (d = {
                duration: d
            });
            h = !b.isEmptyObject(d);
            d.complete = g;
            d.delay && e.delay(d.delay);
            h && b.effects && b.effects.effect[f] ? e[a](d) : f !== a && e[f] ? e[f](d.duration, d.easing, g) : e.queue(function(c) {
                b(this)[a]();
                g && g.call(e[0]);
                c()
            })
        }
    })
})(jQuery);
(function(b) {
    var a = !1;
    b(document).mouseup(function() {
        a = !1
    });
    b.widget("ui.mouse", {
        version: "1.10.3",
        options: {
            cancel: "input,textarea,button,select,option",
            distance: 1,
            delay: 0
        },
        _mouseInit: function() {
            var a = this;
            this.element.bind("mousedown." + this.widgetName, function(b) {
                return a._mouseDown(b)
            }).bind("click." + this.widgetName, function(c) {
                return !0 === b.data(c.target, a.widgetName + ".preventClickEvent") ? (b.removeData(c.target, a.widgetName + ".preventClickEvent"), c.stopImmediatePropagation(), !1) : void 0
            });
            this.started = !1
        },
        _mouseDestroy: function() {
            this.element.unbind("." + this.widgetName);
            this._mouseMoveDelegate && b(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate)
        },
        _mouseDown: function(f) {
            if (!a) {
                this._mouseStarted && this._mouseUp(f);
                this._mouseDownEvent = f;
                var c = this,
                    d = 1 === f.which,
                    e = "string" == typeof this.options.cancel && f.target.nodeName ? b(f.target).closest(this.options.cancel).length : !1;
                return d && !e && this._mouseCapture(f) ? (this.mouseDelayMet = !this.options.delay, this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function() {
                    c.mouseDelayMet = !0
                }, this.options.delay)), this._mouseDistanceMet(f) && this._mouseDelayMet(f) && (this._mouseStarted = !1 !== this._mouseStart(f), !this._mouseStarted) ? (f.preventDefault(), !0) : (!0 === b.data(f.target, this.widgetName + ".preventClickEvent") && b.removeData(f.target, this.widgetName + ".preventClickEvent"), this._mouseMoveDelegate = function(a) {
                    return c._mouseMove(a)
                }, this._mouseUpDelegate = function(a) {
                    return c._mouseUp(a)
                },
                    b(document).bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate), f.preventDefault(), a = !0, !0)) : !0
            }
        },
        _mouseMove: function(a) {
            return b.ui.ie && (!document.documentMode || 9 > document.documentMode) && !a.button ? this._mouseUp(a) : this._mouseStarted ? (this._mouseDrag(a), a.preventDefault()) : (this._mouseDistanceMet(a) && this._mouseDelayMet(a) && (this._mouseStarted = !1 !== this._mouseStart(this._mouseDownEvent, a), this._mouseStarted ? this._mouseDrag(a) : this._mouseUp(a)), !this._mouseStarted)
        },
        _mouseUp: function(a) {
            return b(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate), this._mouseStarted && (this._mouseStarted = !1, a.target === this._mouseDownEvent.target && b.data(a.target, this.widgetName + ".preventClickEvent", !0), this._mouseStop(a)), !1
        },
        _mouseDistanceMet: function(a) {
            return Math.max(Math.abs(this._mouseDownEvent.pageX - a.pageX), Math.abs(this._mouseDownEvent.pageY - a.pageY)) >= this.options.distance
        },
        _mouseDelayMet: function() {
            return this.mouseDelayMet
        },
        _mouseStart: function() {},
        _mouseDrag: function() {},
        _mouseStop: function() {},
        _mouseCapture: function() {
            return !0
        }
    })
})(jQuery);
(function(b) {
    b.widget("ui.draggable", b.ui.mouse, {
        version: "1.10.3",
        widgetEventPrefix: "drag",
        options: {
            addClasses: !0,
            appendTo: "parent",
            axis: !1,
            connectToSortable: !1,
            containment: !1,
            cursor: "auto",
            cursorAt: !1,
            grid: !1,
            handle: !1,
            helper: "original",
            iframeFix: !1,
            opacity: !1,
            refreshPositions: !1,
            revert: !1,
            revertDuration: 500,
            scope: "default",
            scroll: !0,
            scrollSensitivity: 20,
            scrollSpeed: 20,
            snap: !1,
            snapMode: "both",
            snapTolerance: 20,
            stack: !1,
            zIndex: !1,
            drag: null,
            start: null,
            stop: null
        },
        _create: function() {
            "original" !== this.options.helper ||
            /^(?:r|a|f)/.test(this.element.css("position")) || (this.element[0].style.position = "relative");
            this.options.addClasses && this.element.addClass("ui-draggable");
            this.options.disabled && this.element.addClass("ui-draggable-disabled");
            this._mouseInit()
        },
        _destroy: function() {
            this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled");
            this._mouseDestroy()
        },
        _mouseCapture: function(a) {
            var f = this.options;
            return this.helper || f.disabled || 0 < b(a.target).closest(".ui-resizable-handle").length ?
                !1 : (this.handle = this._getHandle(a), this.handle ? (b(!0 === f.iframeFix ? "iframe" : f.iframeFix).each(function() {
                    b("\x3cdiv class\x3d'ui-draggable-iframeFix' style\x3d'background: #fff;'\x3e\x3c/div\x3e").css({
                        width: this.offsetWidth + "px",
                        height: this.offsetHeight + "px",
                        position: "absolute",
                        opacity: "0.001",
                        zIndex: 1E3
                    }).css(b(this).offset()).appendTo("body")
                }), !0) : !1)
        },
        _mouseStart: function(a) {
            var f = this.options;
            return this.helper = this._createHelper(a), this.helper.addClass("ui-draggable-dragging"), this._cacheHelperProportions(),
            b.ui.ddmanager && (b.ui.ddmanager.current = this), this._cacheMargins(), this.cssPosition = this.helper.css("position"), this.scrollParent = this.helper.scrollParent(), this.offsetParent = this.helper.offsetParent(), this.offsetParentCssPosition = this.offsetParent.css("position"), this.offset = this.positionAbs = this.element.offset(), this.offset = {
                top: this.offset.top - this.margins.top,
                left: this.offset.left - this.margins.left
            }, this.offset.scroll = !1, b.extend(this.offset, {
                click: {
                    left: a.pageX - this.offset.left,
                    top: a.pageY - this.offset.top
                },
                parent: this._getParentOffset(),
                relative: this._getRelativeOffset()
            }), this.originalPosition = this.position = this._generatePosition(a), this.originalPageX = a.pageX, this.originalPageY = a.pageY, f.cursorAt && this._adjustOffsetFromHelper(f.cursorAt), this._setContainment(), !1 === this._trigger("start", a) ? (this._clear(), !1) : (this._cacheHelperProportions(), b.ui.ddmanager && !f.dropBehaviour && b.ui.ddmanager.prepareOffsets(this, a), this._mouseDrag(a, !0), b.ui.ddmanager && b.ui.ddmanager.dragStart(this, a), !0)
        },
        _mouseDrag: function(a,
                             f) {
            if ("fixed" === this.offsetParentCssPosition && (this.offset.parent = this._getParentOffset()), this.position = this._generatePosition(a), this.positionAbs = this._convertPositionTo("absolute"), !f) {
                f = this._uiHash();
                if (!1 === this._trigger("drag", a, f)) return this._mouseUp({}), !1;
                this.position = f.position
            }
            return this.options.axis && "y" === this.options.axis || (this.helper[0].style.left = this.position.left + "px"), this.options.axis && "x" === this.options.axis || (this.helper[0].style.top = this.position.top + "px"), b.ui.ddmanager &&
            b.ui.ddmanager.drag(this, a), !1
        },
        _mouseStop: function(a) {
            var f = this,
                c = !1;
            return b.ui.ddmanager && !this.options.dropBehaviour && (c = b.ui.ddmanager.drop(this, a)), this.dropped && (c = this.dropped, this.dropped = !1), "original" !== this.options.helper || b.contains(this.element[0].ownerDocument, this.element[0]) ? ("invalid" === this.options.revert && !c || "valid" === this.options.revert && c || !0 === this.options.revert || b.isFunction(this.options.revert) && this.options.revert.call(this.element, c) ? b(this.helper).animate(this.originalPosition,
                parseInt(this.options.revertDuration, 10),
                function() {
                    !1 !== f._trigger("stop", a) && f._clear()
                }) : !1 !== this._trigger("stop", a) && this._clear(), !1) : !1
        },
        _mouseUp: function(a) {
            return b("div.ui-draggable-iframeFix").each(function() {
                this.parentNode.removeChild(this)
            }), b.ui.ddmanager && b.ui.ddmanager.dragStop(this, a), b.ui.mouse.prototype._mouseUp.call(this, a)
        },
        cancel: function() {
            return this.helper.is(".ui-draggable-dragging") ? this._mouseUp({}) : this._clear(), this
        },
        _getHandle: function(a) {
            return this.options.handle ?
                !!b(a.target).closest(this.element.find(this.options.handle)).length : !0
        },
        _createHelper: function(a) {
            var f = this.options;
            a = b.isFunction(f.helper) ? b(f.helper.apply(this.element[0], [a])) : "clone" === f.helper ? this.element.clone().removeAttr("id") : this.element;
            return a.parents("body").length || a.appendTo("parent" === f.appendTo ? this.element[0].parentNode : f.appendTo), a[0] === this.element[0] || /(fixed|absolute)/.test(a.css("position")) || a.css("position", "absolute"), a
        },
        _adjustOffsetFromHelper: function(a) {
            "string" ==
            typeof a && (a = a.split(" "));
            b.isArray(a) && (a = {
                left: +a[0],
                top: +a[1] || 0
            });
            "left" in a && (this.offset.click.left = a.left + this.margins.left);
            "right" in a && (this.offset.click.left = this.helperProportions.width - a.right + this.margins.left);
            "top" in a && (this.offset.click.top = a.top + this.margins.top);
            "bottom" in a && (this.offset.click.top = this.helperProportions.height - a.bottom + this.margins.top)
        },
        _getParentOffset: function() {
            var a = this.offsetParent.offset();
            return "absolute" === this.cssPosition && this.scrollParent[0] !== document &&
            b.contains(this.scrollParent[0], this.offsetParent[0]) && (a.left += this.scrollParent.scrollLeft(), a.top += this.scrollParent.scrollTop()), (this.offsetParent[0] === document.body || this.offsetParent[0].tagName && "html" === this.offsetParent[0].tagName.toLowerCase() && b.ui.ie) && (a = {
                top: 0,
                left: 0
            }), {
                top: a.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                left: a.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
            }
        },
        _getRelativeOffset: function() {
            if ("relative" === this.cssPosition) {
                var a = this.element.position();
                return {
                    top: a.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
                    left: a.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
                }
            }
            return {
                top: 0,
                left: 0
            }
        },
        _cacheMargins: function() {
            this.margins = {
                left: parseInt(this.element.css("marginLeft"), 10) || 0,
                top: parseInt(this.element.css("marginTop"), 10) || 0,
                right: parseInt(this.element.css("marginRight"), 10) || 0,
                bottom: parseInt(this.element.css("marginBottom"), 10) || 0
            }
        },
        _cacheHelperProportions: function() {
            this.helperProportions = {
                width: this.helper.outerWidth(),
                height: this.helper.outerHeight()
            }
        },
        _setContainment: function() {
            var a, f, c, d = this.options;
            return d.containment ? "window" === d.containment ? (this.containment = [b(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, b(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, b(window).scrollLeft() + b(window).width() - this.helperProportions.width - this.margins.left, b(window).scrollTop() + (b(window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height -
            this.margins.top
            ], void 0) : "document" === d.containment ? (this.containment = [0, 0, b(document).width() - this.helperProportions.width - this.margins.left, (b(document).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top], void 0) : d.containment.constructor === Array ? (this.containment = d.containment, void 0) : ("parent" === d.containment && (d.containment = this.helper[0].parentNode), f = b(d.containment), c = f[0], c && (a = "hidden" !== f.css("overflow"), this.containment = [(parseInt(f.css("borderLeftWidth"),
                10) || 0) + (parseInt(f.css("paddingLeft"), 10) || 0), (parseInt(f.css("borderTopWidth"), 10) || 0) + (parseInt(f.css("paddingTop"), 10) || 0), (a ? Math.max(c.scrollWidth, c.offsetWidth) : c.offsetWidth) - (parseInt(f.css("borderRightWidth"), 10) || 0) - (parseInt(f.css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (a ? Math.max(c.scrollHeight, c.offsetHeight) : c.offsetHeight) - (parseInt(f.css("borderBottomWidth"), 10) || 0) - (parseInt(f.css("paddingBottom"), 10) || 0) - this.helperProportions.height -
            this.margins.top - this.margins.bottom
            ], this.relative_container = f), void 0) : (this.containment = null, void 0)
        },
        _convertPositionTo: function(a, f) {
            f || (f = this.position);
            a = "absolute" === a ? 1 : -1;
            var c = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && b.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent;
            return this.offset.scroll || (this.offset.scroll = {
                top: c.scrollTop(),
                left: c.scrollLeft()
            }), {
                top: f.top + this.offset.relative.top * a + this.offset.parent.top * a - ("fixed" ===
                this.cssPosition ? -this.scrollParent.scrollTop() : this.offset.scroll.top) * a,
                left: f.left + this.offset.relative.left * a + this.offset.parent.left * a - ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : this.offset.scroll.left) * a
            }
        },
        _generatePosition: function(a) {
            var f, c, d, e, g = this.options,
                h = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && b.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
                k = a.pageX,
                l = a.pageY;
            return this.offset.scroll || (this.offset.scroll = {
                top: h.scrollTop(),
                left: h.scrollLeft()
            }), this.originalPosition && (this.containment && (this.relative_container ? (c = this.relative_container.offset(), f = [this.containment[0] + c.left, this.containment[1] + c.top, this.containment[2] + c.left, this.containment[3] + c.top]) : f = this.containment, a.pageX - this.offset.click.left < f[0] && (k = f[0] + this.offset.click.left), a.pageY - this.offset.click.top < f[1] && (l = f[1] + this.offset.click.top), a.pageX - this.offset.click.left > f[2] && (k = f[2] + this.offset.click.left), a.pageY - this.offset.click.top >
            f[3] && (l = f[3] + this.offset.click.top)), g.grid && (d = g.grid[1] ? this.originalPageY + Math.round((l - this.originalPageY) / g.grid[1]) * g.grid[1] : this.originalPageY, l = f ? d - this.offset.click.top >= f[1] || d - this.offset.click.top > f[3] ? d : d - this.offset.click.top >= f[1] ? d - g.grid[1] : d + g.grid[1] : d, e = g.grid[0] ? this.originalPageX + Math.round((k - this.originalPageX) / g.grid[0]) * g.grid[0] : this.originalPageX, k = f ? e - this.offset.click.left >= f[0] || e - this.offset.click.left > f[2] ? e : e - this.offset.click.left >= f[0] ? e - g.grid[0] : e + g.grid[0] :
                e)), {
                top: l - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : this.offset.scroll.top),
                left: k - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : this.offset.scroll.left)
            }
        },
        _clear: function() {
            this.helper.removeClass("ui-draggable-dragging");
            this.helper[0] === this.element[0] || this.cancelHelperRemoval || this.helper.remove();
            this.helper = null;
            this.cancelHelperRemoval = !1
        },
        _trigger: function(a, f, c) {
            return c = c || this._uiHash(), b.ui.plugin.call(this, a, [f, c]), "drag" === a && (this.positionAbs = this._convertPositionTo("absolute")), b.Widget.prototype._trigger.call(this, a, f, c)
        },
        plugins: {},
        _uiHash: function() {
            return {
                helper: this.helper,
                position: this.position,
                originalPosition: this.originalPosition,
                offset: this.positionAbs
            }
        }
    });
    b.ui.plugin.add("draggable", "connectToSortable", {
        start: function(a, f) {
            var c = b(this).data("ui-draggable"),
                d = c.options,
                e = b.extend({}, f, {
                    item: c.element
                });
            c.sortables = [];
            b(d.connectToSortable).each(function() {
                var d = b.data(this, "ui-sortable");
                d && !d.options.disabled && (c.sortables.push({
                    instance: d,
                    shouldRevert: d.options.revert
                }), d.refreshPositions(), d._trigger("activate", a, e))
            })
        },
        stop: function(a, f) {
            var c = b(this).data("ui-draggable"),
                d = b.extend({}, f, {
                    item: c.element
                });
            b.each(c.sortables, function() {
                this.instance.isOver ? (this.instance.isOver = 0, c.cancelHelperRemoval = !0, this.instance.cancelHelperRemoval = !1, this.shouldRevert && (this.instance.options.revert = this.shouldRevert),
                    this.instance._mouseStop(a), this.instance.options.helper = this.instance.options._helper, "original" === c.options.helper && this.instance.currentItem.css({
                    top: "auto",
                    left: "auto"
                })) : (this.instance.cancelHelperRemoval = !1, this.instance._trigger("deactivate", a, d))
            })
        },
        drag: function(a, f) {
            var c = b(this).data("ui-draggable"),
                d = this;
            b.each(c.sortables, function() {
                var e = !1,
                    g = this;
                this.instance.positionAbs = c.positionAbs;
                this.instance.helperProportions = c.helperProportions;
                this.instance.offset.click = c.offset.click;
                this.instance._intersectsWith(this.instance.containerCache) &&
                (e = !0, b.each(c.sortables, function() {
                    return this.instance.positionAbs = c.positionAbs, this.instance.helperProportions = c.helperProportions, this.instance.offset.click = c.offset.click, this !== g && this.instance._intersectsWith(this.instance.containerCache) && b.contains(g.instance.element[0], this.instance.element[0]) && (e = !1), e
                }));
                e ? (this.instance.isOver || (this.instance.isOver = 1, this.instance.currentItem = b(d).clone().removeAttr("id").appendTo(this.instance.element).data("ui-sortable-item", !0), this.instance.options._helper =
                    this.instance.options.helper, this.instance.options.helper = function() {
                    return f.helper[0]
                }, a.target = this.instance.currentItem[0], this.instance._mouseCapture(a, !0), this.instance._mouseStart(a, !0, !0), this.instance.offset.click.top = c.offset.click.top, this.instance.offset.click.left = c.offset.click.left, this.instance.offset.parent.left -= c.offset.parent.left - this.instance.offset.parent.left, this.instance.offset.parent.top -= c.offset.parent.top - this.instance.offset.parent.top, c._trigger("toSortable", a), c.dropped =
                    this.instance.element, c.currentItem = c.element, this.instance.fromOutside = c), this.instance.currentItem && this.instance._mouseDrag(a)) : this.instance.isOver && (this.instance.isOver = 0, this.instance.cancelHelperRemoval = !0, this.instance.options.revert = !1, this.instance._trigger("out", a, this.instance._uiHash(this.instance)), this.instance._mouseStop(a, !0), this.instance.options.helper = this.instance.options._helper, this.instance.currentItem.remove(), this.instance.placeholder && this.instance.placeholder.remove(),
                    c._trigger("fromSortable", a), c.dropped = !1)
            })
        }
    });
    b.ui.plugin.add("draggable", "cursor", {
        start: function() {
            var a = b("body"),
                f = b(this).data("ui-draggable").options;
            a.css("cursor") && (f._cursor = a.css("cursor"));
            a.css("cursor", f.cursor)
        },
        stop: function() {
            var a = b(this).data("ui-draggable").options;
            a._cursor && b("body").css("cursor", a._cursor)
        }
    });
    b.ui.plugin.add("draggable", "opacity", {
        start: function(a, f) {
            a = b(f.helper);
            f = b(this).data("ui-draggable").options;
            a.css("opacity") && (f._opacity = a.css("opacity"));
            a.css("opacity",
                f.opacity)
        },
        stop: function(a, f) {
            a = b(this).data("ui-draggable").options;
            a._opacity && b(f.helper).css("opacity", a._opacity)
        }
    });
    b.ui.plugin.add("draggable", "scroll", {
        start: function() {
            var a = b(this).data("ui-draggable");
            a.scrollParent[0] !== document && "HTML" !== a.scrollParent[0].tagName && (a.overflowOffset = a.scrollParent.offset())
        },
        drag: function(a) {
            var f = b(this).data("ui-draggable"),
                c = f.options,
                d = !1;
            f.scrollParent[0] !== document && "HTML" !== f.scrollParent[0].tagName ? (c.axis && "x" === c.axis || (f.overflowOffset.top +
            f.scrollParent[0].offsetHeight - a.pageY < c.scrollSensitivity ? f.scrollParent[0].scrollTop = d = f.scrollParent[0].scrollTop + c.scrollSpeed : a.pageY - f.overflowOffset.top < c.scrollSensitivity && (f.scrollParent[0].scrollTop = d = f.scrollParent[0].scrollTop - c.scrollSpeed)), c.axis && "y" === c.axis || (f.overflowOffset.left + f.scrollParent[0].offsetWidth - a.pageX < c.scrollSensitivity ? f.scrollParent[0].scrollLeft = d = f.scrollParent[0].scrollLeft + c.scrollSpeed : a.pageX - f.overflowOffset.left < c.scrollSensitivity && (f.scrollParent[0].scrollLeft =
                d = f.scrollParent[0].scrollLeft - c.scrollSpeed))) : (c.axis && "x" === c.axis || (a.pageY - b(document).scrollTop() < c.scrollSensitivity ? d = b(document).scrollTop(b(document).scrollTop() - c.scrollSpeed) : b(window).height() - (a.pageY - b(document).scrollTop()) < c.scrollSensitivity && (d = b(document).scrollTop(b(document).scrollTop() + c.scrollSpeed))), c.axis && "y" === c.axis || (a.pageX - b(document).scrollLeft() < c.scrollSensitivity ? d = b(document).scrollLeft(b(document).scrollLeft() - c.scrollSpeed) : b(window).width() - (a.pageX - b(document).scrollLeft()) <
                c.scrollSensitivity && (d = b(document).scrollLeft(b(document).scrollLeft() + c.scrollSpeed))));
            !1 !== d && b.ui.ddmanager && !c.dropBehaviour && b.ui.ddmanager.prepareOffsets(f, a)
        }
    });
    b.ui.plugin.add("draggable", "snap", {
        start: function() {
            var a = b(this).data("ui-draggable"),
                f = a.options;
            a.snapElements = [];
            b(f.snap.constructor !== String ? f.snap.items || ":data(ui-draggable)" : f.snap).each(function() {
                var c = b(this),
                    d = c.offset();
                this !== a.element[0] && a.snapElements.push({
                    item: this,
                    width: c.outerWidth(),
                    height: c.outerHeight(),
                    top: d.top,
                    left: d.left
                })
            })
        },
        drag: function(a, f) {
            var c, d, e, g, h, k, l, m, n, t, r = b(this).data("ui-draggable"),
                p = r.options,
                y = p.snapTolerance,
                w = f.offset.left,
                u = w + r.helperProportions.width,
                x = f.offset.top,
                B = x + r.helperProportions.height;
            for (n = r.snapElements.length - 1; 0 <= n; n--) h = r.snapElements[n].left, k = h + r.snapElements[n].width, l = r.snapElements[n].top, m = l + r.snapElements[n].height, h - y > u || w > k + y || l - y > B || x > m + y || !b.contains(r.snapElements[n].item.ownerDocument, r.snapElements[n].item) ? (r.snapElements[n].snapping && r.options.snap.release &&
            r.options.snap.release.call(r.element, a, b.extend(r._uiHash(), {
                snapItem: r.snapElements[n].item
            })), r.snapElements[n].snapping = !1) : ("inner" !== p.snapMode && (c = y >= Math.abs(l - B), d = y >= Math.abs(m - x), e = y >= Math.abs(h - u), g = y >= Math.abs(k - w), c && (f.position.top = r._convertPositionTo("relative", {
                top: l - r.helperProportions.height,
                left: 0
            }).top - r.margins.top), d && (f.position.top = r._convertPositionTo("relative", {
                top: m,
                left: 0
            }).top - r.margins.top), e && (f.position.left = r._convertPositionTo("relative", {
                    top: 0,
                    left: h - r.helperProportions.width
                }).left -
                r.margins.left), g && (f.position.left = r._convertPositionTo("relative", {
                top: 0,
                left: k
            }).left - r.margins.left)), t = c || d || e || g, "outer" !== p.snapMode && (c = y >= Math.abs(l - x), d = y >= Math.abs(m - B), e = y >= Math.abs(h - w), g = y >= Math.abs(k - u), c && (f.position.top = r._convertPositionTo("relative", {
                top: l,
                left: 0
            }).top - r.margins.top), d && (f.position.top = r._convertPositionTo("relative", {
                top: m - r.helperProportions.height,
                left: 0
            }).top - r.margins.top), e && (f.position.left = r._convertPositionTo("relative", {
                top: 0,
                left: h
            }).left - r.margins.left),
            g && (f.position.left = r._convertPositionTo("relative", {
                top: 0,
                left: k - r.helperProportions.width
            }).left - r.margins.left)), !r.snapElements[n].snapping && (c || d || e || g || t) && r.options.snap.snap && r.options.snap.snap.call(r.element, a, b.extend(r._uiHash(), {
                snapItem: r.snapElements[n].item
            })), r.snapElements[n].snapping = c || d || e || g || t)
        }
    });
    b.ui.plugin.add("draggable", "stack", {
        start: function() {
            var a, f = this.data("ui-draggable").options,
                f = b.makeArray(b(f.stack)).sort(function(a, d) {
                    return (parseInt(b(a).css("zIndex"), 10) ||
                        0) - (parseInt(b(d).css("zIndex"), 10) || 0)
                });
            f.length && (a = parseInt(b(f[0]).css("zIndex"), 10) || 0, b(f).each(function(c) {
                b(this).css("zIndex", a + c)
            }), this.css("zIndex", a + f.length))
        }
    });
    b.ui.plugin.add("draggable", "zIndex", {
        start: function(a, f) {
            a = b(f.helper);
            f = b(this).data("ui-draggable").options;
            a.css("zIndex") && (f._zIndex = a.css("zIndex"));
            a.css("zIndex", f.zIndex)
        },
        stop: function(a, f) {
            a = b(this).data("ui-draggable").options;
            a._zIndex && b(f.helper).css("zIndex", a._zIndex)
        }
    })
})(jQuery);
(function(b) {
    b.widget("ui.droppable", {
        version: "1.10.3",
        widgetEventPrefix: "drop",
        options: {
            accept: "*",
            activeClass: !1,
            addClasses: !0,
            greedy: !1,
            hoverClass: !1,
            scope: "default",
            tolerance: "intersect",
            activate: null,
            deactivate: null,
            drop: null,
            out: null,
            over: null
        },
        _create: function() {
            var a = this.options,
                f = a.accept;
            this.isover = !1;
            this.isout = !0;
            this.accept = b.isFunction(f) ? f : function(a) {
                return a.is(f)
            };
            this.proportions = {
                width: this.element[0].offsetWidth,
                height: this.element[0].offsetHeight
            };
            b.ui.ddmanager.droppables[a.scope] =
                b.ui.ddmanager.droppables[a.scope] || [];
            b.ui.ddmanager.droppables[a.scope].push(this);
            a.addClasses && this.element.addClass("ui-droppable")
        },
        _destroy: function() {
            for (var a = 0, f = b.ui.ddmanager.droppables[this.options.scope]; f.length > a; a++) f[a] === this && f.splice(a, 1);
            this.element.removeClass("ui-droppable ui-droppable-disabled")
        },
        _setOption: function(a, f) {
            "accept" === a && (this.accept = b.isFunction(f) ? f : function(a) {
                return a.is(f)
            });
            b.Widget.prototype._setOption.apply(this, arguments)
        },
        _activate: function(a) {
            var f =
                b.ui.ddmanager.current;
            this.options.activeClass && this.element.addClass(this.options.activeClass);
            f && this._trigger("activate", a, this.ui(f))
        },
        _deactivate: function(a) {
            var f = b.ui.ddmanager.current;
            this.options.activeClass && this.element.removeClass(this.options.activeClass);
            f && this._trigger("deactivate", a, this.ui(f))
        },
        _over: function(a) {
            var f = b.ui.ddmanager.current;
            f && (f.currentItem || f.element)[0] !== this.element[0] && this.accept.call(this.element[0], f.currentItem || f.element) && (this.options.hoverClass && this.element.addClass(this.options.hoverClass),
                this._trigger("over", a, this.ui(f)))
        },
        _out: function(a) {
            var f = b.ui.ddmanager.current;
            f && (f.currentItem || f.element)[0] !== this.element[0] && this.accept.call(this.element[0], f.currentItem || f.element) && (this.options.hoverClass && this.element.removeClass(this.options.hoverClass), this._trigger("out", a, this.ui(f)))
        },
        _drop: function(a, f) {
            var c = f || b.ui.ddmanager.current,
                d = !1;
            return c && (c.currentItem || c.element)[0] !== this.element[0] ? (this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function() {
                var a =
                    b.data(this, "ui-droppable");
                return a.options.greedy && !a.options.disabled && a.options.scope === c.options.scope && a.accept.call(a.element[0], c.currentItem || c.element) && b.ui.intersect(c, b.extend(a, {
                    offset: a.element.offset()
                }), a.options.tolerance) ? (d = !0, !1) : void 0
            }), d ? !1 : this.accept.call(this.element[0], c.currentItem || c.element) ? (this.options.activeClass && this.element.removeClass(this.options.activeClass), this.options.hoverClass && this.element.removeClass(this.options.hoverClass), this._trigger("drop", a, this.ui(c)),
                this.element) : !1) : !1
        },
        ui: function(a) {
            return {
                draggable: a.currentItem || a.element,
                helper: a.helper,
                position: a.position,
                offset: a.positionAbs
            }
        }
    });
    b.ui.intersect = function(a, b, c) {
        if (!b.offset) return !1;
        var d, e, g = (a.positionAbs || a.position.absolute).left,
            h = g + a.helperProportions.width,
            f = (a.positionAbs || a.position.absolute).top,
            l = f + a.helperProportions.height,
            m = b.offset.left,
            n = m + b.proportions.width,
            t = b.offset.top,
            r = t + b.proportions.height;
        switch (c) {
            case "fit":
                return g >= m && n >= h && f >= t && r >= l;
            case "intersect":
                return g +
                    a.helperProportions.width / 2 > m && n > h - a.helperProportions.width / 2 && f + a.helperProportions.height / 2 > t && r > l - a.helperProportions.height / 2;
            case "pointer":
                return d = (a.positionAbs || a.position.absolute).left + (a.clickOffset || a.offset.click).left, e = (a.positionAbs || a.position.absolute).top + (a.clickOffset || a.offset.click).top, e > t && t + b.proportions.height > e && d > m && m + b.proportions.width > d;
            case "touch":
                return (f >= t && r >= f || l >= t && r >= l || t > f && l > r) && (g >= m && n >= g || h >= m && n >= h || m > g && h > n);
            default:
                return !1
        }
    };
    b.ui.ddmanager = {
        current: null,
        droppables: {
            "default": []
        },
        prepareOffsets: function(a, f) {
            var c, d, e = b.ui.ddmanager.droppables[a.options.scope] || [],
                g = f ? f.type : null,
                h = (a.currentItem || a.element).find(":data(ui-droppable)").addBack();
            c = 0;
            a: for (; e.length > c; c++)
                if (!(e[c].options.disabled || a && !e[c].accept.call(e[c].element[0], a.currentItem || a.element))) {
                    for (d = 0; h.length > d; d++)
                        if (h[d] === e[c].element[0]) {
                            e[c].proportions.height = 0;
                            continue a
                        }
                    e[c].visible = "none" !== e[c].element.css("display");
                    e[c].visible && ("mousedown" === g && e[c]._activate.call(e[c],
                        f), e[c].offset = e[c].element.offset(), e[c].proportions = {
                        width: e[c].element[0].offsetWidth,
                        height: e[c].element[0].offsetHeight
                    })
                }
        },
        drop: function(a, f) {
            var c = !1;
            return b.each((b.ui.ddmanager.droppables[a.options.scope] || []).slice(), function() {
                this.options && (!this.options.disabled && this.visible && b.ui.intersect(a, this, this.options.tolerance) && (c = this._drop.call(this, f) || c), !this.options.disabled && this.visible && this.accept.call(this.element[0], a.currentItem || a.element) && (this.isout = !0, this.isover = !1, this._deactivate.call(this,
                    f)))
            }), c
        },
        dragStart: function(a, f) {
            a.element.parentsUntil("body").bind("scroll.droppable", function() {
                a.options.refreshPositions || b.ui.ddmanager.prepareOffsets(a, f)
            })
        },
        drag: function(a, f) {
            a.options.refreshPositions && b.ui.ddmanager.prepareOffsets(a, f);
            b.each(b.ui.ddmanager.droppables[a.options.scope] || [], function() {
                if (!this.options.disabled && !this.greedyChild && this.visible) {
                    var c, d, e, g = b.ui.intersect(a, this, this.options.tolerance);
                    (g = !g && this.isover ? "isout" : g && !this.isover ? "isover" : null) && (this.options.greedy &&
                    (d = this.options.scope, e = this.element.parents(":data(ui-droppable)").filter(function() {
                        return b.data(this, "ui-droppable").options.scope === d
                    }), e.length && (c = b.data(e[0], "ui-droppable"), c.greedyChild = "isover" === g)), c && "isover" === g && (c.isover = !1, c.isout = !0, c._out.call(c, f)), this[g] = !0, this["isout" === g ? "isover" : "isout"] = !1, this["isover" === g ? "_over" : "_out"].call(this, f), c && "isout" === g && (c.isout = !1, c.isover = !0, c._over.call(c, f)))
                }
            })
        },
        dragStop: function(a, f) {
            a.element.parentsUntil("body").unbind("scroll.droppable");
            a.options.refreshPositions || b.ui.ddmanager.prepareOffsets(a, f)
        }
    }
})(jQuery);
(function(b) {
    function a(a) {
        return parseInt(a, 10) || 0
    }

    function f(a) {
        return !isNaN(parseInt(a, 10))
    }
    b.widget("ui.resizable", b.ui.mouse, {
        version: "1.10.3",
        widgetEventPrefix: "resize",
        options: {
            alsoResize: !1,
            animate: !1,
            animateDuration: "slow",
            animateEasing: "swing",
            aspectRatio: !1,
            autoHide: !1,
            containment: !1,
            ghost: !1,
            grid: !1,
            handles: "e,s,se",
            helper: !1,
            maxHeight: null,
            maxWidth: null,
            minHeight: 10,
            minWidth: 10,
            zIndex: 90,
            resize: null,
            start: null,
            stop: null
        },
        _create: function() {
            var a, d, e, g, h, f = this,
                l = this.options;
            if (this.element.addClass("ui-resizable"),
                b.extend(this, {
                    _aspectRatio: !!l.aspectRatio,
                    aspectRatio: l.aspectRatio,
                    originalElement: this.element,
                    _proportionallyResizeElements: [],
                    _helper: l.helper || l.ghost || l.animate ? l.helper || "ui-resizable-helper" : null
                }), this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i) && (this.element.wrap(b("\x3cdiv class\x3d'ui-wrapper' style\x3d'overflow: hidden;'\x3e\x3c/div\x3e").css({
                position: this.element.css("position"),
                width: this.element.outerWidth(),
                height: this.element.outerHeight(),
                top: this.element.css("top"),
                left: this.element.css("left")
            })), this.element = this.element.parent().data("ui-resizable", this.element.data("ui-resizable")), this.elementIsWrapper = !0, this.element.css({
                marginLeft: this.originalElement.css("marginLeft"),
                marginTop: this.originalElement.css("marginTop"),
                marginRight: this.originalElement.css("marginRight"),
                marginBottom: this.originalElement.css("marginBottom")
            }), this.originalElement.css({
                marginLeft: 0,
                marginTop: 0,
                marginRight: 0,
                marginBottom: 0
            }), this.originalResizeStyle = this.originalElement.css("resize"),
                this.originalElement.css("resize", "none"), this._proportionallyResizeElements.push(this.originalElement.css({
                position: "static",
                zoom: 1,
                display: "block"
            })), this.originalElement.css({
                margin: this.originalElement.css("margin")
            }), this._proportionallyResize()), this.handles = l.handles || (b(".ui-resizable-handle", this.element).length ? {
                n: ".ui-resizable-n",
                e: ".ui-resizable-e",
                s: ".ui-resizable-s",
                w: ".ui-resizable-w",
                se: ".ui-resizable-se",
                sw: ".ui-resizable-sw",
                ne: ".ui-resizable-ne",
                nw: ".ui-resizable-nw"
            } : "e,s,se"),
            this.handles.constructor === String)
                for ("all" === this.handles && (this.handles = "n,e,s,w,se,sw,ne,nw"), a = this.handles.split(","), this.handles = {}, d = 0; a.length > d; d++) e = b.trim(a[d]), h = "ui-resizable-" + e, g = b("\x3cdiv class\x3d'ui-resizable-handle " + h + "'\x3e\x3c/div\x3e"), g.css({
                    zIndex: l.zIndex
                }), "se" === e && g.addClass("ui-icon ui-icon-gripsmall-diagonal-se"), this.handles[e] = ".ui-resizable-" + e, this.element.append(g);
            this._renderAxis = function(a) {
                var c, e, d, g;
                a = a || this.element;
                for (c in this.handles) this.handles[c].constructor ===
                String && (this.handles[c] = b(this.handles[c], this.element).show()), this.elementIsWrapper && this.originalElement[0].nodeName.match(/textarea|input|select|button/i) && (e = b(this.handles[c], this.element), g = /sw|ne|nw|se|n|s/.test(c) ? e.outerHeight() : e.outerWidth(), d = ["padding", /ne|nw|n/.test(c) ? "Top" : /se|sw|s/.test(c) ? "Bottom" : /^e$/.test(c) ? "Right" : "Left"].join(""), a.css(d, g), this._proportionallyResize()), b(this.handles[c]).length
            };
            this._renderAxis(this.element);
            this._handles = b(".ui-resizable-handle", this.element).disableSelection();
            this._handles.mouseover(function() {
                f.resizing || (this.className && (g = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i)), f.axis = g && g[1] ? g[1] : "se")
            });
            l.autoHide && (this._handles.hide(), b(this.element).addClass("ui-resizable-autohide").mouseenter(function() {
                l.disabled || (b(this).removeClass("ui-resizable-autohide"), f._handles.show())
            }).mouseleave(function() {
                l.disabled || f.resizing || (b(this).addClass("ui-resizable-autohide"), f._handles.hide())
            }));
            this._mouseInit()
        },
        _destroy: function() {
            this._mouseDestroy();
            var a, d = function(a) {
                b(a).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").removeData("ui-resizable").unbind(".resizable").find(".ui-resizable-handle").remove()
            };
            return this.elementIsWrapper && (d(this.element), a = this.element, this.originalElement.css({
                position: a.css("position"),
                width: a.outerWidth(),
                height: a.outerHeight(),
                top: a.css("top"),
                left: a.css("left")
            }).insertAfter(a), a.remove()), this.originalElement.css("resize", this.originalResizeStyle), d(this.originalElement),
                this
        },
        _mouseCapture: function(a) {
            var c, e, g = !1;
            for (c in this.handles) e = b(this.handles[c])[0], (e === a.target || b.contains(e, a.target)) && (g = !0);
            return !this.options.disabled && g
        },
        _mouseStart: function(c) {
            var d, e, g, h = this.options,
                f = this.element.position(),
                l = this.element;
            return this.resizing = !0, /absolute/.test(l.css("position")) ? l.css({
                position: "absolute",
                top: l.css("top"),
                left: l.css("left")
            }) : l.is(".ui-draggable") && l.css({
                position: "absolute",
                top: f.top,
                left: f.left
            }), this._renderProxy(), d = a(this.helper.css("left")),
                e = a(this.helper.css("top")), h.containment && (d += b(h.containment).scrollLeft() || 0, e += b(h.containment).scrollTop() || 0), this.offset = this.helper.offset(), this.position = {
                left: d,
                top: e
            }, this.size = this._helper ? {
                width: l.outerWidth(),
                height: l.outerHeight()
            } : {
                width: l.width(),
                height: l.height()
            }, this.originalSize = this._helper ? {
                width: l.outerWidth(),
                height: l.outerHeight()
            } : {
                width: l.width(),
                height: l.height()
            }, this.originalPosition = {
                left: d,
                top: e
            }, this.sizeDiff = {
                width: l.outerWidth() - l.width(),
                height: l.outerHeight() -
                    l.height()
            }, this.originalMousePosition = {
                left: c.pageX,
                top: c.pageY
            }, this.aspectRatio = "number" == typeof h.aspectRatio ? h.aspectRatio : this.originalSize.width / this.originalSize.height || 1, g = b(".ui-resizable-" + this.axis).css("cursor"), b("body").css("cursor", "auto" === g ? this.axis + "-resize" : g), l.addClass("ui-resizable-resizing"), this._propagate("start", c), !0
        },
        _mouseDrag: function(a) {
            var c, e = this.helper,
                g = {},
                h = this.originalMousePosition,
                f = this.position.top,
                l = this.position.left,
                m = this.size.width,
                n = this.size.height,
                t = a.pageX - h.left || 0,
                h = a.pageY - h.top || 0,
                r = this._change[this.axis];
            return r ? (c = r.apply(this, [a, t, h]), this._updateVirtualBoundaries(a.shiftKey), (this._aspectRatio || a.shiftKey) && (c = this._updateRatio(c, a)), c = this._respectSize(c, a), this._updateCache(c), this._propagate("resize", a), this.position.top !== f && (g.top = this.position.top + "px"), this.position.left !== l && (g.left = this.position.left + "px"), this.size.width !== m && (g.width = this.size.width + "px"), this.size.height !== n && (g.height = this.size.height + "px"), e.css(g), !this._helper && this._proportionallyResizeElements.length && this._proportionallyResize(), b.isEmptyObject(g) || this._trigger("resize", a, this.ui()), !1) : !1
        },
        _mouseStop: function(a) {
            this.resizing = !1;
            var c, e, g, h, f, l, m, n = this.options;
            return this._helper && (c = this._proportionallyResizeElements, e = c.length && /textarea/i.test(c[0].nodeName), g = e && b.ui.hasScroll(c[0], "left") ? 0 : this.sizeDiff.height, h = e ? 0 : this.sizeDiff.width, f = {
                width: this.helper.width() - h,
                height: this.helper.height() - g
            }, l = parseInt(this.element.css("left"),
                10) + (this.position.left - this.originalPosition.left) || null, m = parseInt(this.element.css("top"), 10) + (this.position.top - this.originalPosition.top) || null, n.animate || this.element.css(b.extend(f, {
                top: m,
                left: l
            })), this.helper.height(this.size.height), this.helper.width(this.size.width), this._helper && !n.animate && this._proportionallyResize()), b("body").css("cursor", "auto"), this.element.removeClass("ui-resizable-resizing"), this._propagate("stop", a), this._helper && this.helper.remove(), !1
        },
        _updateVirtualBoundaries: function(a) {
            var b,
                c, g, h, k;
            k = this.options;
            k = {
                minWidth: f(k.minWidth) ? k.minWidth : 0,
                maxWidth: f(k.maxWidth) ? k.maxWidth : 1 / 0,
                minHeight: f(k.minHeight) ? k.minHeight : 0,
                maxHeight: f(k.maxHeight) ? k.maxHeight : 1 / 0
            };
            (this._aspectRatio || a) && (b = k.minHeight * this.aspectRatio, g = k.minWidth / this.aspectRatio, c = k.maxHeight * this.aspectRatio, h = k.maxWidth / this.aspectRatio, b > k.minWidth && (k.minWidth = b), g > k.minHeight && (k.minHeight = g), k.maxWidth > c && (k.maxWidth = c), k.maxHeight > h && (k.maxHeight = h));
            this._vBoundaries = k
        },
        _updateCache: function(a) {
            this.offset =
                this.helper.offset();
            f(a.left) && (this.position.left = a.left);
            f(a.top) && (this.position.top = a.top);
            f(a.height) && (this.size.height = a.height);
            f(a.width) && (this.size.width = a.width)
        },
        _updateRatio: function(a) {
            var b = this.position,
                c = this.size,
                g = this.axis;
            return f(a.height) ? a.width = a.height * this.aspectRatio : f(a.width) && (a.height = a.width / this.aspectRatio), "sw" === g && (a.left = b.left + (c.width - a.width), a.top = null), "nw" === g && (a.top = b.top + (c.height - a.height), a.left = b.left + (c.width - a.width)), a
        },
        _respectSize: function(a) {
            var b =
                    this._vBoundaries,
                c = this.axis,
                g = f(a.width) && b.maxWidth && b.maxWidth < a.width,
                h = f(a.height) && b.maxHeight && b.maxHeight < a.height,
                k = f(a.width) && b.minWidth && b.minWidth > a.width,
                l = f(a.height) && b.minHeight && b.minHeight > a.height,
                m = this.originalPosition.left + this.originalSize.width,
                n = this.position.top + this.size.height,
                t = /sw|nw|w/.test(c),
                c = /nw|ne|n/.test(c);
            return k && (a.width = b.minWidth), l && (a.height = b.minHeight), g && (a.width = b.maxWidth), h && (a.height = b.maxHeight), k && t && (a.left = m - b.minWidth), g && t && (a.left = m - b.maxWidth),
            l && c && (a.top = n - b.minHeight), h && c && (a.top = n - b.maxHeight), a.width || a.height || a.left || !a.top ? a.width || a.height || a.top || !a.left || (a.left = null) : a.top = null, a
        },
        _proportionallyResize: function() {
            if (this._proportionallyResizeElements.length) {
                var a, b, e, g, h, f = this.helper || this.element;
                for (a = 0; this._proportionallyResizeElements.length > a; a++) {
                    if (h = this._proportionallyResizeElements[a], !this.borderDif)
                        for (this.borderDif = [], e = [h.css("borderTopWidth"), h.css("borderRightWidth"), h.css("borderBottomWidth"), h.css("borderLeftWidth")],
                                 g = [h.css("paddingTop"), h.css("paddingRight"), h.css("paddingBottom"), h.css("paddingLeft")], b = 0; e.length > b; b++) this.borderDif[b] = (parseInt(e[b], 10) || 0) + (parseInt(g[b], 10) || 0);
                    h.css({
                        height: f.height() - this.borderDif[0] - this.borderDif[2] || 0,
                        width: f.width() - this.borderDif[1] - this.borderDif[3] || 0
                    })
                }
            }
        },
        _renderProxy: function() {
            var a = this.options;
            this.elementOffset = this.element.offset();
            this._helper ? (this.helper = this.helper || b("\x3cdiv style\x3d'overflow:hidden;'\x3e\x3c/div\x3e"), this.helper.addClass(this._helper).css({
                width: this.element.outerWidth() -
                    1,
                height: this.element.outerHeight() - 1,
                position: "absolute",
                left: this.elementOffset.left + "px",
                top: this.elementOffset.top + "px",
                zIndex: ++a.zIndex
            }), this.helper.appendTo("body").disableSelection()) : this.helper = this.element
        },
        _change: {
            e: function(a, b) {
                return {
                    width: this.originalSize.width + b
                }
            },
            w: function(a, b) {
                return {
                    left: this.originalPosition.left + b,
                    width: this.originalSize.width - b
                }
            },
            n: function(a, b, e) {
                return {
                    top: this.originalPosition.top + e,
                    height: this.originalSize.height - e
                }
            },
            s: function(a, b, e) {
                return {
                    height: this.originalSize.height +
                        e
                }
            },
            se: function(a, d, e) {
                return b.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [a, d, e]))
            },
            sw: function(a, d, e) {
                return b.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [a, d, e]))
            },
            ne: function(a, d, e) {
                return b.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [a, d, e]))
            },
            nw: function(a, d, e) {
                return b.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [a, d, e]))
            }
        },
        _propagate: function(a, d) {
            b.ui.plugin.call(this, a, [d, this.ui()]);
            "resize" !== a && this._trigger(a, d, this.ui())
        },
        plugins: {},
        ui: function() {
            return {
                originalElement: this.originalElement,
                element: this.element,
                helper: this.helper,
                position: this.position,
                size: this.size,
                originalSize: this.originalSize,
                originalPosition: this.originalPosition
            }
        }
    });
    b.ui.plugin.add("resizable", "animate", {
        stop: function(a) {
            var c = b(this).data("ui-resizable"),
                e = c.options,
                g = c._proportionallyResizeElements,
                h = g.length && /textarea/i.test(g[0].nodeName),
                f = h && b.ui.hasScroll(g[0], "left") ? 0 : c.sizeDiff.height,
                h = {
                    width: c.size.width - (h ? 0 : c.sizeDiff.width),
                    height: c.size.height - f
                },
                f = parseInt(c.element.css("left"), 10) + (c.position.left - c.originalPosition.left) || null,
                l = parseInt(c.element.css("top"), 10) + (c.position.top - c.originalPosition.top) || null;
            c.element.animate(b.extend(h, l && f ? {
                top: l,
                left: f
            } : {}), {
                duration: e.animateDuration,
                easing: e.animateEasing,
                step: function() {
                    var e = {
                        width: parseInt(c.element.css("width"), 10),
                        height: parseInt(c.element.css("height"), 10),
                        top: parseInt(c.element.css("top"), 10),
                        left: parseInt(c.element.css("left"),
                            10)
                    };
                    g && g.length && b(g[0]).css({
                        width: e.width,
                        height: e.height
                    });
                    c._updateCache(e);
                    c._propagate("resize", a)
                }
            })
        }
    });
    b.ui.plugin.add("resizable", "containment", {
        start: function() {
            var c, d, e, g, h, f, l, m = b(this).data("ui-resizable"),
                n = m.element,
                t = m.options.containment;
            (n = t instanceof b ? t.get(0) : /parent/.test(t) ? n.parent().get(0) : t) && (m.containerElement = b(n), /document/.test(t) || t === document ? (m.containerOffset = {
                left: 0,
                top: 0
            }, m.containerPosition = {
                left: 0,
                top: 0
            }, m.parentData = {
                element: b(document),
                left: 0,
                top: 0,
                width: b(document).width(),
                height: b(document).height() || document.body.parentNode.scrollHeight
            }) : (c = b(n), d = [], b(["Top", "Right", "Left", "Bottom"]).each(function(b, e) {
                d[b] = a(c.css("padding" + e))
            }), m.containerOffset = c.offset(), m.containerPosition = c.position(), m.containerSize = {
                height: c.innerHeight() - d[3],
                width: c.innerWidth() - d[1]
            }, e = m.containerOffset, g = m.containerSize.height, h = m.containerSize.width, f = b.ui.hasScroll(n, "left") ? n.scrollWidth : h, l = b.ui.hasScroll(n) ? n.scrollHeight : g, m.parentData = {
                element: n,
                left: e.left,
                top: e.top,
                width: f,
                height: l
            }))
        },
        resize: function(a) {
            var c, e, g, h, f = b(this).data("ui-resizable");
            c = f.options;
            e = f.containerOffset;
            g = f.position;
            a = f._aspectRatio || a.shiftKey;
            h = {
                top: 0,
                left: 0
            };
            var l = f.containerElement;
            l[0] !== document && /static/.test(l.css("position")) && (h = e);
            g.left < (f._helper ? e.left : 0) && (f.size.width += f._helper ? f.position.left - e.left : f.position.left - h.left, a && (f.size.height = f.size.width / f.aspectRatio), f.position.left = c.helper ? e.left : 0);
            g.top < (f._helper ? e.top : 0) && (f.size.height += f._helper ? f.position.top - e.top :
                f.position.top, a && (f.size.width = f.size.height * f.aspectRatio), f.position.top = f._helper ? e.top : 0);
            f.offset.left = f.parentData.left + f.position.left;
            f.offset.top = f.parentData.top + f.position.top;
            c = Math.abs(f.offset.left - h.left + f.sizeDiff.width);
            e = Math.abs((f._helper ? f.offset.top - h.top : f.offset.top - e.top) + f.sizeDiff.height);
            g = f.containerElement.get(0) === f.element.parent().get(0);
            h = /relative|absolute/.test(f.containerElement.css("position"));
            g && h && (c -= f.parentData.left);
            c + f.size.width >= f.parentData.width &&
            (f.size.width = f.parentData.width - c, a && (f.size.height = f.size.width / f.aspectRatio));
            e + f.size.height >= f.parentData.height && (f.size.height = f.parentData.height - e, a && (f.size.width = f.size.height * f.aspectRatio))
        },
        stop: function() {
            var a = b(this).data("ui-resizable"),
                d = a.options,
                e = a.containerOffset,
                g = a.containerPosition,
                h = a.containerElement,
                f = b(a.helper),
                l = f.offset(),
                m = f.outerWidth() - a.sizeDiff.width,
                f = f.outerHeight() - a.sizeDiff.height;
            a._helper && !d.animate && /relative/.test(h.css("position")) && b(this).css({
                left: l.left -
                    g.left - e.left,
                width: m,
                height: f
            });
            a._helper && !d.animate && /static/.test(h.css("position")) && b(this).css({
                left: l.left - g.left - e.left,
                width: m,
                height: f
            })
        }
    });
    b.ui.plugin.add("resizable", "alsoResize", {
        start: function() {
            var a = b(this).data("ui-resizable").options,
                d = function(a) {
                    b(a).each(function() {
                        var a = b(this);
                        a.data("ui-resizable-alsoresize", {
                            width: parseInt(a.width(), 10),
                            height: parseInt(a.height(), 10),
                            left: parseInt(a.css("left"), 10),
                            top: parseInt(a.css("top"), 10)
                        })
                    })
                };
            "object" != typeof a.alsoResize || a.alsoResize.parentNode ?
                d(a.alsoResize) : a.alsoResize.length ? (a.alsoResize = a.alsoResize[0], d(a.alsoResize)) : b.each(a.alsoResize, function(a) {
                    d(a)
                })
        },
        resize: function(a, d) {
            a = b(this).data("ui-resizable");
            var c = a.options,
                g = a.originalSize,
                h = a.originalPosition,
                f = {
                    height: a.size.height - g.height || 0,
                    width: a.size.width - g.width || 0,
                    top: a.position.top - h.top || 0,
                    left: a.position.left - h.left || 0
                },
                l = function(a, c) {
                    b(a).each(function() {
                        var a = b(this),
                            e = b(this).data("ui-resizable-alsoresize"),
                            g = {},
                            h = c && c.length ? c : a.parents(d.originalElement[0]).length ? ["width", "height"] : ["width", "height", "top", "left"];
                        b.each(h, function(a, b) {
                            (a = (e[b] || 0) + (f[b] || 0)) && 0 <= a && (g[b] = a || null)
                        });
                        a.css(g)
                    })
                };
            "object" != typeof c.alsoResize || c.alsoResize.nodeType ? l(c.alsoResize) : b.each(c.alsoResize, function(a, b) {
                l(a, b)
            })
        },
        stop: function() {
            b(this).removeData("resizable-alsoresize")
        }
    });
    b.ui.plugin.add("resizable", "ghost", {
        start: function() {
            var a = b(this).data("ui-resizable"),
                d = a.options,
                e = a.size;
            a.ghost = a.originalElement.clone();
            a.ghost.css({
                opacity: .25,
                display: "block",
                position: "relative",
                height: e.height,
                width: e.width,
                margin: 0,
                left: 0,
                top: 0
            }).addClass("ui-resizable-ghost").addClass("string" == typeof d.ghost ? d.ghost : "");
            a.ghost.appendTo(a.helper)
        },
        resize: function() {
            var a = b(this).data("ui-resizable");
            a.ghost && a.ghost.css({
                position: "relative",
                height: a.size.height,
                width: a.size.width
            })
        },
        stop: function() {
            var a = b(this).data("ui-resizable");
            a.ghost && a.helper && a.helper.get(0).removeChild(a.ghost.get(0))
        }
    });
    b.ui.plugin.add("resizable", "grid", {
        resize: function() {
            var a = b(this).data("ui-resizable"),
                d = a.options,
                e = a.size,
                g = a.originalSize,
                h = a.originalPosition,
                f = a.axis,
                l = "number" == typeof d.grid ? [d.grid, d.grid] : d.grid,
                m = l[0] || 1,
                n = l[1] || 1,
                t = Math.round((e.width - g.width) / m) * m,
                e = Math.round((e.height - g.height) / n) * n,
                r = g.width + t,
                g = g.height + e,
                p = d.maxWidth && r > d.maxWidth,
                y = d.maxHeight && g > d.maxHeight,
                w = d.minWidth && d.minWidth > r,
                u = d.minHeight && d.minHeight > g;
            d.grid = l;
            w && (r += m);
            u && (g += n);
            p && (r -= m);
            y && (g -= n);
            /^(se|s|e)$/.test(f) ? (a.size.width = r, a.size.height = g) : /^(ne)$/.test(f) ? (a.size.width = r, a.size.height = g, a.position.top =
                h.top - e) : /^(sw)$/.test(f) ? (a.size.width = r, a.size.height = g, a.position.left = h.left - t) : (a.size.width = r, a.size.height = g, a.position.top = h.top - e, a.position.left = h.left - t)
        }
    })
})(jQuery);
(function(b) {
    b.widget("ui.selectable", b.ui.mouse, {
        version: "1.10.3",
        options: {
            appendTo: "body",
            autoRefresh: !0,
            distance: 0,
            filter: "*",
            tolerance: "touch",
            selected: null,
            selecting: null,
            start: null,
            stop: null,
            unselected: null,
            unselecting: null
        },
        _create: function() {
            var a, f = this;
            this.element.addClass("ui-selectable");
            this.dragged = !1;
            this.refresh = function() {
                a = b(f.options.filter, f.element[0]);
                a.addClass("ui-selectee");
                a.each(function() {
                    var a = b(this),
                        d = a.offset();
                    b.data(this, "selectable-item", {
                        element: this,
                        $element: a,
                        left: d.left,
                        top: d.top,
                        right: d.left + a.outerWidth(),
                        bottom: d.top + a.outerHeight(),
                        startselected: !1,
                        selected: a.hasClass("ui-selected"),
                        selecting: a.hasClass("ui-selecting"),
                        unselecting: a.hasClass("ui-unselecting")
                    })
                })
            };
            this.refresh();
            this.selectees = a.addClass("ui-selectee");
            this._mouseInit();
            this.helper = b("\x3cdiv class\x3d'ui-selectable-helper'\x3e\x3c/div\x3e")
        },
        _destroy: function() {
            this.selectees.removeClass("ui-selectee").removeData("selectable-item");
            this.element.removeClass("ui-selectable ui-selectable-disabled");
            this._mouseDestroy()
        },
        _mouseStart: function(a) {
            var f = this,
                c = this.options;
            this.opos = [a.pageX, a.pageY];
            this.options.disabled || (this.selectees = b(c.filter, this.element[0]), this._trigger("start", a), b(c.appendTo).append(this.helper), this.helper.css({
                left: a.pageX,
                top: a.pageY,
                width: 0,
                height: 0
            }), c.autoRefresh && this.refresh(), this.selectees.filter(".ui-selected").each(function() {
                var c = b.data(this, "selectable-item");
                c.startselected = !0;
                a.metaKey || a.ctrlKey || (c.$element.removeClass("ui-selected"), c.selected = !1,
                    c.$element.addClass("ui-unselecting"), c.unselecting = !0, f._trigger("unselecting", a, {
                    unselecting: c.element
                }))
            }), b(a.target).parents().addBack().each(function() {
                var c, e = b.data(this, "selectable-item");
                return e ? (c = !a.metaKey && !a.ctrlKey || !e.$element.hasClass("ui-selected"), e.$element.removeClass(c ? "ui-unselecting" : "ui-selected").addClass(c ? "ui-selecting" : "ui-unselecting"), e.unselecting = !c, e.selecting = c, e.selected = c, c ? f._trigger("selecting", a, {
                    selecting: e.element
                }) : f._trigger("unselecting", a, {
                    unselecting: e.element
                }), !1) : void 0
            }))
        },
        _mouseDrag: function(a) {
            if (this.dragged = !0, !this.options.disabled) {
                var f, c = this,
                    d = this.options,
                    e = this.opos[0],
                    g = this.opos[1],
                    h = a.pageX,
                    k = a.pageY;
                return e > h && (f = h, h = e, e = f), g > k && (f = k, k = g, g = f), this.helper.css({
                    left: e,
                    top: g,
                    width: h - e,
                    height: k - g
                }), this.selectees.each(function() {
                    var f = b.data(this, "selectable-item"),
                        m = !1;
                    f && f.element !== c.element[0] && ("touch" === d.tolerance ? m = !(f.left > h || e > f.right || f.top > k || g > f.bottom) : "fit" === d.tolerance && (m = f.left > e && h > f.right && f.top > g && k > f.bottom), m ? (f.selected &&
                    (f.$element.removeClass("ui-selected"), f.selected = !1), f.unselecting && (f.$element.removeClass("ui-unselecting"), f.unselecting = !1), f.selecting || (f.$element.addClass("ui-selecting"), f.selecting = !0, c._trigger("selecting", a, {
                        selecting: f.element
                    }))) : (f.selecting && ((a.metaKey || a.ctrlKey) && f.startselected ? (f.$element.removeClass("ui-selecting"), f.selecting = !1, f.$element.addClass("ui-selected"), f.selected = !0) : (f.$element.removeClass("ui-selecting"), f.selecting = !1, f.startselected && (f.$element.addClass("ui-unselecting"),
                        f.unselecting = !0), c._trigger("unselecting", a, {
                        unselecting: f.element
                    }))), f.selected && (a.metaKey || a.ctrlKey || f.startselected || (f.$element.removeClass("ui-selected"), f.selected = !1, f.$element.addClass("ui-unselecting"), f.unselecting = !0, c._trigger("unselecting", a, {
                        unselecting: f.element
                    })))))
                }), !1
            }
        },
        _mouseStop: function(a) {
            var f = this;
            return this.dragged = !1, b(".ui-unselecting", this.element[0]).each(function() {
                var c = b.data(this, "selectable-item");
                c.$element.removeClass("ui-unselecting");
                c.unselecting = !1;
                c.startselected = !1;
                f._trigger("unselected", a, {
                    unselected: c.element
                })
            }), b(".ui-selecting", this.element[0]).each(function() {
                var c = b.data(this, "selectable-item");
                c.$element.removeClass("ui-selecting").addClass("ui-selected");
                c.selecting = !1;
                c.selected = !0;
                c.startselected = !0;
                f._trigger("selected", a, {
                    selected: c.element
                })
            }), this._trigger("stop", a), this.helper.remove(), !1
        }
    })
})(jQuery);
(function(b) {
    function a(a, b, e) {
        return a > b && b + e > a
    }

    function f(a) {
        return /left|right/.test(a.css("float")) || /inline|table-cell/.test(a.css("display"))
    }
    b.widget("ui.sortable", b.ui.mouse, {
        version: "1.10.3",
        widgetEventPrefix: "sort",
        ready: !1,
        options: {
            appendTo: "parent",
            axis: !1,
            connectWith: !1,
            containment: !1,
            cursor: "auto",
            cursorAt: !1,
            dropOnEmpty: !0,
            forcePlaceholderSize: !1,
            forceHelperSize: !1,
            grid: !1,
            handle: !1,
            helper: "original",
            items: "\x3e *",
            opacity: !1,
            placeholder: !1,
            revert: !1,
            scroll: !0,
            scrollSensitivity: 20,
            scrollSpeed: 20,
            scope: "default",
            tolerance: "intersect",
            zIndex: 1E3,
            activate: null,
            beforeStop: null,
            change: null,
            deactivate: null,
            out: null,
            over: null,
            receive: null,
            remove: null,
            sort: null,
            start: null,
            stop: null,
            update: null
        },
        _create: function() {
            var a = this.options;
            this.containerCache = {};
            this.element.addClass("ui-sortable");
            this.refresh();
            this.floating = this.items.length ? "x" === a.axis || f(this.items[0].item) : !1;
            this.offset = this.element.offset();
            this._mouseInit();
            this.ready = !0
        },
        _destroy: function() {
            this.element.removeClass("ui-sortable ui-sortable-disabled");
            this._mouseDestroy();
            for (var a = this.items.length - 1; 0 <= a; a--) this.items[a].item.removeData(this.widgetName + "-item");
            return this
        },
        _setOption: function(a, d) {
            "disabled" === a ? (this.options[a] = d, this.widget().toggleClass("ui-sortable-disabled", !!d)) : b.Widget.prototype._setOption.apply(this, arguments)
        },
        _mouseCapture: function(a, d) {
            var c = null,
                g = !1,
                h = this;
            return this.reverting ? !1 : this.options.disabled || "static" === this.options.type ? !1 : (this._refreshItems(a), b(a.target).parents().each(function() {
                return b.data(this,
                    h.widgetName + "-item") === h ? (c = b(this), !1) : void 0
            }), b.data(a.target, h.widgetName + "-item") === h && (c = b(a.target)), c ? !this.options.handle || d || (b(this.options.handle, c).find("*").addBack().each(function() {
                this === a.target && (g = !0)
            }), g) ? (this.currentItem = c, this._removeCurrentsFromItems(), !0) : !1 : !1)
        },
        _mouseStart: function(a, d, e) {
            var c;
            d = this.options;
            if (this.currentContainer = this, this.refreshPositions(), this.helper = this._createHelper(a), this._cacheHelperProportions(), this._cacheMargins(), this.scrollParent = this.helper.scrollParent(),
                this.offset = this.currentItem.offset(), this.offset = {
                top: this.offset.top - this.margins.top,
                left: this.offset.left - this.margins.left
            }, b.extend(this.offset, {
                click: {
                    left: a.pageX - this.offset.left,
                    top: a.pageY - this.offset.top
                },
                parent: this._getParentOffset(),
                relative: this._getRelativeOffset()
            }), this.helper.css("position", "absolute"), this.cssPosition = this.helper.css("position"), this.originalPosition = this._generatePosition(a), this.originalPageX = a.pageX, this.originalPageY = a.pageY, d.cursorAt && this._adjustOffsetFromHelper(d.cursorAt),
                this.domPosition = {
                    prev: this.currentItem.prev()[0],
                    parent: this.currentItem.parent()[0]
                }, this.helper[0] !== this.currentItem[0] && this.currentItem.hide(), this._createPlaceholder(), d.containment && this._setContainment(), d.cursor && "auto" !== d.cursor && (c = this.document.find("body"), this.storedCursor = c.css("cursor"), c.css("cursor", d.cursor), this.storedStylesheet = b("\x3cstyle\x3e*{ cursor: " + d.cursor + " !important; }\x3c/style\x3e").appendTo(c)), d.opacity && (this.helper.css("opacity") && (this._storedOpacity = this.helper.css("opacity")),
                this.helper.css("opacity", d.opacity)), d.zIndex && (this.helper.css("zIndex") && (this._storedZIndex = this.helper.css("zIndex")), this.helper.css("zIndex", d.zIndex)), this.scrollParent[0] !== document && "HTML" !== this.scrollParent[0].tagName && (this.overflowOffset = this.scrollParent.offset()), this._trigger("start", a, this._uiHash()), this._preserveHelperProportions || this._cacheHelperProportions(), !e)
                for (e = this.containers.length - 1; 0 <= e; e--) this.containers[e]._trigger("activate", a, this._uiHash(this));
            return b.ui.ddmanager &&
            (b.ui.ddmanager.current = this), b.ui.ddmanager && !d.dropBehaviour && b.ui.ddmanager.prepareOffsets(this, a), this.dragging = !0, this.helper.addClass("ui-sortable-helper"), this._mouseDrag(a), !0
        },
        _mouseDrag: function(a) {
            var c, e, g, h;
            c = this.options;
            var f = !1;
            this.position = this._generatePosition(a);
            this.positionAbs = this._convertPositionTo("absolute");
            this.lastPositionAbs || (this.lastPositionAbs = this.positionAbs);
            this.options.scroll && (this.scrollParent[0] !== document && "HTML" !== this.scrollParent[0].tagName ? (this.overflowOffset.top +
            this.scrollParent[0].offsetHeight - a.pageY < c.scrollSensitivity ? this.scrollParent[0].scrollTop = f = this.scrollParent[0].scrollTop + c.scrollSpeed : a.pageY - this.overflowOffset.top < c.scrollSensitivity && (this.scrollParent[0].scrollTop = f = this.scrollParent[0].scrollTop - c.scrollSpeed), this.overflowOffset.left + this.scrollParent[0].offsetWidth - a.pageX < c.scrollSensitivity ? this.scrollParent[0].scrollLeft = f = this.scrollParent[0].scrollLeft + c.scrollSpeed : a.pageX - this.overflowOffset.left < c.scrollSensitivity && (this.scrollParent[0].scrollLeft =
                f = this.scrollParent[0].scrollLeft - c.scrollSpeed)) : (a.pageY - b(document).scrollTop() < c.scrollSensitivity ? f = b(document).scrollTop(b(document).scrollTop() - c.scrollSpeed) : b(window).height() - (a.pageY - b(document).scrollTop()) < c.scrollSensitivity && (f = b(document).scrollTop(b(document).scrollTop() + c.scrollSpeed)), a.pageX - b(document).scrollLeft() < c.scrollSensitivity ? f = b(document).scrollLeft(b(document).scrollLeft() - c.scrollSpeed) : b(window).width() - (a.pageX - b(document).scrollLeft()) < c.scrollSensitivity && (f = b(document).scrollLeft(b(document).scrollLeft() +
                c.scrollSpeed))), !1 !== f && b.ui.ddmanager && !c.dropBehaviour && b.ui.ddmanager.prepareOffsets(this, a));
            this.positionAbs = this._convertPositionTo("absolute");
            this.options.axis && "y" === this.options.axis || (this.helper[0].style.left = this.position.left + "px");
            this.options.axis && "x" === this.options.axis || (this.helper[0].style.top = this.position.top + "px");
            for (c = this.items.length - 1; 0 <= c; c--)
                if (e = this.items[c], g = e.item[0], h = this._intersectsWithPointer(e), h && e.instance === this.currentContainer && g !== this.currentItem[0] &&
                this.placeholder[1 === h ? "next" : "prev"]()[0] !== g && !b.contains(this.placeholder[0], g) && ("semi-dynamic" === this.options.type ? !b.contains(this.element[0], g) : !0)) {
                    if (this.direction = 1 === h ? "down" : "up", "pointer" !== this.options.tolerance && !this._intersectsWithSides(e)) break;
                    this._rearrange(a, e);
                    this._trigger("change", a, this._uiHash());
                    break
                }
            return this._contactContainers(a), b.ui.ddmanager && b.ui.ddmanager.drag(this, a), this._trigger("sort", a, this._uiHash()), this.lastPositionAbs = this.positionAbs, !1
        },
        _mouseStop: function(a,
                             d) {
            if (a) {
                if (b.ui.ddmanager && !this.options.dropBehaviour && b.ui.ddmanager.drop(this, a), this.options.revert) {
                    var c = this;
                    d = this.placeholder.offset();
                    var g = this.options.axis,
                        h = {};
                    g && "x" !== g || (h.left = d.left - this.offset.parent.left - this.margins.left + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollLeft));
                    g && "y" !== g || (h.top = d.top - this.offset.parent.top - this.margins.top + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollTop));
                    this.reverting = !0;
                    b(this.helper).animate(h, parseInt(this.options.revert,
                        10) || 500, function() {
                        c._clear(a)
                    })
                } else this._clear(a, d);
                return !1
            }
        },
        cancel: function() {
            if (this.dragging) {
                this._mouseUp({
                    target: null
                });
                "original" === this.options.helper ? this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper") : this.currentItem.show();
                for (var a = this.containers.length - 1; 0 <= a; a--) this.containers[a]._trigger("deactivate", null, this._uiHash(this)), this.containers[a].containerCache.over && (this.containers[a]._trigger("out", null, this._uiHash(this)), this.containers[a].containerCache.over =
                    0)
            }
            return this.placeholder && (this.placeholder[0].parentNode && this.placeholder[0].parentNode.removeChild(this.placeholder[0]), "original" !== this.options.helper && this.helper && this.helper[0].parentNode && this.helper.remove(), b.extend(this, {
                helper: null,
                dragging: !1,
                reverting: !1,
                _noFinalSort: null
            }), this.domPosition.prev ? b(this.domPosition.prev).after(this.currentItem) : b(this.domPosition.parent).prepend(this.currentItem)), this
        },
        serialize: function(a) {
            var c = this._getItemsAsjQuery(a && a.connected),
                e = [];
            return a =
                a || {}, b(c).each(function() {
                var c = (b(a.item || this).attr(a.attribute || "id") || "").match(a.expression || /(.+)[\-=_](.+)/);
                c && e.push((a.key || c[1] + "[]") + "\x3d" + (a.key && a.expression ? c[1] : c[2]))
            }), !e.length && a.key && e.push(a.key + "\x3d"), e.join("\x26")
        },
        toArray: function(a) {
            var c = this._getItemsAsjQuery(a && a.connected),
                e = [];
            return a = a || {}, c.each(function() {
                e.push(b(a.item || this).attr(a.attribute || "id") || "")
            }), e
        },
        _intersectsWith: function(a) {
            var b = this.positionAbs.left,
                c = b + this.helperProportions.width,
                g = this.positionAbs.top,
                h = g + this.helperProportions.height,
                f = a.left,
                l = f + a.width,
                m = a.top,
                n = m + a.height,
                t = this.offset.click.top,
                r = this.offset.click.left,
                t = "x" === this.options.axis || g + t > m && n > g + t,
                r = "y" === this.options.axis || b + r > f && l > b + r;
            return "pointer" === this.options.tolerance || this.options.forcePointerForContainers || "pointer" !== this.options.tolerance && this.helperProportions[this.floating ? "width" : "height"] > a[this.floating ? "width" : "height"] ? t && r : b + this.helperProportions.width / 2 > f && l > c - this.helperProportions.width / 2 && g + this.helperProportions.height /
                2 > m && n > h - this.helperProportions.height / 2
        },
        _intersectsWithPointer: function(b) {
            var c = "x" === this.options.axis || a(this.positionAbs.top + this.offset.click.top, b.top, b.height);
            b = "y" === this.options.axis || a(this.positionAbs.left + this.offset.click.left, b.left, b.width);
            c = c && b;
            b = this._getDragVerticalDirection();
            var e = this._getDragHorizontalDirection();
            return c ? this.floating ? e && "right" === e || "down" === b ? 2 : 1 : b && ("down" === b ? 2 : 1) : !1
        },
        _intersectsWithSides: function(b) {
            var c = a(this.positionAbs.top + this.offset.click.top,
                b.top + b.height / 2, b.height);
            b = a(this.positionAbs.left + this.offset.click.left, b.left + b.width / 2, b.width);
            var e = this._getDragVerticalDirection(),
                g = this._getDragHorizontalDirection();
            return this.floating && g ? "right" === g && b || "left" === g && !b : e && ("down" === e && c || "up" === e && !c)
        },
        _getDragVerticalDirection: function() {
            var a = this.positionAbs.top - this.lastPositionAbs.top;
            return 0 !== a && (0 < a ? "down" : "up")
        },
        _getDragHorizontalDirection: function() {
            var a = this.positionAbs.left - this.lastPositionAbs.left;
            return 0 !== a && (0 < a ?
                "right" : "left")
        },
        refresh: function(a) {
            return this._refreshItems(a), this.refreshPositions(), this
        },
        _connectWith: function() {
            var a = this.options;
            return a.connectWith.constructor === String ? [a.connectWith] : a.connectWith
        },
        _getItemsAsjQuery: function(a) {
            var c, e, g, h = [],
                f = [],
                l = this._connectWith();
            if (l && a)
                for (a = l.length - 1; 0 <= a; a--)
                    for (e = b(l[a]), c = e.length - 1; 0 <= c; c--)(g = b.data(e[c], this.widgetFullName)) && g !== this && !g.options.disabled && f.push([b.isFunction(g.options.items) ? g.options.items.call(g.element) : b(g.options.items,
                        g.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), g]);
            f.push([b.isFunction(this.options.items) ? this.options.items.call(this.element, null, {
                options: this.options,
                item: this.currentItem
            }) : b(this.options.items, this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), this]);
            for (a = f.length - 1; 0 <= a; a--) f[a][0].each(function() {
                h.push(this)
            });
            return b(h)
        },
        _removeCurrentsFromItems: function() {
            var a = this.currentItem.find(":data(" + this.widgetName + "-item)");
            this.items = b.grep(this.items,
                function(b) {
                    for (var c = 0; a.length > c; c++)
                        if (a[c] === b.item[0]) return !1;
                    return !0
                })
        },
        _refreshItems: function(a) {
            this.items = [];
            this.containers = [this];
            var c, e, g, h, f, l = this.items,
                m = [
                    [b.isFunction(this.options.items) ? this.options.items.call(this.element[0], a, {
                        item: this.currentItem
                    }) : b(this.options.items, this.element), this]
                ];
            if ((f = this._connectWith()) && this.ready)
                for (c = f.length - 1; 0 <= c; c--)
                    for (g = b(f[c]), e = g.length - 1; 0 <= e; e--)(h = b.data(g[e], this.widgetFullName)) && h !== this && !h.options.disabled && (m.push([b.isFunction(h.options.items) ?
                        h.options.items.call(h.element[0], a, {
                            item: this.currentItem
                        }) : b(h.options.items, h.element), h
                    ]), this.containers.push(h));
            for (c = m.length - 1; 0 <= c; c--)
                for (a = m[c][1], g = m[c][0], e = 0, f = g.length; f > e; e++) h = b(g[e]), h.data(this.widgetName + "-item", a), l.push({
                    item: h,
                    instance: a,
                    width: 0,
                    height: 0,
                    left: 0,
                    top: 0
                })
        },
        refreshPositions: function(a) {
            this.offsetParent && this.helper && (this.offset.parent = this._getParentOffset());
            var c, e, g, h;
            for (c = this.items.length - 1; 0 <= c; c--) e = this.items[c], e.instance !== this.currentContainer && this.currentContainer &&
            e.item[0] !== this.currentItem[0] || (g = this.options.toleranceElement ? b(this.options.toleranceElement, e.item) : e.item, a || (e.width = g.outerWidth(), e.height = g.outerHeight()), h = g.offset(), e.left = h.left, e.top = h.top);
            if (this.options.custom && this.options.custom.refreshContainers) this.options.custom.refreshContainers.call(this);
            else
                for (c = this.containers.length - 1; 0 <= c; c--) h = this.containers[c].element.offset(), this.containers[c].containerCache.left = h.left, this.containers[c].containerCache.top = h.top, this.containers[c].containerCache.width =
                    this.containers[c].element.outerWidth(), this.containers[c].containerCache.height = this.containers[c].element.outerHeight();
            return this
        },
        _createPlaceholder: function(a) {
            a = a || this;
            var c, e = a.options;
            e.placeholder && e.placeholder.constructor !== String || (c = e.placeholder, e.placeholder = {
                element: function() {
                    var e = a.currentItem[0].nodeName.toLowerCase(),
                        d = b("\x3c" + e + "\x3e", a.document[0]).addClass(c || a.currentItem[0].className + " ui-sortable-placeholder").removeClass("ui-sortable-helper");
                    return "tr" === e ? a.currentItem.children().each(function() {
                        b("\x3ctd\x3e\x26#160;\x3c/td\x3e",
                            a.document[0]).attr("colspan", b(this).attr("colspan") || 1).appendTo(d)
                    }) : "img" === e && d.attr("src", a.currentItem.attr("src")), c || d.css("visibility", "hidden"), d
                },
                update: function(b, d) {
                    (!c || e.forcePlaceholderSize) && (d.height() || d.height(a.currentItem.innerHeight() - parseInt(a.currentItem.css("paddingTop") || 0, 10) - parseInt(a.currentItem.css("paddingBottom") || 0, 10)), d.width() || d.width(a.currentItem.innerWidth() - parseInt(a.currentItem.css("paddingLeft") || 0, 10) - parseInt(a.currentItem.css("paddingRight") || 0, 10)))
                }
            });
            a.placeholder = b(e.placeholder.element.call(a.element, a.currentItem));
            a.currentItem.after(a.placeholder);
            e.placeholder.update(a, a.placeholder)
        },
        _contactContainers: function(c) {
            var d, e, g, h, k, l, m, n, t, r = e = null;
            for (d = this.containers.length - 1; 0 <= d; d--) b.contains(this.currentItem[0], this.containers[d].element[0]) || (this._intersectsWith(this.containers[d].containerCache) ? e && b.contains(this.containers[d].element[0], e.element[0]) || (e = this.containers[d], r = d) : this.containers[d].containerCache.over && (this.containers[d]._trigger("out",
                c, this._uiHash(this)), this.containers[d].containerCache.over = 0));
            if (e)
                if (1 === this.containers.length) this.containers[r].containerCache.over || (this.containers[r]._trigger("over", c, this._uiHash(this)), this.containers[r].containerCache.over = 1);
                else {
                    d = 1E4;
                    g = null;
                    h = (t = e.floating || f(this.currentItem)) ? "left" : "top";
                    k = t ? "width" : "height";
                    l = this.positionAbs[h] + this.offset.click[h];
                    for (e = this.items.length - 1; 0 <= e; e--) b.contains(this.containers[r].element[0], this.items[e].item[0]) && this.items[e].item[0] !== this.currentItem[0] &&
                    (!t || a(this.positionAbs.top + this.offset.click.top, this.items[e].top, this.items[e].height)) && (m = this.items[e].item.offset()[h], n = !1, Math.abs(m - l) > Math.abs(m + this.items[e][k] - l) && (n = !0, m += this.items[e][k]), d > Math.abs(m - l) && (d = Math.abs(m - l), g = this.items[e], this.direction = n ? "up" : "down"));
                    (g || this.options.dropOnEmpty) && this.currentContainer !== this.containers[r] && (g ? this._rearrange(c, g, null, !0) : this._rearrange(c, null, this.containers[r].element, !0), this._trigger("change", c, this._uiHash()), this.containers[r]._trigger("change",
                        c, this._uiHash(this)), this.currentContainer = this.containers[r], this.options.placeholder.update(this.currentContainer, this.placeholder), this.containers[r]._trigger("over", c, this._uiHash(this)), this.containers[r].containerCache.over = 1)
                }
        },
        _createHelper: function(a) {
            var c = this.options;
            a = b.isFunction(c.helper) ? b(c.helper.apply(this.element[0], [a, this.currentItem])) : "clone" === c.helper ? this.currentItem.clone() : this.currentItem;
            return a.parents("body").length || b("parent" !== c.appendTo ? c.appendTo : this.currentItem[0].parentNode)[0].appendChild(a[0]),
            a[0] === this.currentItem[0] && (this._storedCSS = {
                width: this.currentItem[0].style.width,
                height: this.currentItem[0].style.height,
                position: this.currentItem.css("position"),
                top: this.currentItem.css("top"),
                left: this.currentItem.css("left")
            }), (!a[0].style.width || c.forceHelperSize) && a.width(this.currentItem.width()), (!a[0].style.height || c.forceHelperSize) && a.height(this.currentItem.height()), a
        },
        _adjustOffsetFromHelper: function(a) {
            "string" == typeof a && (a = a.split(" "));
            b.isArray(a) && (a = {
                left: +a[0],
                top: +a[1] ||
                    0
            });
            "left" in a && (this.offset.click.left = a.left + this.margins.left);
            "right" in a && (this.offset.click.left = this.helperProportions.width - a.right + this.margins.left);
            "top" in a && (this.offset.click.top = a.top + this.margins.top);
            "bottom" in a && (this.offset.click.top = this.helperProportions.height - a.bottom + this.margins.top)
        },
        _getParentOffset: function() {
            this.offsetParent = this.helper.offsetParent();
            var a = this.offsetParent.offset();
            return "absolute" === this.cssPosition && this.scrollParent[0] !== document && b.contains(this.scrollParent[0],
                this.offsetParent[0]) && (a.left += this.scrollParent.scrollLeft(), a.top += this.scrollParent.scrollTop()), (this.offsetParent[0] === document.body || this.offsetParent[0].tagName && "html" === this.offsetParent[0].tagName.toLowerCase() && b.ui.ie) && (a = {
                top: 0,
                left: 0
            }), {
                top: a.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                left: a.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
            }
        },
        _getRelativeOffset: function() {
            if ("relative" === this.cssPosition) {
                var a = this.currentItem.position();
                return {
                    top: a.top -
                        (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
                    left: a.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
                }
            }
            return {
                top: 0,
                left: 0
            }
        },
        _cacheMargins: function() {
            this.margins = {
                left: parseInt(this.currentItem.css("marginLeft"), 10) || 0,
                top: parseInt(this.currentItem.css("marginTop"), 10) || 0
            }
        },
        _cacheHelperProportions: function() {
            this.helperProportions = {
                width: this.helper.outerWidth(),
                height: this.helper.outerHeight()
            }
        },
        _setContainment: function() {
            var a, d, e, g = this.options;
            "parent" === g.containment && (g.containment = this.helper[0].parentNode);
            "document" !== g.containment && "window" !== g.containment || (this.containment = [0 - this.offset.relative.left - this.offset.parent.left, 0 - this.offset.relative.top - this.offset.parent.top, b("document" === g.containment ? document : window).width() - this.helperProportions.width - this.margins.left, (b("document" === g.containment ? document : window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]);
            /^(document|window|parent)$/.test(g.containment) ||
            (a = b(g.containment)[0], d = b(g.containment).offset(), e = "hidden" !== b(a).css("overflow"), this.containment = [d.left + (parseInt(b(a).css("borderLeftWidth"), 10) || 0) + (parseInt(b(a).css("paddingLeft"), 10) || 0) - this.margins.left, d.top + (parseInt(b(a).css("borderTopWidth"), 10) || 0) + (parseInt(b(a).css("paddingTop"), 10) || 0) - this.margins.top, d.left + (e ? Math.max(a.scrollWidth, a.offsetWidth) : a.offsetWidth) - (parseInt(b(a).css("borderLeftWidth"), 10) || 0) - (parseInt(b(a).css("paddingRight"), 10) || 0) - this.helperProportions.width -
            this.margins.left, d.top + (e ? Math.max(a.scrollHeight, a.offsetHeight) : a.offsetHeight) - (parseInt(b(a).css("borderTopWidth"), 10) || 0) - (parseInt(b(a).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top
            ])
        },
        _convertPositionTo: function(a, d) {
            d || (d = this.position);
            a = "absolute" === a ? 1 : -1;
            var c = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && b.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
                g = /(html|body)/i.test(c[0].tagName);
            return {
                top: d.top +
                    this.offset.relative.top * a + this.offset.parent.top * a - ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : g ? 0 : c.scrollTop()) * a,
                left: d.left + this.offset.relative.left * a + this.offset.parent.left * a - ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : g ? 0 : c.scrollLeft()) * a
            }
        },
        _generatePosition: function(a) {
            var c, e, g = this.options,
                h = a.pageX,
                f = a.pageY,
                l = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && b.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
                m = /(html|body)/i.test(l[0].tagName);
            return "relative" !== this.cssPosition || this.scrollParent[0] !== document && this.scrollParent[0] !== this.offsetParent[0] || (this.offset.relative = this._getRelativeOffset()), this.originalPosition && (this.containment && (a.pageX - this.offset.click.left < this.containment[0] && (h = this.containment[0] + this.offset.click.left), a.pageY - this.offset.click.top < this.containment[1] && (f = this.containment[1] + this.offset.click.top), a.pageX - this.offset.click.left > this.containment[2] && (h = this.containment[2] +
                this.offset.click.left), a.pageY - this.offset.click.top > this.containment[3] && (f = this.containment[3] + this.offset.click.top)), g.grid && (c = this.originalPageY + Math.round((f - this.originalPageY) / g.grid[1]) * g.grid[1], f = this.containment ? c - this.offset.click.top >= this.containment[1] && c - this.offset.click.top <= this.containment[3] ? c : c - this.offset.click.top >= this.containment[1] ? c - g.grid[1] : c + g.grid[1] : c, e = this.originalPageX + Math.round((h - this.originalPageX) / g.grid[0]) * g.grid[0], h = this.containment ? e - this.offset.click.left >=
            this.containment[0] && e - this.offset.click.left <= this.containment[2] ? e : e - this.offset.click.left >= this.containment[0] ? e - g.grid[0] : e + g.grid[0] : e)), {
                top: f - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : m ? 0 : l.scrollTop()),
                left: h - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : m ? 0 : l.scrollLeft())
            }
        },
        _rearrange: function(a, b, e, g) {
            e ? e[0].appendChild(this.placeholder[0]) :
                b.item[0].parentNode.insertBefore(this.placeholder[0], "down" === this.direction ? b.item[0] : b.item[0].nextSibling);
            var c = this.counter = this.counter ? ++this.counter : 1;
            this._delay(function() {
                c === this.counter && this.refreshPositions(!g)
            })
        },
        _clear: function(a, b) {
            this.reverting = !1;
            var c, d = [];
            if (!this._noFinalSort && this.currentItem.parent().length && this.placeholder.before(this.currentItem), this._noFinalSort = null, this.helper[0] === this.currentItem[0]) {
                for (c in this._storedCSS) "auto" !== this._storedCSS[c] && "static" !==
                this._storedCSS[c] || (this._storedCSS[c] = "");
                this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper")
            } else this.currentItem.show();
            this.fromOutside && !b && d.push(function(a) {
                this._trigger("receive", a, this._uiHash(this.fromOutside))
            });
            !this.fromOutside && this.domPosition.prev === this.currentItem.prev().not(".ui-sortable-helper")[0] && this.domPosition.parent === this.currentItem.parent()[0] || b || d.push(function(a) {
                this._trigger("update", a, this._uiHash())
            });
            this !== this.currentContainer && (b || (d.push(function(a) {
                this._trigger("remove",
                    a, this._uiHash())
            }), d.push(function(a) {
                return function(b) {
                    a._trigger("receive", b, this._uiHash(this))
                }
            }.call(this, this.currentContainer)), d.push(function(a) {
                return function(b) {
                    a._trigger("update", b, this._uiHash(this))
                }
            }.call(this, this.currentContainer))));
            for (c = this.containers.length - 1; 0 <= c; c--) b || d.push(function(a) {
                return function(b) {
                    a._trigger("deactivate", b, this._uiHash(this))
                }
            }.call(this, this.containers[c])), this.containers[c].containerCache.over && (d.push(function(a) {
                return function(b) {
                    a._trigger("out",
                        b, this._uiHash(this))
                }
            }.call(this, this.containers[c])), this.containers[c].containerCache.over = 0);
            if (this.storedCursor && (this.document.find("body").css("cursor", this.storedCursor), this.storedStylesheet.remove()), this._storedOpacity && this.helper.css("opacity", this._storedOpacity), this._storedZIndex && this.helper.css("zIndex", "auto" === this._storedZIndex ? "" : this._storedZIndex), this.dragging = !1, this.cancelHelperRemoval) {
                if (!b) {
                    this._trigger("beforeStop", a, this._uiHash());
                    for (c = 0; d.length > c; c++) d[c].call(this,
                        a);
                    this._trigger("stop", a, this._uiHash())
                }
                return this.fromOutside = !1, !1
            }
            if (b || this._trigger("beforeStop", a, this._uiHash()), this.placeholder[0].parentNode.removeChild(this.placeholder[0]), this.helper[0] !== this.currentItem[0] && this.helper.remove(), this.helper = null, !b) {
                for (c = 0; d.length > c; c++) d[c].call(this, a);
                this._trigger("stop", a, this._uiHash())
            }
            return this.fromOutside = !1, !0
        },
        _trigger: function() {
            !1 === b.Widget.prototype._trigger.apply(this, arguments) && this.cancel()
        },
        _uiHash: function(a) {
            var c = a || this;
            return {
                helper: c.helper,
                placeholder: c.placeholder || b([]),
                position: c.position,
                originalPosition: c.originalPosition,
                offset: c.positionAbs,
                item: c.currentItem,
                sender: a ? a.element : null
            }
        }
    })
})(jQuery);
(function(b, a) {
    b.effects = {
        effect: {}
    };
    (function(a, b) {
        function c(a, b, c) {
            var e = t[b.type] || {};
            return null == a ? c || !b.def ? null : b.def : (a = e.floor ? ~~a : parseFloat(a), isNaN(a) ? b.def : e.mod ? (a + e.mod) % e.mod : 0 > a ? 0 : a > e.max ? e.max : a)
        }

        function e(c) {
            var e = m(),
                d = e._rgba = [];
            return c = c.toLowerCase(), y(l, function(a, g) {
                var h;
                a = (a = g.re.exec(c)) && g.parse(a);
                g = g.space || "rgba";
                return a ? (h = e[g](a), e[n[g].cache] = h[n[g].cache], d = e._rgba = h._rgba, !1) : b
            }), d.length ? ("0,0,0,0" === d.join() && a.extend(d, h.transparent), e) : h[c]
        }

        function g(a,
                   b, c) {
            return c = (c + 1) % 1, 1 > 6 * c ? a + 6 * (b - a) * c : 1 > 2 * c ? b : 2 > 3 * c ? a + 6 * (b - a) * (2 / 3 - c) : a
        }
        var h, f = /^([\-+])=\s*(\d+\.?\d*)/,
            l = [{
                re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                parse: function(a) {
                    return [a[1], a[2], a[3], a[4]]
                }
            }, {
                re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                parse: function(a) {
                    return [2.55 * a[1], 2.55 * a[2], 2.55 * a[3], a[4]]
                }
            }, {
                re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
                parse: function(a) {
                    return [parseInt(a[1],
                        16), parseInt(a[2], 16), parseInt(a[3], 16)]
                }
            }, {
                re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
                parse: function(a) {
                    return [parseInt(a[1] + a[1], 16), parseInt(a[2] + a[2], 16), parseInt(a[3] + a[3], 16)]
                }
            }, {
                re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
                space: "hsla",
                parse: function(a) {
                    return [a[1], a[2] / 100, a[3] / 100, a[4]]
                }
            }],
            m = a.Color = function(b, c, e, d) {
                return new a.Color.fn.parse(b, c, e, d)
            },
            n = {
                rgba: {
                    props: {
                        red: {
                            idx: 0,
                            type: "byte"
                        },
                        green: {
                            idx: 1,
                            type: "byte"
                        },
                        blue: {
                            idx: 2,
                            type: "byte"
                        }
                    }
                },
                hsla: {
                    props: {
                        hue: {
                            idx: 0,
                            type: "degrees"
                        },
                        saturation: {
                            idx: 1,
                            type: "percent"
                        },
                        lightness: {
                            idx: 2,
                            type: "percent"
                        }
                    }
                }
            },
            t = {
                "byte": {
                    floor: !0,
                    max: 255
                },
                percent: {
                    max: 1
                },
                degrees: {
                    mod: 360,
                    floor: !0
                }
            },
            r = m.support = {},
            p = a("\x3cp\x3e")[0],
            y = a.each;
        p.style.cssText = "background-color:rgba(1,1,1,.5)";
        r.rgba = -1 < p.style.backgroundColor.indexOf("rgba");
        y(n, function(a, b) {
            b.cache = "_" + a;
            b.props.alpha = {
                idx: 3,
                type: "percent",
                def: 1
            }
        });
        m.fn = a.extend(m.prototype, {
            parse: function(d, g, f, k) {
                if (d === b) return this._rgba = [null,
                    null, null, null
                ], this;
                (d.jquery || d.nodeType) && (d = a(d).css(g), g = b);
                var p = this,
                    l = a.type(d),
                    x = this._rgba = [];
                return g !== b && (d = [d, g, f, k], l = "array"), "string" === l ? this.parse(e(d) || h._default) : "array" === l ? (y(n.rgba.props, function(a, b) {
                    x[b.idx] = c(d[b.idx], b)
                }), this) : "object" === l ? (d instanceof m ? y(n, function(a, b) {
                    d[b.cache] && (p[b.cache] = d[b.cache].slice())
                }) : y(n, function(b, e) {
                    var g = e.cache;
                    y(e.props, function(a, b) {
                        if (!p[g] && e.to) {
                            if ("alpha" === a || null == d[a]) return;
                            p[g] = e.to(p._rgba)
                        }
                        p[g][b.idx] = c(d[a], b, !0)
                    });
                    p[g] && 0 > a.inArray(null, p[g].slice(0, 3)) && (p[g][3] = 1, e.from && (p._rgba = e.from(p[g])))
                }), this) : b
            },
            is: function(a) {
                var c = m(a),
                    e = !0,
                    d = this;
                return y(n, function(a, g) {
                    var h, f = c[g.cache];
                    return f && (h = d[g.cache] || g.to && g.to(d._rgba) || [], y(g.props, function(a, c) {
                        return null != f[c.idx] ? e = f[c.idx] === h[c.idx] : b
                    })), e
                }), e
            },
            _space: function() {
                var a = [],
                    b = this;
                return y(n, function(c, e) {
                    b[e.cache] && a.push(c)
                }), a.pop()
            },
            transition: function(a, b) {
                var e = m(a);
                a = e._space();
                var d = n[a],
                    g = 0 === this.alpha() ? m("transparent") : this,
                    h =
                        g[d.cache] || d.to(g._rgba),
                    f = h.slice();
                return e = e[d.cache], y(d.props, function(a, d) {
                    a = d.idx;
                    var g = h[a],
                        k = e[a],
                        p = t[d.type] || {};
                    null !== k && (null === g ? f[a] = k : (p.mod && (k - g > p.mod / 2 ? g += p.mod : g - k > p.mod / 2 && (g -= p.mod)), f[a] = c((k - g) * b + g, d)))
                }), this[a](f)
            },
            blend: function(b) {
                if (1 === this._rgba[3]) return this;
                var c = this._rgba.slice(),
                    e = c.pop(),
                    d = m(b)._rgba;
                return m(a.map(c, function(a, b) {
                    return (1 - e) * d[b] + e * a
                }))
            },
            toRgbaString: function() {
                var b = "rgba(",
                    c = a.map(this._rgba, function(a, b) {
                        return null == a ? 2 < b ? 1 : 0 : a
                    });
                return 1 ===
                c[3] && (c.pop(), b = "rgb("), b + c.join() + ")"
            },
            toHslaString: function() {
                var b = "hsla(",
                    c = a.map(this.hsla(), function(a, b) {
                        return null == a && (a = 2 < b ? 1 : 0), b && 3 > b && (a = Math.round(100 * a) + "%"), a
                    });
                return 1 === c[3] && (c.pop(), b = "hsl("), b + c.join() + ")"
            },
            toHexString: function(b) {
                var c = this._rgba.slice(),
                    e = c.pop();
                return b && c.push(~~(255 * e)), "#" + a.map(c, function(a) {
                    return a = (a || 0).toString(16), 1 === a.length ? "0" + a : a
                }).join("")
            },
            toString: function() {
                return 0 === this._rgba[3] ? "transparent" : this.toRgbaString()
            }
        });
        m.fn.parse.prototype =
            m.fn;
        n.hsla.to = function(a) {
            if (null == a[0] || null == a[1] || null == a[2]) return [null, null, null, a[3]];
            var b, c, e = a[0] / 255,
                d = a[1] / 255,
                g = a[2] / 255;
            a = a[3];
            var h = Math.max(e, d, g),
                f = Math.min(e, d, g),
                k = h - f,
                p = h + f,
                l = .5 * p;
            return b = f === h ? 0 : e === h ? 60 * (d - g) / k + 360 : d === h ? 60 * (g - e) / k + 120 : 60 * (e - d) / k + 240, c = 0 === k ? 0 : .5 >= l ? k / p : k / (2 - p), [Math.round(b) % 360, c, l, null == a ? 1 : a]
        };
        n.hsla.from = function(a) {
            if (null == a[0] || null == a[1] || null == a[2]) return [null, null, null, a[3]];
            var b = a[0] / 360,
                c = a[1],
                e = a[2];
            a = a[3];
            c = .5 >= e ? e * (1 + c) : e + c - e * c;
            e = 2 * e - c;
            return [Math.round(255 *
                g(e, c, b + 1 / 3)), Math.round(255 * g(e, c, b)), Math.round(255 * g(e, c, b - 1 / 3)), a]
        };
        y(n, function(e, d) {
            var g = d.props,
                h = d.cache,
                k = d.to,
                p = d.from;
            m.fn[e] = function(e) {
                if (k && !this[h] && (this[h] = k(this._rgba)), e === b) return this[h].slice();
                var d, f = a.type(e),
                    l = "array" === f || "object" === f ? e : arguments,
                    n = this[h].slice();
                return y(g, function(a, b) {
                    a = l["object" === f ? a : b.idx];
                    null == a && (a = n[b.idx]);
                    n[b.idx] = c(a, b)
                }), p ? (d = m(p(n)), d[h] = n, d) : m(n)
            };
            y(g, function(b, c) {
                m.fn[b] || (m.fn[b] = function(d) {
                    var g, h = a.type(d),
                        k = "alpha" === b ? this._hsla ?
                            "hsla" : "rgba" : e,
                        p = this[k](),
                        l = p[c.idx];
                    return "undefined" === h ? l : ("function" === h && (d = d.call(this, l), h = a.type(d)), null == d && c.empty ? this : ("string" === h && (g = f.exec(d), g && (d = l + parseFloat(g[2]) * ("+" === g[1] ? 1 : -1))), p[c.idx] = d, this[k](p)))
                })
            })
        });
        m.hook = function(b) {
            b = b.split(" ");
            y(b, function(b, c) {
                a.cssHooks[c] = {
                    set: function(b, d) {
                        var g, h = "";
                        if ("transparent" !== d && ("string" !== a.type(d) || (g = e(d)))) {
                            if (d = m(g || d), !r.rgba && 1 !== d._rgba[3]) {
                                for (g = "backgroundColor" === c ? b.parentNode : b;
                                     ("" === h || "transparent" === h) && g &&
                                     g.style;) try {
                                    h = a.css(g, "backgroundColor"), g = g.parentNode
                                } catch (D) {}
                                d = d.blend(h && "transparent" !== h ? h : "_default")
                            }
                            d = d.toRgbaString()
                        }
                        try {
                            b.style[c] = d
                        } catch (D) {}
                    }
                };
                a.fx.step[c] = function(b) {
                    b.colorInit || (b.start = m(b.elem, c), b.end = m(b.end), b.colorInit = !0);
                    a.cssHooks[c].set(b.elem, b.start.transition(b.end, b.pos))
                }
            })
        };
        m.hook("backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor");
        a.cssHooks.borderColor = {
            expand: function(a) {
                var b = {};
                return y(["Top", "Right", "Bottom", "Left"], function(c, e) {
                    b["border" + e + "Color"] = a
                }), b
            }
        };
        h = a.Color.names = {
            aqua: "#00ffff",
            black: "#000000",
            blue: "#0000ff",
            fuchsia: "#ff00ff",
            gray: "#808080",
            green: "#008000",
            lime: "#00ff00",
            maroon: "#800000",
            navy: "#000080",
            olive: "#808000",
            purple: "#800080",
            red: "#ff0000",
            silver: "#c0c0c0",
            teal: "#008080",
            white: "#ffffff",
            yellow: "#ffff00",
            transparent: [null, null, null, 0],
            _default: "#ffffff"
        }
    })(jQuery);
    (function() {
        function f(a) {
            var c, e = a.ownerDocument.defaultView ?
                a.ownerDocument.defaultView.getComputedStyle(a, null) : a.currentStyle,
                d = {};
            if (e && e.length && e[0] && e[e[0]])
                for (a = e.length; a--;) c = e[a], "string" == typeof e[c] && (d[b.camelCase(c)] = e[c]);
            else
                for (c in e) "string" == typeof e[c] && (d[c] = e[c]);
            return d
        }
        var c = ["add", "remove", "toggle"],
            d = {
                border: 1,
                borderBottom: 1,
                borderColor: 1,
                borderLeft: 1,
                borderRight: 1,
                borderTop: 1,
                borderWidth: 1,
                margin: 1,
                padding: 1
            };
        b.each(["borderLeftStyle", "borderRightStyle", "borderBottomStyle", "borderTopStyle"], function(a, c) {
            b.fx.step[c] = function(a) {
                ("none" !==
                    a.end && !a.setAttr || 1 === a.pos && !a.setAttr) && (jQuery.style(a.elem, c, a.end), a.setAttr = !0)
            }
        });
        b.fn.addBack || (b.fn.addBack = function(a) {
            return this.add(null == a ? this.prevObject : this.prevObject.filter(a))
        });
        b.effects.animateClass = function(a, g, h, k) {
            var e = b.speed(g, h, k);
            return this.queue(function() {
                var g, h = b(this),
                    k = h.attr("class") || "",
                    l = e.children ? h.find("*").addBack() : h,
                    l = l.map(function() {
                        return {
                            el: b(this),
                            start: f(this)
                        }
                    });
                g = function() {
                    b.each(c, function(b, c) {
                        a[c] && h[c + "Class"](a[c])
                    })
                };
                g();
                l = l.map(function() {
                    this.end =
                        f(this.el[0]);
                    var a = this.start,
                        c = this.end,
                        e, g, h = {};
                    for (e in c) g = c[e], a[e] !== g && (d[e] || (b.fx.step[e] || !isNaN(parseFloat(g))) && (h[e] = g));
                    return this.diff = h, this
                });
                h.attr("class", k);
                l = l.map(function() {
                    var a = this,
                        c = b.Deferred(),
                        d = b.extend({}, e, {
                            queue: !1,
                            complete: function() {
                                c.resolve(a)
                            }
                        });
                    return this.el.animate(this.diff, d), c.promise()
                });
                b.when.apply(b, l.get()).done(function() {
                    g();
                    b.each(arguments, function() {
                        var a = this.el;
                        b.each(this.diff, function(b) {
                            a.css(b, "")
                        })
                    });
                    e.complete.call(h[0])
                })
            })
        };
        b.fn.extend({
            addClass: function(a) {
                return function(c,
                                e, d, f) {
                    return e ? b.effects.animateClass.call(this, {
                        add: c
                    }, e, d, f) : a.apply(this, arguments)
                }
            }(b.fn.addClass),
            removeClass: function(a) {
                return function(c, e, d, f) {
                    return 1 < arguments.length ? b.effects.animateClass.call(this, {
                        remove: c
                    }, e, d, f) : a.apply(this, arguments)
                }
            }(b.fn.removeClass),
            toggleClass: function(c) {
                return function(e, d, f, l, m) {
                    return "boolean" == typeof d || d === a ? f ? b.effects.animateClass.call(this, d ? {
                        add: e
                    } : {
                        remove: e
                    }, f, l, m) : c.apply(this, arguments) : b.effects.animateClass.call(this, {
                        toggle: e
                    }, d, f, l)
                }
            }(b.fn.toggleClass),
            switchClass: function(a, c, d, f, l) {
                return b.effects.animateClass.call(this, {
                    add: c,
                    remove: a
                }, d, f, l)
            }
        })
    })();
    (function() {
        function f(a, c, g, h) {
            return b.isPlainObject(a) && (c = a, a = a.effect), a = {
                effect: a
            }, null == c && (c = {}), b.isFunction(c) && (h = c, g = null, c = {}), ("number" == typeof c || b.fx.speeds[c]) && (h = g, g = c, c = {}), b.isFunction(g) && (h = g, g = null), c && b.extend(a, c), g = g || c.duration, a.duration = b.fx.off ? 0 : "number" == typeof g ? g : g in b.fx.speeds ? b.fx.speeds[g] : b.fx.speeds._default, a.complete = h || c.complete, a
        }

        function c(a) {
            return !a ||
            "number" == typeof a || b.fx.speeds[a] ? !0 : "string" != typeof a || b.effects.effect[a] ? b.isFunction(a) ? !0 : "object" != typeof a || a.effect ? !1 : !0 : !0
        }
        b.extend(b.effects, {
            version: "1.10.3",
            save: function(a, b) {
                for (var c = 0; b.length > c; c++) null !== b[c] && a.data("ui-effects-" + b[c], a[0].style[b[c]])
            },
            restore: function(b, c) {
                var e, d;
                for (d = 0; c.length > d; d++) null !== c[d] && (e = b.data("ui-effects-" + c[d]), e === a && (e = ""), b.css(c[d], e))
            },
            setMode: function(a, b) {
                return "toggle" === b && (b = a.is(":hidden") ? "show" : "hide"), b
            },
            getBaseline: function(a,
                                  b) {
                var c;
                switch (a[0]) {
                    case "top":
                        c = 0;
                        break;
                    case "middle":
                        c = .5;
                        break;
                    case "bottom":
                        c = 1;
                        break;
                    default:
                        c = a[0] / b.height
                }
                switch (a[1]) {
                    case "left":
                        a = 0;
                        break;
                    case "center":
                        a = .5;
                        break;
                    case "right":
                        a = 1;
                        break;
                    default:
                        a = a[1] / b.width
                }
                return {
                    x: a,
                    y: c
                }
            },
            createWrapper: function(a) {
                if (a.parent().is(".ui-effects-wrapper")) return a.parent();
                var c = {
                        width: a.outerWidth(!0),
                        height: a.outerHeight(!0),
                        "float": a.css("float")
                    },
                    d = b("\x3cdiv\x3e\x3c/div\x3e").addClass("ui-effects-wrapper").css({
                        fontSize: "100%",
                        background: "transparent",
                        border: "none",
                        margin: 0,
                        padding: 0
                    }),
                    h = {
                        width: a.width(),
                        height: a.height()
                    },
                    f = document.activeElement;
                try {
                    f.id
                } catch (l) {
                    f = document.body
                }
                return a.wrap(d), (a[0] === f || b.contains(a[0], f)) && b(f).focus(), d = a.parent(), "static" === a.css("position") ? (d.css({
                    position: "relative"
                }), a.css({
                    position: "relative"
                })) : (b.extend(c, {
                    position: a.css("position"),
                    zIndex: a.css("z-index")
                }), b.each(["top", "left", "bottom", "right"], function(b, e) {
                    c[e] = a.css(e);
                    isNaN(parseInt(c[e], 10)) && (c[e] = "auto")
                }), a.css({
                    position: "relative",
                    top: 0,
                    left: 0,
                    right: "auto",
                    bottom: "auto"
                })), a.css(h), d.css(c).show()
            },
            removeWrapper: function(a) {
                var c = document.activeElement;
                return a.parent().is(".ui-effects-wrapper") && (a.parent().replaceWith(a), (a[0] === c || b.contains(a[0], c)) && b(c).focus()), a
            },
            setTransition: function(a, c, g, h) {
                return h = h || {}, b.each(c, function(b, c) {
                    b = a.cssUnit(c);
                    0 < b[0] && (h[c] = b[0] * g + b[1])
                }), h
            }
        });
        b.fn.extend({
            effect: function() {
                function a(a) {
                    function e() {
                        b.isFunction(g) && g.call(d[0]);
                        b.isFunction(a) && a()
                    }
                    var d = b(this),
                        g = c.complete,
                        h = c.mode;
                    (d.is(":hidden") ? "hide" === h : "show" === h) ? (d[h](), e()) : k.call(d[0], c, e)
                }
                var c = f.apply(this, arguments),
                    g = c.mode,
                    h = c.queue,
                    k = b.effects.effect[c.effect];
                return b.fx.off || !k ? g ? this[g](c.duration, c.complete) : this.each(function() {
                    c.complete && c.complete.call(this)
                }) : !1 === h ? this.each(a) : this.queue(h || "fx", a)
            },
            show: function(a) {
                return function(b) {
                    if (c(b)) return a.apply(this, arguments);
                    var e = f.apply(this, arguments);
                    return e.mode = "show", this.effect.call(this, e)
                }
            }(b.fn.show),
            hide: function(a) {
                return function(b) {
                    if (c(b)) return a.apply(this,
                        arguments);
                    var e = f.apply(this, arguments);
                    return e.mode = "hide", this.effect.call(this, e)
                }
            }(b.fn.hide),
            toggle: function(a) {
                return function(b) {
                    if (c(b) || "boolean" == typeof b) return a.apply(this, arguments);
                    var e = f.apply(this, arguments);
                    return e.mode = "toggle", this.effect.call(this, e)
                }
            }(b.fn.toggle),
            cssUnit: function(a) {
                var c = this.css(a),
                    d = [];
                return b.each(["em", "px", "%", "pt"], function(a, b) {
                    0 < c.indexOf(b) && (d = [parseFloat(c), b])
                }), d
            }
        })
    })();
    (function() {
        var a = {};
        b.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function(b,
                                                                     d) {
            a[d] = function(a) {
                return Math.pow(a, b + 2)
            }
        });
        b.extend(a, {
            Sine: function(a) {
                return 1 - Math.cos(a * Math.PI / 2)
            },
            Circ: function(a) {
                return 1 - Math.sqrt(1 - a * a)
            },
            Elastic: function(a) {
                return 0 === a || 1 === a ? a : -Math.pow(2, 8 * (a - 1)) * Math.sin((80 * (a - 1) - 7.5) * Math.PI / 15)
            },
            Back: function(a) {
                return a * a * (3 * a - 2)
            },
            Bounce: function(a) {
                for (var b, c = 4;
                     ((b = Math.pow(2, --c)) - 1) / 11 > a;);
                return 1 / Math.pow(4, 3 - c) - 7.5625 * Math.pow((3 * b - 2) / 22 - a, 2)
            }
        });
        b.each(a, function(a, d) {
            b.easing["easeIn" + a] = d;
            b.easing["easeOut" + a] = function(a) {
                return 1 -
                    d(1 - a)
            };
            b.easing["easeInOut" + a] = function(a) {
                return .5 > a ? d(2 * a) / 2 : 1 - d(-2 * a + 2) / 2
            }
        })
    })()
})(jQuery);
(function(b) {
    var a = 0,
        f = {},
        c = {};
    f.height = f.paddingTop = f.paddingBottom = f.borderTopWidth = f.borderBottomWidth = "hide";
    c.height = c.paddingTop = c.paddingBottom = c.borderTopWidth = c.borderBottomWidth = "show";
    b.widget("ui.accordion", {
        version: "1.10.3",
        options: {
            active: 0,
            animate: {},
            collapsible: !1,
            event: "click",
            header: "\x3e li \x3e :first-child,\x3e :not(li):even",
            heightStyle: "auto",
            icons: {
                activeHeader: "ui-icon-triangle-1-s",
                header: "ui-icon-triangle-1-e"
            },
            activate: null,
            beforeActivate: null
        },
        _create: function() {
            var a =
                this.options;
            this.prevShow = this.prevHide = b();
            this.element.addClass("ui-accordion ui-widget ui-helper-reset").attr("role", "tablist");
            a.collapsible || !1 !== a.active && null != a.active || (a.active = 0);
            this._processPanels();
            0 > a.active && (a.active += this.headers.length);
            this._refresh()
        },
        _getCreateEventData: function() {
            return {
                header: this.active,
                panel: this.active.length ? this.active.next() : b(),
                content: this.active.length ? this.active.next() : b()
            }
        },
        _createIcons: function() {
            var a = this.options.icons;
            a && (b("\x3cspan\x3e").addClass("ui-accordion-header-icon ui-icon " +
                a.header).prependTo(this.headers), this.active.children(".ui-accordion-header-icon").removeClass(a.header).addClass(a.activeHeader), this.headers.addClass("ui-accordion-icons"))
        },
        _destroyIcons: function() {
            this.headers.removeClass("ui-accordion-icons").children(".ui-accordion-header-icon").remove()
        },
        _destroy: function() {
            var a;
            this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role");
            this.headers.removeClass("ui-accordion-header ui-accordion-header-active ui-helper-reset ui-state-default ui-corner-all ui-state-active ui-state-disabled ui-corner-top").removeAttr("role").removeAttr("aria-selected").removeAttr("aria-controls").removeAttr("tabIndex").each(function() {
                /^ui-accordion/.test(this.id) &&
                this.removeAttribute("id")
            });
            this._destroyIcons();
            a = this.headers.next().css("display", "").removeAttr("role").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-labelledby").removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active ui-state-disabled").each(function() {
                /^ui-accordion/.test(this.id) && this.removeAttribute("id")
            });
            "content" !== this.options.heightStyle && a.css("height", "")
        },
        _setOption: function(a, b) {
            return "active" ===
            a ? (this._activate(b), void 0) : ("event" === a && (this.options.event && this._off(this.headers, this.options.event), this._setupEvents(b)), this._super(a, b), "collapsible" !== a || b || !1 !== this.options.active || this._activate(0), "icons" === a && (this._destroyIcons(), b && this._createIcons()), "disabled" === a && this.headers.add(this.headers.next()).toggleClass("ui-state-disabled", !!b), void 0)
        },
        _keydown: function(a) {
            if (!a.altKey && !a.ctrlKey) {
                var c = b.ui.keyCode,
                    d = this.headers.length,
                    h = this.headers.index(a.target),
                    f = !1;
                switch (a.keyCode) {
                    case c.RIGHT:
                    case c.DOWN:
                        f =
                            this.headers[(h + 1) % d];
                        break;
                    case c.LEFT:
                    case c.UP:
                        f = this.headers[(h - 1 + d) % d];
                        break;
                    case c.SPACE:
                    case c.ENTER:
                        this._eventHandler(a);
                        break;
                    case c.HOME:
                        f = this.headers[0];
                        break;
                    case c.END:
                        f = this.headers[d - 1]
                }
                f && (b(a.target).attr("tabIndex", -1), b(f).attr("tabIndex", 0), f.focus(), a.preventDefault())
            }
        },
        _panelKeyDown: function(a) {
            a.keyCode === b.ui.keyCode.UP && a.ctrlKey && b(a.currentTarget).prev().focus()
        },
        refresh: function() {
            var a = this.options;
            this._processPanels();
            !1 === a.active && !0 === a.collapsible || !this.headers.length ?
                (a.active = !1, this.active = b()) : !1 === a.active ? this._activate(0) : this.active.length && !b.contains(this.element[0], this.active[0]) ? this.headers.length === this.headers.find(".ui-state-disabled").length ? (a.active = !1, this.active = b()) : this._activate(Math.max(0, a.active - 1)) : a.active = this.headers.index(this.active);
            this._destroyIcons();
            this._refresh()
        },
        _processPanels: function() {
            this.headers = this.element.find(this.options.header).addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-all");
            this.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").filter(":not(.ui-accordion-content-active)").hide()
        },
        _refresh: function() {
            var c, e = this.options,
                g = e.heightStyle,
                h = this.element.parent(),
                f = this.accordionId = "ui-accordion-" + (this.element.attr("id") || ++a);
            this.active = this._findActive(e.active).addClass("ui-accordion-header-active ui-state-active ui-corner-top").removeClass("ui-corner-all");
            this.active.next().addClass("ui-accordion-content-active").show();
            this.headers.attr("role", "tab").each(function(a) {
                var c = b(this),
                    e = c.attr("id"),
                    d = c.next(),
                    g = d.attr("id");
                e || (e = f + "-header-" + a, c.attr("id", e));
                g || (g = f + "-panel-" +
                    a, d.attr("id", g));
                c.attr("aria-controls", g);
                d.attr("aria-labelledby", e)
            }).next().attr("role", "tabpanel");
            this.headers.not(this.active).attr({
                "aria-selected": "false",
                tabIndex: -1
            }).next().attr({
                "aria-expanded": "false",
                "aria-hidden": "true"
            }).hide();
            this.active.length ? this.active.attr({
                "aria-selected": "true",
                tabIndex: 0
            }).next().attr({
                "aria-expanded": "true",
                "aria-hidden": "false"
            }) : this.headers.eq(0).attr("tabIndex", 0);
            this._createIcons();
            this._setupEvents(e.event);
            "fill" === g ? (c = h.height(), this.element.siblings(":visible").each(function() {
                var a =
                        b(this),
                    e = a.css("position");
                "absolute" !== e && "fixed" !== e && (c -= a.outerHeight(!0))
            }), this.headers.each(function() {
                c -= b(this).outerHeight(!0)
            }), this.headers.next().each(function() {
                b(this).height(Math.max(0, c - b(this).innerHeight() + b(this).height()))
            }).css("overflow", "auto")) : "auto" === g && (c = 0, this.headers.next().each(function() {
                c = Math.max(c, b(this).css("height", "").height())
            }).height(c))
        },
        _activate: function(a) {
            a = this._findActive(a)[0];
            a !== this.active[0] && (a = a || this.active[0], this._eventHandler({
                target: a,
                currentTarget: a,
                preventDefault: b.noop
            }))
        },
        _findActive: function(a) {
            return "number" == typeof a ? this.headers.eq(a) : b()
        },
        _setupEvents: function(a) {
            var c = {
                keydown: "_keydown"
            };
            a && b.each(a.split(" "), function(a, b) {
                c[b] = "_eventHandler"
            });
            this._off(this.headers.add(this.headers.next()));
            this._on(this.headers, c);
            this._on(this.headers.next(), {
                keydown: "_panelKeyDown"
            });
            this._hoverable(this.headers);
            this._focusable(this.headers)
        },
        _eventHandler: function(a) {
            var c = this.options,
                d = this.active,
                h = b(a.currentTarget),
                f = h[0] ===
                    d[0],
                l = f && c.collapsible,
                m = l ? b() : h.next(),
                n = d.next(),
                m = {
                    oldHeader: d,
                    oldPanel: n,
                    newHeader: l ? b() : h,
                    newPanel: m
                };
            a.preventDefault();
            f && !c.collapsible || !1 === this._trigger("beforeActivate", a, m) || (c.active = l ? !1 : this.headers.index(h), this.active = f ? b() : h, this._toggle(m), d.removeClass("ui-accordion-header-active ui-state-active"), c.icons && d.children(".ui-accordion-header-icon").removeClass(c.icons.activeHeader).addClass(c.icons.header), f || (h.removeClass("ui-corner-all").addClass("ui-accordion-header-active ui-state-active ui-corner-top"),
            c.icons && h.children(".ui-accordion-header-icon").removeClass(c.icons.header).addClass(c.icons.activeHeader), h.next().addClass("ui-accordion-content-active")))
        },
        _toggle: function(a) {
            var c = a.newPanel,
                d = this.prevShow.length ? this.prevShow : a.oldPanel;
            this.prevShow.add(this.prevHide).stop(!0, !0);
            this.prevShow = c;
            this.prevHide = d;
            this.options.animate ? this._animate(c, d, a) : (d.hide(), c.show(), this._toggleComplete(a));
            d.attr({
                "aria-expanded": "false",
                "aria-hidden": "true"
            });
            d.prev().attr("aria-selected", "false");
            c.length && d.length ? d.prev().attr("tabIndex", -1) : c.length && this.headers.filter(function() {
                return 0 === b(this).attr("tabIndex")
            }).attr("tabIndex", -1);
            c.attr({
                "aria-expanded": "true",
                "aria-hidden": "false"
            }).prev().attr({
                "aria-selected": "true",
                tabIndex: 0
            })
        },
        _animate: function(a, b, g) {
            var e, d, l, m = this,
                n = 0,
                t = a.length && (!b.length || a.index() < b.index()),
                r = this.options.animate || {},
                t = t && r.down || r,
                p = function() {
                    m._toggleComplete(g)
                };
            return "number" == typeof t && (l = t), "string" == typeof t && (d = t), d = d || t.easing || r.easing,
                l = l || t.duration || r.duration, b.length ? a.length ? (e = a.show().outerHeight(), b.animate(f, {
                duration: l,
                easing: d,
                step: function(a, b) {
                    b.now = Math.round(a)
                }
            }), a.hide().animate(c, {
                duration: l,
                easing: d,
                complete: p,
                step: function(a, c) {
                    c.now = Math.round(a);
                    "height" !== c.prop ? n += c.now : "content" !== m.options.heightStyle && (c.now = Math.round(e - b.outerHeight() - n), n = 0)
                }
            }), void 0) : b.animate(f, l, d, p) : a.animate(c, l, d, p)
        },
        _toggleComplete: function(a) {
            var b = a.oldPanel;
            b.removeClass("ui-accordion-content-active").prev().removeClass("ui-corner-top").addClass("ui-corner-all");
            b.length && (b.parent()[0].className = b.parent()[0].className);
            this._trigger("activate", null, a)
        }
    })
})(jQuery);
(function(b) {
    var a = 0;
    b.widget("ui.autocomplete", {
        version: "1.10.3",
        defaultElement: "\x3cinput\x3e",
        options: {
            appendTo: null,
            autoFocus: !1,
            delay: 300,
            minLength: 1,
            position: {
                my: "left top",
                at: "left bottom",
                collision: "none"
            },
            source: null,
            change: null,
            close: null,
            focus: null,
            open: null,
            response: null,
            search: null,
            select: null
        },
        pending: 0,
        _create: function() {
            var a, c, d, e = this.element[0].nodeName.toLowerCase(),
                g = "textarea" === e,
                e = "input" === e;
            this.isMultiLine = g ? !0 : e ? !1 : this.element.prop("isContentEditable");
            this.valueMethod =
                this.element[g || e ? "val" : "text"];
            this.isNewMenu = !0;
            this.element.addClass("ui-autocomplete-input").attr("autocomplete", "off");
            this._on(this.element, {
                keydown: function(e) {
                    if (this.element.prop("readOnly")) return a = !0, d = !0, c = !0, void 0;
                    c = d = a = !1;
                    var g = b.ui.keyCode;
                    switch (e.keyCode) {
                        case g.PAGE_UP:
                            a = !0;
                            this._move("previousPage", e);
                            break;
                        case g.PAGE_DOWN:
                            a = !0;
                            this._move("nextPage", e);
                            break;
                        case g.UP:
                            a = !0;
                            this._keyEvent("previous", e);
                            break;
                        case g.DOWN:
                            a = !0;
                            this._keyEvent("next", e);
                            break;
                        case g.ENTER:
                        case g.NUMPAD_ENTER:
                            this.menu.active &&
                            (a = !0, e.preventDefault(), this.menu.select(e));
                            break;
                        case g.TAB:
                            this.menu.active && this.menu.select(e);
                            break;
                        case g.ESCAPE:
                            this.menu.element.is(":visible") && (this._value(this.term), this.close(e), e.preventDefault());
                            break;
                        default:
                            c = !0, this._searchTimeout(e)
                    }
                },
                keypress: function(e) {
                    if (a) return a = !1, (!this.isMultiLine || this.menu.element.is(":visible")) && e.preventDefault(), void 0;
                    if (!c) {
                        var d = b.ui.keyCode;
                        switch (e.keyCode) {
                            case d.PAGE_UP:
                                this._move("previousPage", e);
                                break;
                            case d.PAGE_DOWN:
                                this._move("nextPage",
                                    e);
                                break;
                            case d.UP:
                                this._keyEvent("previous", e);
                                break;
                            case d.DOWN:
                                this._keyEvent("next", e)
                        }
                    }
                },
                input: function(a) {
                    return d ? (d = !1, a.preventDefault(), void 0) : (this._searchTimeout(a), void 0)
                },
                focus: function() {
                    this.selectedItem = null;
                    this.previous = this._value()
                },
                blur: function(a) {
                    return this.cancelBlur ? (delete this.cancelBlur, void 0) : (clearTimeout(this.searching), this.close(a), this._change(a), void 0)
                }
            });
            this._initSource();
            this.menu = b("\x3cul\x3e").addClass("ui-autocomplete ui-front").appendTo(this._appendTo()).menu({
                role: null
            }).hide().data("ui-menu");
            this._on(this.menu.element, {
                mousedown: function(a) {
                    a.preventDefault();
                    this.cancelBlur = !0;
                    this._delay(function() {
                        delete this.cancelBlur
                    });
                    var c = this.menu.element[0];
                    b(a.target).closest(".ui-menu-item").length || this._delay(function() {
                        var a = this;
                        this.document.one("mousedown", function(e) {
                            e.target === a.element[0] || e.target === c || b.contains(c, e.target) || a.close()
                        })
                    })
                },
                menufocus: function(a, c) {
                    if (this.isNewMenu && (this.isNewMenu = !1, a.originalEvent && /^mouse/.test(a.originalEvent.type))) return this.menu.blur(),
                        this.document.one("mousemove", function() {
                            b(a.target).trigger(a.originalEvent)
                        }), void 0;
                    c = c.item.data("ui-autocomplete-item");
                    !1 !== this._trigger("focus", a, {
                        item: c
                    }) ? a.originalEvent && /^key/.test(a.originalEvent.type) && this._value(c.value) : this.liveRegion.text(c.value)
                },
                menuselect: function(a, b) {
                    var c = b.item.data("ui-autocomplete-item"),
                        e = this.previous;
                    this.element[0] !== this.document[0].activeElement && (this.element.focus(), this.previous = e, this._delay(function() {
                        this.previous = e;
                        this.selectedItem = c
                    }));
                    !1 !== this._trigger("select", a, {
                        item: c
                    }) && this._value(c.value);
                    this.term = this._value();
                    this.close(a);
                    this.selectedItem = c
                }
            });
            this.liveRegion = b("\x3cspan\x3e", {
                role: "status",
                "aria-live": "polite"
            }).addClass("ui-helper-hidden-accessible").insertBefore(this.element);
            this._on(this.window, {
                beforeunload: function() {
                    this.element.removeAttr("autocomplete")
                }
            })
        },
        _destroy: function() {
            clearTimeout(this.searching);
            this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete");
            this.menu.element.remove();
            this.liveRegion.remove()
        },
        _setOption: function(a, b) {
            this._super(a, b);
            "source" === a && this._initSource();
            "appendTo" === a && this.menu.element.appendTo(this._appendTo());
            "disabled" === a && b && this.xhr && this.xhr.abort()
        },
        _appendTo: function() {
            var a = this.options.appendTo;
            return a && (a = a.jquery || a.nodeType ? b(a) : this.document.find(a).eq(0)), a || (a = this.element.closest(".ui-front")), a.length || (a = this.document[0].body), a
        },
        _initSource: function() {
            var a, c, d = this;
            b.isArray(this.options.source) ? (a = this.options.source, this.source =
                function(c, d) {
                    d(b.ui.autocomplete.filter(a, c.term))
                }) : "string" == typeof this.options.source ? (c = this.options.source, this.source = function(a, g) {
                d.xhr && d.xhr.abort();
                d.xhr = b.ajax({
                    url: c,
                    data: a,
                    dataType: "json",
                    success: function(a) {
                        g(a)
                    },
                    error: function() {
                        g([])
                    }
                })
            }) : this.source = this.options.source
        },
        _searchTimeout: function(a) {
            clearTimeout(this.searching);
            this.searching = this._delay(function() {
                this.term !== this._value() && (this.selectedItem = null, this.search(null, a))
            }, this.options.delay)
        },
        search: function(a, b) {
            return a =
                null != a ? a : this._value(), this.term = this._value(), a.length < this.options.minLength ? this.close(b) : !1 !== this._trigger("search", b) ? this._search(a) : void 0
        },
        _search: function(a) {
            this.pending++;
            this.element.addClass("ui-autocomplete-loading");
            this.cancelSearch = !1;
            this.source({
                term: a
            }, this._response())
        },
        _response: function() {
            var b = this,
                c = ++a;
            return function(d) {
                c === a && b.__response(d);
                b.pending--;
                b.pending || b.element.removeClass("ui-autocomplete-loading")
            }
        },
        __response: function(a) {
            a && (a = this._normalize(a));
            this._trigger("response",
                null, {
                    content: a
                });
            !this.options.disabled && a && a.length && !this.cancelSearch ? (this._suggest(a), this._trigger("open")) : this._close()
        },
        close: function(a) {
            this.cancelSearch = !0;
            this._close(a)
        },
        _close: function(a) {
            this.menu.element.is(":visible") && (this.menu.element.hide(), this.menu.blur(), this.isNewMenu = !0, this._trigger("close", a))
        },
        _change: function(a) {
            this.previous !== this._value() && this._trigger("change", a, {
                item: this.selectedItem
            })
        },
        _normalize: function(a) {
            return a.length && a[0].label && a[0].value ? a : b.map(a,
                function(a) {
                    return "string" == typeof a ? {
                        label: a,
                        value: a
                    } : b.extend({
                        label: a.label || a.value,
                        value: a.value || a.label
                    }, a)
                })
        },
        _suggest: function(a) {
            var c = this.menu.element.empty();
            this._renderMenu(c, a);
            this.isNewMenu = !0;
            this.menu.refresh();
            c.show();
            this._resizeMenu();
            c.position(b.extend({
                of: this.element
            }, this.options.position));
            this.options.autoFocus && this.menu.next()
        },
        _resizeMenu: function() {
            var a = this.menu.element;
            a.outerWidth(Math.max(a.width("").outerWidth() + 1, this.element.outerWidth()))
        },
        _renderMenu: function(a,
                              c) {
            var d = this;
            b.each(c, function(b, c) {
                d._renderItemData(a, c)
            })
        },
        _renderItemData: function(a, b) {
            return this._renderItem(a, b).data("ui-autocomplete-item", b)
        },
        _renderItem: function(a, c) {
            return b("\x3cli\x3e").append(b("\x3ca\x3e").text(c.label)).appendTo(a)
        },
        _move: function(a, b) {
            return this.menu.element.is(":visible") ? this.menu.isFirstItem() && /^previous/.test(a) || this.menu.isLastItem() && /^next/.test(a) ? (this._value(this.term), this.menu.blur(), void 0) : (this.menu[a](b), void 0) : (this.search(null, b), void 0)
        },
        widget: function() {
            return this.menu.element
        },
        _value: function() {
            return this.valueMethod.apply(this.element, arguments)
        },
        _keyEvent: function(a, b) {
            this.isMultiLine && !this.menu.element.is(":visible") || (this._move(a, b), b.preventDefault())
        }
    });
    b.extend(b.ui.autocomplete, {
        escapeRegex: function(a) {
            return a.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$\x26")
        },
        filter: function(a, c) {
            var d = RegExp(b.ui.autocomplete.escapeRegex(c), "i");
            return b.grep(a, function(a) {
                return d.test(a.label || a.value || a)
            })
        }
    });
    b.widget("ui.autocomplete",
        b.ui.autocomplete, {
            options: {
                messages: {
                    noResults: "No search results.",
                    results: function(a) {
                        return a + (1 < a ? " results are" : " result is") + " available, use up and down arrow keys to navigate."
                    }
                }
            },
            __response: function(a) {
                var b;
                this._superApply(arguments);
                this.options.disabled || this.cancelSearch || (b = a && a.length ? this.options.messages.results(a.length) : this.options.messages.noResults, this.liveRegion.text(b))
            }
        })
})(jQuery);
(function(b) {
    var a, f, c, d, e = function() {
            var a = b(this);
            setTimeout(function() {
                a.find(":ui-button").button("refresh")
            }, 1)
        },
        g = function(a) {
            var c = a.name,
                e = a.form,
                d = b([]);
            return c && (c = c.replace(/'/g, "\\'"), d = e ? b(e).find("[name\x3d'" + c + "']") : b("[name\x3d'" + c + "']", a.ownerDocument).filter(function() {
                return !this.form
            })), d
        };
    b.widget("ui.button", {
        version: "1.10.3",
        defaultElement: "\x3cbutton\x3e",
        options: {
            disabled: null,
            text: !0,
            label: null,
            icons: {
                primary: null,
                secondary: null
            }
        },
        _create: function() {
            this.element.closest("form").unbind("reset" +
                this.eventNamespace).bind("reset" + this.eventNamespace, e);
            "boolean" != typeof this.options.disabled ? this.options.disabled = !!this.element.prop("disabled") : this.element.prop("disabled", this.options.disabled);
            this._determineButtonType();
            this.hasTitle = !!this.buttonElement.attr("title");
            var h = this,
                k = this.options,
                l = "checkbox" === this.type || "radio" === this.type,
                m = l ? "" : "ui-state-active";
            null === k.label && (k.label = "input" === this.type ? this.buttonElement.val() : this.buttonElement.html());
            this._hoverable(this.buttonElement);
            this.buttonElement.addClass("ui-button ui-widget ui-state-default ui-corner-all").attr("role", "button").bind("mouseenter" + this.eventNamespace, function() {
                k.disabled || this === a && b(this).addClass("ui-state-active")
            }).bind("mouseleave" + this.eventNamespace, function() {
                k.disabled || b(this).removeClass(m)
            }).bind("click" + this.eventNamespace, function(a) {
                k.disabled && (a.preventDefault(), a.stopImmediatePropagation())
            });
            this.element.bind("focus" + this.eventNamespace, function() {
                h.buttonElement.addClass("ui-state-focus")
            }).bind("blur" +
                this.eventNamespace,
                function() {
                    h.buttonElement.removeClass("ui-state-focus")
                });
            l && (this.element.bind("change" + this.eventNamespace, function() {
                d || h.refresh()
            }), this.buttonElement.bind("mousedown" + this.eventNamespace, function(a) {
                k.disabled || (d = !1, f = a.pageX, c = a.pageY)
            }).bind("mouseup" + this.eventNamespace, function(a) {
                k.disabled || (f !== a.pageX || c !== a.pageY) && (d = !0)
            }));
            "checkbox" === this.type ? this.buttonElement.bind("click" + this.eventNamespace, function() {
                return k.disabled || d ? !1 : void 0
            }) : "radio" === this.type ?
                this.buttonElement.bind("click" + this.eventNamespace, function() {
                    if (k.disabled || d) return !1;
                    b(this).addClass("ui-state-active");
                    h.buttonElement.attr("aria-pressed", "true");
                    var a = h.element[0];
                    g(a).not(a).map(function() {
                        return b(this).button("widget")[0]
                    }).removeClass("ui-state-active").attr("aria-pressed", "false")
                }) : (this.buttonElement.bind("mousedown" + this.eventNamespace, function() {
                    return k.disabled ? !1 : (b(this).addClass("ui-state-active"), a = this, h.document.one("mouseup", function() {
                        a = null
                    }), void 0)
                }).bind("mouseup" +
                    this.eventNamespace,
                    function() {
                        return k.disabled ? !1 : (b(this).removeClass("ui-state-active"), void 0)
                    }).bind("keydown" + this.eventNamespace, function(a) {
                    return k.disabled ? !1 : ((a.keyCode === b.ui.keyCode.SPACE || a.keyCode === b.ui.keyCode.ENTER) && b(this).addClass("ui-state-active"), void 0)
                }).bind("keyup" + this.eventNamespace + " blur" + this.eventNamespace, function() {
                    b(this).removeClass("ui-state-active")
                }), this.buttonElement.is("a") && this.buttonElement.keyup(function(a) {
                    a.keyCode === b.ui.keyCode.SPACE && b(this).click()
                }));
            this._setOption("disabled", k.disabled);
            this._resetButton()
        },
        _determineButtonType: function() {
            var a, b, c;
            this.type = this.element.is("[type\x3dcheckbox]") ? "checkbox" : this.element.is("[type\x3dradio]") ? "radio" : this.element.is("input") ? "input" : "button";
            "checkbox" === this.type || "radio" === this.type ? (a = this.element.parents().last(), b = "label[for\x3d'" + this.element.attr("id") + "']", this.buttonElement = a.find(b), this.buttonElement.length || (a = a.length ? a.siblings() : this.element.siblings(), this.buttonElement = a.filter(b),
            this.buttonElement.length || (this.buttonElement = a.find(b))), this.element.addClass("ui-helper-hidden-accessible"), c = this.element.is(":checked"), c && this.buttonElement.addClass("ui-state-active"), this.buttonElement.prop("aria-pressed", c)) : this.buttonElement = this.element
        },
        widget: function() {
            return this.buttonElement
        },
        _destroy: function() {
            this.element.removeClass("ui-helper-hidden-accessible");
            this.buttonElement.removeClass("ui-button ui-widget ui-state-default ui-corner-all ui-state-hover ui-state-active  ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only").removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html());
            this.hasTitle || this.buttonElement.removeAttr("title")
        },
        _setOption: function(a, b) {
            return this._super(a, b), "disabled" === a ? (b ? this.element.prop("disabled", !0) : this.element.prop("disabled", !1), void 0) : (this._resetButton(), void 0)
        },
        refresh: function() {
            var a = this.element.is("input, button") ? this.element.is(":disabled") : this.element.hasClass("ui-button-disabled");
            a !== this.options.disabled && this._setOption("disabled", a);
            "radio" === this.type ? g(this.element[0]).each(function() {
                b(this).is(":checked") ? b(this).button("widget").addClass("ui-state-active").attr("aria-pressed",
                    "true") : b(this).button("widget").removeClass("ui-state-active").attr("aria-pressed", "false")
            }) : "checkbox" === this.type && (this.element.is(":checked") ? this.buttonElement.addClass("ui-state-active").attr("aria-pressed", "true") : this.buttonElement.removeClass("ui-state-active").attr("aria-pressed", "false"))
        },
        _resetButton: function() {
            if ("input" === this.type) return this.options.label && this.element.val(this.options.label), void 0;
            var a = this.buttonElement.removeClass("ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only"),
                c = b("\x3cspan\x3e\x3c/span\x3e", this.document[0]).addClass("ui-button-text").html(this.options.label).appendTo(a.empty()).text(),
                e = this.options.icons,
                d = e.primary && e.secondary,
                g = [];
            e.primary || e.secondary ? (this.options.text && g.push("ui-button-text-icon" + (d ? "s" : e.primary ? "-primary" : "-secondary")), e.primary && a.prepend("\x3cspan class\x3d'ui-button-icon-primary ui-icon " + e.primary + "'\x3e\x3c/span\x3e"), e.secondary && a.append("\x3cspan class\x3d'ui-button-icon-secondary ui-icon " + e.secondary + "'\x3e\x3c/span\x3e"),
            this.options.text || (g.push(d ? "ui-button-icons-only" : "ui-button-icon-only"), this.hasTitle || a.attr("title", b.trim(c)))) : g.push("ui-button-text-only");
            a.addClass(g.join(" "))
        }
    });
    b.widget("ui.buttonset", {
        version: "1.10.3",
        options: {
            items: "button, input[type\x3dbutton], input[type\x3dsubmit], input[type\x3dreset], input[type\x3dcheckbox], input[type\x3dradio], a, :data(ui-button)"
        },
        _create: function() {
            this.element.addClass("ui-buttonset")
        },
        _init: function() {
            this.refresh()
        },
        _setOption: function(a, b) {
            "disabled" ===
            a && this.buttons.button("option", a, b);
            this._super(a, b)
        },
        refresh: function() {
            var a = "rtl" === this.element.css("direction");
            this.buttons = this.element.find(this.options.items).filter(":ui-button").button("refresh").end().not(":ui-button").button().end().map(function() {
                return b(this).button("widget")[0]
            }).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass(a ? "ui-corner-right" : "ui-corner-left").end().filter(":last").addClass(a ? "ui-corner-left" : "ui-corner-right").end().end()
        },
        _destroy: function() {
            this.element.removeClass("ui-buttonset");
            this.buttons.map(function() {
                return b(this).button("widget")[0]
            }).removeClass("ui-corner-left ui-corner-right").end().button("destroy")
        }
    })
})(jQuery);
(function(b, a) {
    function f() {
        this._curInst = null;
        this._keyEvent = !1;
        this._disabledInputs = [];
        this._inDialog = this._datepickerShowing = !1;
        this._mainDivId = "ui-datepicker-div";
        this._inlineClass = "ui-datepicker-inline";
        this._appendClass = "ui-datepicker-append";
        this._triggerClass = "ui-datepicker-trigger";
        this._dialogClass = "ui-datepicker-dialog";
        this._disableClass = "ui-datepicker-disabled";
        this._unselectableClass = "ui-datepicker-unselectable";
        this._currentClass = "ui-datepicker-current-day";
        this._dayOverClass = "ui-datepicker-days-cell-over";
        this.regional = [];
        this.regional[""] = {
            closeText: "Done",
            prevText: "Prev",
            nextText: "Next",
            currentText: "Today",
            monthNames: "January February March April May June July August September October November December".split(" "),
            monthNamesShort: "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),
            dayNames: "Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" "),
            dayNamesShort: "Sun Mon Tue Wed Thu Fri Sat".split(" "),
            dayNamesMin: "Su Mo Tu We Th Fr Sa".split(" "),
            weekHeader: "Wk",
            dateFormat: "mm/dd/yy",
            firstDay: 0,
            isRTL: !1,
            showMonthAfterYear: !1,
            yearSuffix: ""
        };
        this._defaults = {
            showOn: "focus",
            showAnim: "fadeIn",
            showOptions: {},
            defaultDate: null,
            appendText: "",
            buttonText: "...",
            buttonImage: "",
            buttonImageOnly: !1,
            hideIfNoPrevNext: !1,
            navigationAsDateFormat: !1,
            gotoCurrent: !1,
            changeMonth: !1,
            changeYear: !1,
            yearRange: "c-10:c+10",
            showOtherMonths: !1,
            selectOtherMonths: !1,
            showWeek: !1,
            calculateWeek: this.iso8601Week,
            shortYearCutoff: "+10",
            minDate: null,
            maxDate: null,
            duration: "fast",
            beforeShowDay: null,
            beforeShow: null,
            onSelect: null,
            onChangeMonthYear: null,
            onClose: null,
            numberOfMonths: 1,
            showCurrentAtPos: 0,
            stepMonths: 1,
            stepBigMonths: 12,
            altField: "",
            altFormat: "",
            constrainInput: !0,
            showButtonPanel: !1,
            autoSize: !1,
            disabled: !1
        };
        b.extend(this._defaults, this.regional[""]);
        this.dpDiv = c(b("\x3cdiv id\x3d'" + this._mainDivId + "' class\x3d'ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'\x3e\x3c/div\x3e"))
    }

    function c(a) {
        return a.delegate("button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a",
            "mouseout",
            function() {
                b(this).removeClass("ui-state-hover"); - 1 !== this.className.indexOf("ui-datepicker-prev") && b(this).removeClass("ui-datepicker-prev-hover"); - 1 !== this.className.indexOf("ui-datepicker-next") && b(this).removeClass("ui-datepicker-next-hover")
            }).delegate("button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a", "mouseover", function() {
            b.datepicker._isDisabledDatepicker(e.inline ? a.parent()[0] : e.input[0]) || (b(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"),
                b(this).addClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && b(this).addClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && b(this).addClass("ui-datepicker-next-hover"))
        })
    }

    function d(a, c) {
        b.extend(a, c);
        for (var e in c) null == c[e] && (a[e] = c[e]);
        return a
    }
    b.extend(b.ui, {
        datepicker: {
            version: "1.10.3"
        }
    });
    var e;
    b.extend(f.prototype, {
        markerClassName: "hasDatepicker",
        maxRows: 4,
        _widgetDatepicker: function() {
            return this.dpDiv
        },
        setDefaults: function(a) {
            return d(this._defaults,
                a || {}), this
        },
        _attachDatepicker: function(a, c) {
            var e, d, g;
            e = a.nodeName.toLowerCase();
            d = "div" === e || "span" === e;
            a.id || (this.uuid += 1, a.id = "dp" + this.uuid);
            g = this._newInst(b(a), d);
            g.settings = b.extend({}, c || {});
            "input" === e ? this._connectDatepicker(a, g) : d && this._inlineDatepicker(a, g)
        },
        _newInst: function(a, e) {
            return {
                id: a[0].id.replace(/([^A-Za-z0-9_\-])/g, "\\\\$1"),
                input: a,
                selectedDay: 0,
                selectedMonth: 0,
                selectedYear: 0,
                drawMonth: 0,
                drawYear: 0,
                inline: e,
                dpDiv: e ? c(b("\x3cdiv class\x3d'" + this._inlineClass + " ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'\x3e\x3c/div\x3e")) : this.dpDiv
            }
        },
        _connectDatepicker: function(a, c) {
            var e = b(a);
            c.append = b([]);
            c.trigger = b([]);
            e.hasClass(this.markerClassName) || (this._attachments(e, c), e.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp), this._autoSize(c), b.data(a, "datepicker", c), c.settings.disabled && this._disableDatepicker(a))
        },
        _attachments: function(a, c) {
            var e, d, g;
            e = this._get(c, "appendText");
            var h = this._get(c, "isRTL");
            c.append && c.append.remove();
            e && (c.append = b("\x3cspan class\x3d'" +
                this._appendClass + "'\x3e" + e + "\x3c/span\x3e"), a[h ? "before" : "after"](c.append));
            a.unbind("focus", this._showDatepicker);
            c.trigger && c.trigger.remove();
            e = this._get(c, "showOn");
            "focus" !== e && "both" !== e || a.focus(this._showDatepicker);
            "button" !== e && "both" !== e || (d = this._get(c, "buttonText"), g = this._get(c, "buttonImage"), c.trigger = b(this._get(c, "buttonImageOnly") ? b("\x3cimg/\x3e").addClass(this._triggerClass).attr({
                src: g,
                alt: d,
                title: d
            }) : b("\x3cbutton type\x3d'button'\x3e\x3c/button\x3e").addClass(this._triggerClass).html(g ?
                b("\x3cimg/\x3e").attr({
                    src: g,
                    alt: d,
                    title: d
                }) : d)), a[h ? "before" : "after"](c.trigger), c.trigger.click(function() {
                return b.datepicker._datepickerShowing && b.datepicker._lastInput === a[0] ? b.datepicker._hideDatepicker() : b.datepicker._datepickerShowing && b.datepicker._lastInput !== a[0] ? (b.datepicker._hideDatepicker(), b.datepicker._showDatepicker(a[0])) : b.datepicker._showDatepicker(a[0]), !1
            }))
        },
        _autoSize: function(a) {
            if (this._get(a, "autoSize") && !a.inline) {
                var b, c, e, d, g = new Date(2009, 11, 20),
                    f = this._get(a, "dateFormat");
                f.match(/[DM]/) && (b = function(a) {
                    for (d = e = c = 0; a.length > d; d++) a[d].length > c && (c = a[d].length, e = d);
                    return e
                }, g.setMonth(b(this._get(a, f.match(/MM/) ? "monthNames" : "monthNamesShort"))), g.setDate(b(this._get(a, f.match(/DD/) ? "dayNames" : "dayNamesShort")) + 20 - g.getDay()));
                a.input.attr("size", this._formatDate(a, g).length)
            }
        },
        _inlineDatepicker: function(a, c) {
            var e = b(a);
            e.hasClass(this.markerClassName) || (e.addClass(this.markerClassName).append(c.dpDiv), b.data(a, "datepicker", c), this._setDate(c, this._getDefaultDate(c), !0), this._updateDatepicker(c), this._updateAlternate(c), c.settings.disabled && this._disableDatepicker(a), c.dpDiv.css("display", "block"))
        },
        _dialogDatepicker: function(a, c, e, f, m) {
            var g, h, k, p, l;
            a = this._dialogInst;
            return a || (this.uuid += 1, g = "dp" + this.uuid, this._dialogInput = b("\x3cinput type\x3d'text' id\x3d'" + g + "' style\x3d'position: absolute; top: -100px; width: 0px;'/\x3e"), this._dialogInput.keydown(this._doKeyDown), b("body").append(this._dialogInput), a = this._dialogInst = this._newInst(this._dialogInput, !1), a.settings = {}, b.data(this._dialogInput[0], "datepicker", a)), d(a.settings, f || {}), c = c && c.constructor === Date ? this._formatDate(a, c) : c, this._dialogInput.val(c), this._pos = m ? m.length ? m : [m.pageX, m.pageY] : null, this._pos || (h = document.documentElement.clientWidth, k = document.documentElement.clientHeight, p = document.documentElement.scrollLeft || document.body.scrollLeft, l = document.documentElement.scrollTop || document.body.scrollTop, this._pos = [h / 2 - 100 + p, k / 2 - 150 + l]), this._dialogInput.css("left", this._pos[0] + 20 + "px").css("top",
                this._pos[1] + "px"), a.settings.onSelect = e, this._inDialog = !0, this.dpDiv.addClass(this._dialogClass), this._showDatepicker(this._dialogInput[0]), b.blockUI && b.blockUI(this.dpDiv), b.data(this._dialogInput[0], "datepicker", a), this
        },
        _destroyDatepicker: function(a) {
            var c, e = b(a),
                d = b.data(a, "datepicker");
            e.hasClass(this.markerClassName) && (c = a.nodeName.toLowerCase(), b.removeData(a, "datepicker"), "input" === c ? (d.append.remove(), d.trigger.remove(), e.removeClass(this.markerClassName).unbind("focus", this._showDatepicker).unbind("keydown",
                this._doKeyDown).unbind("keypress", this._doKeyPress).unbind("keyup", this._doKeyUp)) : ("div" === c || "span" === c) && e.removeClass(this.markerClassName).empty())
        },
        _enableDatepicker: function(a) {
            var c, e, d = b(a),
                g = b.data(a, "datepicker");
            d.hasClass(this.markerClassName) && (c = a.nodeName.toLowerCase(), "input" === c ? (a.disabled = !1, g.trigger.filter("button").each(function() {
                this.disabled = !1
            }).end().filter("img").css({
                opacity: "1.0",
                cursor: ""
            })) : ("div" === c || "span" === c) && (e = d.children("." + this._inlineClass), e.children().removeClass("ui-state-disabled"),
                e.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !1)), this._disabledInputs = b.map(this._disabledInputs, function(b) {
                return b === a ? null : b
            }))
        },
        _disableDatepicker: function(a) {
            var c, e, d = b(a),
                g = b.data(a, "datepicker");
            d.hasClass(this.markerClassName) && (c = a.nodeName.toLowerCase(), "input" === c ? (a.disabled = !0, g.trigger.filter("button").each(function() {
                this.disabled = !0
            }).end().filter("img").css({
                opacity: "0.5",
                cursor: "default"
            })) : ("div" === c || "span" === c) && (e = d.children("." + this._inlineClass),
                e.children().addClass("ui-state-disabled"), e.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !0)), this._disabledInputs = b.map(this._disabledInputs, function(b) {
                return b === a ? null : b
            }), this._disabledInputs[this._disabledInputs.length] = a)
        },
        _isDisabledDatepicker: function(a) {
            if (!a) return !1;
            for (var b = 0; this._disabledInputs.length > b; b++)
                if (this._disabledInputs[b] === a) return !0;
            return !1
        },
        _getInst: function(a) {
            try {
                return b.data(a, "datepicker")
            } catch (h) {
                throw "Missing instance data for this datepicker";
            }
        },
        _optionDatepicker: function(c, e, f) {
            var g, h, k, t, r = this._getInst(c);
            return 2 === arguments.length && "string" == typeof e ? "defaults" === e ? b.extend({}, b.datepicker._defaults) : r ? "all" === e ? b.extend({}, r.settings) : this._get(r, e) : null : (g = e || {}, "string" == typeof e && (g = {}, g[e] = f), r && (this._curInst === r && this._hideDatepicker(), h = this._getDateDatepicker(c, !0), k = this._getMinMaxDate(r, "min"), t = this._getMinMaxDate(r, "max"), d(r.settings, g), null !== k && g.dateFormat !== a && g.minDate === a && (r.settings.minDate = this._formatDate(r,
                k)), null !== t && g.dateFormat !== a && g.maxDate === a && (r.settings.maxDate = this._formatDate(r, t)), "disabled" in g && (g.disabled ? this._disableDatepicker(c) : this._enableDatepicker(c)), this._attachments(b(c), r), this._autoSize(r), this._setDate(r, h), this._updateAlternate(r), this._updateDatepicker(r)), a)
        },
        _changeDatepicker: function(a, b, c) {
            this._optionDatepicker(a, b, c)
        },
        _refreshDatepicker: function(a) {
            (a = this._getInst(a)) && this._updateDatepicker(a)
        },
        _setDateDatepicker: function(a, b) {
            (a = this._getInst(a)) && (this._setDate(a,
                b), this._updateDatepicker(a), this._updateAlternate(a))
        },
        _getDateDatepicker: function(a, b) {
            a = this._getInst(a);
            return a && !a.inline && this._setDateFromField(a, b), a ? this._getDate(a) : null
        },
        _doKeyDown: function(a) {
            var c, e, d, g = b.datepicker._getInst(a.target),
                f = !0,
                t = g.dpDiv.is(".ui-datepicker-rtl");
            if (g._keyEvent = !0, b.datepicker._datepickerShowing) switch (a.keyCode) {
                case 9:
                    b.datepicker._hideDatepicker();
                    f = !1;
                    break;
                case 13:
                    return d = b("td." + b.datepicker._dayOverClass + ":not(." + b.datepicker._currentClass + ")", g.dpDiv),
                    d[0] && b.datepicker._selectDay(a.target, g.selectedMonth, g.selectedYear, d[0]), c = b.datepicker._get(g, "onSelect"), c ? (e = b.datepicker._formatDate(g), c.apply(g.input ? g.input[0] : null, [e, g])) : b.datepicker._hideDatepicker(), !1;
                case 27:
                    b.datepicker._hideDatepicker();
                    break;
                case 33:
                    b.datepicker._adjustDate(a.target, a.ctrlKey ? -b.datepicker._get(g, "stepBigMonths") : -b.datepicker._get(g, "stepMonths"), "M");
                    break;
                case 34:
                    b.datepicker._adjustDate(a.target, a.ctrlKey ? +b.datepicker._get(g, "stepBigMonths") : +b.datepicker._get(g,
                        "stepMonths"), "M");
                    break;
                case 35:
                    (a.ctrlKey || a.metaKey) && b.datepicker._clearDate(a.target);
                    f = a.ctrlKey || a.metaKey;
                    break;
                case 36:
                    (a.ctrlKey || a.metaKey) && b.datepicker._gotoToday(a.target);
                    f = a.ctrlKey || a.metaKey;
                    break;
                case 37:
                    (a.ctrlKey || a.metaKey) && b.datepicker._adjustDate(a.target, t ? 1 : -1, "D");
                    f = a.ctrlKey || a.metaKey;
                    a.originalEvent.altKey && b.datepicker._adjustDate(a.target, a.ctrlKey ? -b.datepicker._get(g, "stepBigMonths") : -b.datepicker._get(g, "stepMonths"), "M");
                    break;
                case 38:
                    (a.ctrlKey || a.metaKey) && b.datepicker._adjustDate(a.target, -7, "D");
                    f = a.ctrlKey || a.metaKey;
                    break;
                case 39:
                    (a.ctrlKey || a.metaKey) && b.datepicker._adjustDate(a.target, t ? -1 : 1, "D");
                    f = a.ctrlKey || a.metaKey;
                    a.originalEvent.altKey && b.datepicker._adjustDate(a.target, a.ctrlKey ? +b.datepicker._get(g, "stepBigMonths") : +b.datepicker._get(g, "stepMonths"), "M");
                    break;
                case 40:
                    (a.ctrlKey || a.metaKey) && b.datepicker._adjustDate(a.target, 7, "D");
                    f = a.ctrlKey || a.metaKey;
                    break;
                default:
                    f = !1
            } else 36 === a.keyCode && a.ctrlKey ? b.datepicker._showDatepicker(this) : f = !1;
            f && (a.preventDefault(), a.stopPropagation())
        },
        _doKeyPress: function(c) {
            var e, d, g = b.datepicker._getInst(c.target);
            return b.datepicker._get(g, "constrainInput") ? (e = b.datepicker._possibleChars(b.datepicker._get(g, "dateFormat")), d = String.fromCharCode(null == c.charCode ? c.keyCode : c.charCode), c.ctrlKey || c.metaKey || " " > d || !e || -1 < e.indexOf(d)) : a
        },
        _doKeyUp: function(a) {
            var c;
            a = b.datepicker._getInst(a.target);
            if (a.input.val() !== a.lastVal) try {
                (c = b.datepicker.parseDate(b.datepicker._get(a, "dateFormat"), a.input ? a.input.val() : null, b.datepicker._getFormatConfig(a))) &&
                (b.datepicker._setDateFromField(a), b.datepicker._updateAlternate(a), b.datepicker._updateDatepicker(a))
            } catch (k) {}
            return !0
        },
        _showDatepicker: function(a) {
            if (a = a.target || a, "input" !== a.nodeName.toLowerCase() && (a = b("input", a.parentNode)[0]), !b.datepicker._isDisabledDatepicker(a) && b.datepicker._lastInput !== a) {
                var c, e, g, f, n, t;
                c = b.datepicker._getInst(a);
                b.datepicker._curInst && b.datepicker._curInst !== c && (b.datepicker._curInst.dpDiv.stop(!0, !0), c && b.datepicker._datepickerShowing && b.datepicker._hideDatepicker(b.datepicker._curInst.input[0]));
                e = (e = b.datepicker._get(c, "beforeShow")) ? e.apply(a, [a, c]) : {};
                !1 !== e && (d(c.settings, e), c.lastVal = null, b.datepicker._lastInput = a, b.datepicker._setDateFromField(c), b.datepicker._inDialog && (a.value = ""), b.datepicker._pos || (b.datepicker._pos = b.datepicker._findPos(a), b.datepicker._pos[1] += a.offsetHeight), g = !1, b(a).parents().each(function() {
                    return g |= "fixed" === b(this).css("position"), !g
                }), f = {
                    left: b.datepicker._pos[0],
                    top: b.datepicker._pos[1]
                }, b.datepicker._pos = null, c.dpDiv.empty(), c.dpDiv.css({
                    position: "absolute",
                    display: "block",
                    top: "-1000px"
                }), b.datepicker._updateDatepicker(c), f = b.datepicker._checkOffset(c, f, g), c.dpDiv.css({
                    position: b.datepicker._inDialog && b.blockUI ? "static" : g ? "fixed" : "absolute",
                    display: "none",
                    left: f.left + "px",
                    top: f.top + "px"
                }), c.inline || (n = b.datepicker._get(c, "showAnim"), t = b.datepicker._get(c, "duration"), c.dpDiv.zIndex(b(a).zIndex() + 1), b.datepicker._datepickerShowing = !0, b.effects && b.effects.effect[n] ? c.dpDiv.show(n, b.datepicker._get(c, "showOptions"), t) : c.dpDiv[n || "show"](n ? t : null), b.datepicker._shouldFocusInput(c) &&
                c.input.focus(), b.datepicker._curInst = c))
            }
        },
        _updateDatepicker: function(a) {
            this.maxRows = 4;
            e = a;
            a.dpDiv.empty().append(this._generateHTML(a));
            this._attachHandlers(a);
            a.dpDiv.find("." + this._dayOverClass + " a").mouseover();
            var c, d = this._getNumberOfMonths(a),
                g = d[1];
            a.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width("");
            1 < g && a.dpDiv.addClass("ui-datepicker-multi-" + g).css("width", 17 * g + "em");
            a.dpDiv[(1 !== d[0] || 1 !== d[1] ? "add" : "remove") + "Class"]("ui-datepicker-multi");
            a.dpDiv[(this._get(a, "isRTL") ? "add" : "remove") + "Class"]("ui-datepicker-rtl");
            a === b.datepicker._curInst && b.datepicker._datepickerShowing && b.datepicker._shouldFocusInput(a) && a.input.focus();
            a.yearshtml && (c = a.yearshtml, setTimeout(function() {
                c === a.yearshtml && a.yearshtml && a.dpDiv.find("select.ui-datepicker-year:first").replaceWith(a.yearshtml);
                c = a.yearshtml = null
            }, 0))
        },
        _shouldFocusInput: function(a) {
            return a.input && a.input.is(":visible") && !a.input.is(":disabled") && !a.input.is(":focus")
        },
        _checkOffset: function(a,
                               c, e) {
            var d = a.dpDiv.outerWidth(),
                g = a.dpDiv.outerHeight(),
                h = a.input ? a.input.outerWidth() : 0,
                f = a.input ? a.input.outerHeight() : 0,
                k = document.documentElement.clientWidth + (e ? 0 : b(document).scrollLeft()),
                p = document.documentElement.clientHeight + (e ? 0 : b(document).scrollTop());
            return c.left -= this._get(a, "isRTL") ? d - h : 0, c.left -= e && c.left === a.input.offset().left ? b(document).scrollLeft() : 0, c.top -= e && c.top === a.input.offset().top + f ? b(document).scrollTop() : 0, c.left -= Math.min(c.left, c.left + d > k && k > d ? Math.abs(c.left + d - k) :
                0), c.top -= Math.min(c.top, c.top + g > p && p > g ? Math.abs(g + f) : 0), c
        },
        _findPos: function(a) {
            for (var c, e = this._getInst(a), e = this._get(e, "isRTL"); a && ("hidden" === a.type || 1 !== a.nodeType || b.expr.filters.hidden(a));) a = a[e ? "previousSibling" : "nextSibling"];
            return c = b(a).offset(), [c.left, c.top]
        },
        _hideDatepicker: function(a) {
            var c, e, d, g, f = this._curInst;
            !f || a && f !== b.data(a, "datepicker") || this._datepickerShowing && (c = this._get(f, "showAnim"), e = this._get(f, "duration"), d = function() {
                b.datepicker._tidyDialog(f)
            }, b.effects && (b.effects.effect[c] ||
                b.effects[c]) ? f.dpDiv.hide(c, b.datepicker._get(f, "showOptions"), e, d) : f.dpDiv["slideDown" === c ? "slideUp" : "fadeIn" === c ? "fadeOut" : "hide"](c ? e : null, d), c || d(), this._datepickerShowing = !1, g = this._get(f, "onClose"), g && g.apply(f.input ? f.input[0] : null, [f.input ? f.input.val() : "", f]), this._lastInput = null, this._inDialog && (this._dialogInput.css({
                position: "absolute",
                left: "0",
                top: "-100px"
            }), b.blockUI && (b.unblockUI(), b("body").append(this.dpDiv))), this._inDialog = !1)
        },
        _tidyDialog: function(a) {
            a.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar")
        },
        _checkExternalClick: function(a) {
            if (b.datepicker._curInst) {
                a = b(a.target);
                var c = b.datepicker._getInst(a[0]);
                (!(a[0].id === b.datepicker._mainDivId || 0 !== a.parents("#" + b.datepicker._mainDivId).length || a.hasClass(b.datepicker.markerClassName) || a.closest("." + b.datepicker._triggerClass).length || !b.datepicker._datepickerShowing || b.datepicker._inDialog && b.blockUI) || a.hasClass(b.datepicker.markerClassName) && b.datepicker._curInst !== c) && b.datepicker._hideDatepicker()
            }
        },
        _adjustDate: function(a, c, e) {
            a = b(a);
            var d =
                this._getInst(a[0]);
            this._isDisabledDatepicker(a[0]) || (this._adjustInstDate(d, c + ("M" === e ? this._get(d, "showCurrentAtPos") : 0), e), this._updateDatepicker(d))
        },
        _gotoToday: function(a) {
            var c;
            a = b(a);
            var e = this._getInst(a[0]);
            this._get(e, "gotoCurrent") && e.currentDay ? (e.selectedDay = e.currentDay, e.drawMonth = e.selectedMonth = e.currentMonth, e.drawYear = e.selectedYear = e.currentYear) : (c = new Date, e.selectedDay = c.getDate(), e.drawMonth = e.selectedMonth = c.getMonth(), e.drawYear = e.selectedYear = c.getFullYear());
            this._notifyChange(e);
            this._adjustDate(a)
        },
        _selectMonthYear: function(a, c, e) {
            a = b(a);
            var d = this._getInst(a[0]);
            d["selected" + ("M" === e ? "Month" : "Year")] = d["draw" + ("M" === e ? "Month" : "Year")] = parseInt(c.options[c.selectedIndex].value, 10);
            this._notifyChange(d);
            this._adjustDate(a)
        },
        _selectDay: function(a, c, e, d) {
            var g, f = b(a);
            b(d).hasClass(this._unselectableClass) || this._isDisabledDatepicker(f[0]) || (g = this._getInst(f[0]), g.selectedDay = g.currentDay = b("a", d).html(), g.selectedMonth = g.currentMonth = c, g.selectedYear = g.currentYear = e, this._selectDate(a,
                this._formatDate(g, g.currentDay, g.currentMonth, g.currentYear)))
        },
        _clearDate: function(a) {
            a = b(a);
            this._selectDate(a, "")
        },
        _selectDate: function(a, c) {
            a = b(a);
            var e = this._getInst(a[0]);
            c = null != c ? c : this._formatDate(e);
            e.input && e.input.val(c);
            this._updateAlternate(e);
            (a = this._get(e, "onSelect")) ? a.apply(e.input ? e.input[0] : null, [c, e]): e.input && e.input.trigger("change");
            e.inline ? this._updateDatepicker(e) : (this._hideDatepicker(), this._lastInput = e.input[0], "object" != typeof e.input[0] && e.input.focus(), this._lastInput =
                null)
        },
        _updateAlternate: function(a) {
            var c, e, d, g = this._get(a, "altField");
            g && (c = this._get(a, "altFormat") || this._get(a, "dateFormat"), e = this._getDate(a), d = this.formatDate(c, e, this._getFormatConfig(a)), b(g).each(function() {
                b(this).val(d)
            }))
        },
        noWeekends: function(a) {
            a = a.getDay();
            return [0 < a && 6 > a, ""]
        },
        iso8601Week: function(a) {
            var b;
            a = new Date(a.getTime());
            return a.setDate(a.getDate() + 4 - (a.getDay() || 7)), b = a.getTime(), a.setMonth(0), a.setDate(1), Math.floor(Math.round((b - a) / 864E5) / 7) + 1
        },
        parseDate: function(c, e,
                            d) {
            if (null == c || null == e) throw "Invalid arguments";
            if (e = "object" == typeof e ? "" + e : e + "", "" === e) return null;
            var g, f, h, k, r = 0,
                p = (d ? d.shortYearCutoff : null) || this._defaults.shortYearCutoff,
                p = "string" != typeof p ? p : (new Date).getFullYear() % 100 + parseInt(p, 10),
                y = (d ? d.dayNamesShort : null) || this._defaults.dayNamesShort,
                w = (d ? d.dayNames : null) || this._defaults.dayNames,
                u = (d ? d.monthNamesShort : null) || this._defaults.monthNamesShort;
            d = (d ? d.monthNames : null) || this._defaults.monthNames;
            var x = -1,
                B = -1,
                A = -1,
                C = -1,
                I = !1,
                D = function(a) {
                    a =
                        c.length > g + 1 && c.charAt(g + 1) === a;
                    return a && g++, a
                },
                E = function(a) {
                    var b = D(a);
                    a = RegExp("^\\d{1," + ("@" === a ? 14 : "!" === a ? 20 : "y" === a && b ? 4 : "o" === a ? 3 : 2) + "}");
                    a = e.substring(r).match(a);
                    if (!a) throw "Missing number at position " + r;
                    return r += a[0].length, parseInt(a[0], 10)
                },
                J = function(c, d, g) {
                    var f = -1;
                    c = b.map(D(c) ? g : d, function(a, b) {
                        return [
                            [b, a]
                        ]
                    }).sort(function(a, b) {
                        return -(a[1].length - b[1].length)
                    });
                    if (b.each(c, function(b, c) {
                        b = c[1];
                        return e.substr(r, b.length).toLowerCase() === b.toLowerCase() ? (f = c[0], r += b.length, !1) :
                            a
                    }), -1 !== f) return f + 1;
                    throw "Unknown name at position " + r;
                },
                O = function() {
                    if (e.charAt(r) !== c.charAt(g)) throw "Unexpected literal at position " + r;
                    r++
                };
            for (g = 0; c.length > g; g++)
                if (I) "'" !== c.charAt(g) || D("'") ? O() : I = !1;
                else switch (c.charAt(g)) {
                    case "d":
                        A = E("d");
                        break;
                    case "D":
                        J("D", y, w);
                        break;
                    case "o":
                        C = E("o");
                        break;
                    case "m":
                        B = E("m");
                        break;
                    case "M":
                        B = J("M", u, d);
                        break;
                    case "y":
                        x = E("y");
                        break;
                    case "@":
                        k = new Date(E("@"));
                        x = k.getFullYear();
                        B = k.getMonth() + 1;
                        A = k.getDate();
                        break;
                    case "!":
                        k = new Date((E("!") - this._ticksTo1970) /
                            1E4);
                        x = k.getFullYear();
                        B = k.getMonth() + 1;
                        A = k.getDate();
                        break;
                    case "'":
                        D("'") ? O() : I = !0;
                        break;
                    default:
                        O()
                }
            if (e.length > r && (h = e.substr(r), !/^\s+/.test(h))) throw "Extra/unparsed characters found in date: " + h;
            if (-1 === x ? x = (new Date).getFullYear() : 100 > x && (x += (new Date).getFullYear() - (new Date).getFullYear() % 100 + (p >= x ? 0 : -100)), -1 < C)
                for (B = 1, A = C; !(f = this._getDaysInMonth(x, B - 1), f >= A);) B++, A -= f;
            if (k = this._daylightSavingAdjust(new Date(x, B - 1, A)), k.getFullYear() !== x || k.getMonth() + 1 !== B || k.getDate() !== A) throw "Invalid date";
            return k
        },
        ATOM: "yy-mm-dd",
        COOKIE: "D, dd M yy",
        ISO_8601: "yy-mm-dd",
        RFC_822: "D, d M y",
        RFC_850: "DD, dd-M-y",
        RFC_1036: "D, d M y",
        RFC_1123: "D, d M yy",
        RFC_2822: "D, d M yy",
        RSS: "D, d M y",
        TICKS: "!",
        TIMESTAMP: "@",
        W3C: "yy-mm-dd",
        _ticksTo1970: 864E9 * (718685 + Math.floor(492.5) - Math.floor(19.7) + Math.floor(4.925)),
        formatDate: function(a, b, c) {
            if (!b) return "";
            var e, d = (c ? c.dayNamesShort : null) || this._defaults.dayNamesShort,
                g = (c ? c.dayNames : null) || this._defaults.dayNames,
                f = (c ? c.monthNamesShort : null) || this._defaults.monthNamesShort;
            c = (c ? c.monthNames : null) || this._defaults.monthNames;
            var h = function(b) {
                    b = a.length > e + 1 && a.charAt(e + 1) === b;
                    return b && e++, b
                },
                k = function(a, b, c) {
                    b = "" + b;
                    if (h(a))
                        for (; c > b.length;) b = "0" + b;
                    return b
                },
                y = function(a, b, c, e) {
                    return h(a) ? e[b] : c[b]
                },
                w = "",
                u = !1;
            if (b)
                for (e = 0; a.length > e; e++)
                    if (u) "'" !== a.charAt(e) || h("'") ? w += a.charAt(e) : u = !1;
                    else switch (a.charAt(e)) {
                        case "d":
                            w += k("d", b.getDate(), 2);
                            break;
                        case "D":
                            w += y("D", b.getDay(), d, g);
                            break;
                        case "o":
                            w += k("o", Math.round(((new Date(b.getFullYear(), b.getMonth(), b.getDate())).getTime() -
                                (new Date(b.getFullYear(), 0, 0)).getTime()) / 864E5), 3);
                            break;
                        case "m":
                            w += k("m", b.getMonth() + 1, 2);
                            break;
                        case "M":
                            w += y("M", b.getMonth(), f, c);
                            break;
                        case "y":
                            w += h("y") ? b.getFullYear() : (10 > b.getYear() % 100 ? "0" : "") + b.getYear() % 100;
                            break;
                        case "@":
                            w += b.getTime();
                            break;
                        case "!":
                            w += 1E4 * b.getTime() + this._ticksTo1970;
                            break;
                        case "'":
                            h("'") ? w += "'" : u = !0;
                            break;
                        default:
                            w += a.charAt(e)
                    }
            return w
        },
        _possibleChars: function(a) {
            var b, c = "",
                e = !1,
                d = function(c) {
                    c = a.length > b + 1 && a.charAt(b + 1) === c;
                    return c && b++, c
                };
            for (b = 0; a.length >
            b; b++)
                if (e) "'" !== a.charAt(b) || d("'") ? c += a.charAt(b) : e = !1;
                else switch (a.charAt(b)) {
                    case "d":
                    case "m":
                    case "y":
                    case "@":
                        c += "0123456789";
                        break;
                    case "D":
                    case "M":
                        return null;
                    case "'":
                        d("'") ? c += "'" : e = !0;
                        break;
                    default:
                        c += a.charAt(b)
                }
            return c
        },
        _get: function(b, c) {
            return b.settings[c] !== a ? b.settings[c] : this._defaults[c]
        },
        _setDateFromField: function(a, b) {
            if (a.input.val() !== a.lastVal) {
                var c = this._get(a, "dateFormat"),
                    e = a.lastVal = a.input ? a.input.val() : null,
                    d = this._getDefaultDate(a),
                    g = d,
                    f = this._getFormatConfig(a);
                try {
                    g = this.parseDate(c, e, f) || d
                } catch (r) {
                    e = b ? "" : e
                }
                a.selectedDay = g.getDate();
                a.drawMonth = a.selectedMonth = g.getMonth();
                a.drawYear = a.selectedYear = g.getFullYear();
                a.currentDay = e ? g.getDate() : 0;
                a.currentMonth = e ? g.getMonth() : 0;
                a.currentYear = e ? g.getFullYear() : 0;
                this._adjustInstDate(a)
            }
        },
        _getDefaultDate: function(a) {
            return this._restrictMinMax(a, this._determineDate(a, this._get(a, "defaultDate"), new Date))
        },
        _determineDate: function(a, c, e) {
            var d = function(a) {
                    var b = new Date;
                    return b.setDate(b.getDate() + a), b
                },
                g = function(c) {
                    try {
                        return b.datepicker.parseDate(b.datepicker._get(a,
                            "dateFormat"), c, b.datepicker._getFormatConfig(a))
                    } catch (u) {}
                    for (var e = (c.toLowerCase().match(/^c/) ? b.datepicker._getDate(a) : null) || new Date, d = e.getFullYear(), g = e.getMonth(), e = e.getDate(), f = /([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g, h = f.exec(c); h;) {
                        switch (h[2] || "d") {
                            case "d":
                            case "D":
                                e += parseInt(h[1], 10);
                                break;
                            case "w":
                            case "W":
                                e += 7 * parseInt(h[1], 10);
                                break;
                            case "m":
                            case "M":
                                g += parseInt(h[1], 10);
                                e = Math.min(e, b.datepicker._getDaysInMonth(d, g));
                                break;
                            case "y":
                            case "Y":
                                d += parseInt(h[1], 10), e = Math.min(e, b.datepicker._getDaysInMonth(d,
                                    g))
                        }
                        h = f.exec(c)
                    }
                    return new Date(d, g, e)
                };
            c = null == c || "" === c ? e : "string" == typeof c ? g(c) : "number" == typeof c ? isNaN(c) ? e : d(c) : new Date(c.getTime());
            return c = c && "Invalid Date" == "" + c ? e : c, c && (c.setHours(0), c.setMinutes(0), c.setSeconds(0), c.setMilliseconds(0)), this._daylightSavingAdjust(c)
        },
        _daylightSavingAdjust: function(a) {
            return a ? (a.setHours(12 < a.getHours() ? a.getHours() + 2 : 0), a) : null
        },
        _setDate: function(a, b, c) {
            var e = !b,
                d = a.selectedMonth,
                g = a.selectedYear;
            b = this._restrictMinMax(a, this._determineDate(a, b, new Date));
            a.selectedDay = a.currentDay = b.getDate();
            a.drawMonth = a.selectedMonth = a.currentMonth = b.getMonth();
            a.drawYear = a.selectedYear = a.currentYear = b.getFullYear();
            d === a.selectedMonth && g === a.selectedYear || c || this._notifyChange(a);
            this._adjustInstDate(a);
            a.input && a.input.val(e ? "" : this._formatDate(a))
        },
        _getDate: function(a) {
            return !a.currentYear || a.input && "" === a.input.val() ? null : this._daylightSavingAdjust(new Date(a.currentYear, a.currentMonth, a.currentDay))
        },
        _attachHandlers: function(a) {
            var c = this._get(a, "stepMonths"),
                e = "#" + a.id.replace(/\\\\/g, "\\");
            a.dpDiv.find("[data-handler]").map(function() {
                b(this).bind(this.getAttribute("data-event"), {
                    prev: function() {
                        b.datepicker._adjustDate(e, -c, "M")
                    },
                    next: function() {
                        b.datepicker._adjustDate(e, +c, "M")
                    },
                    hide: function() {
                        b.datepicker._hideDatepicker()
                    },
                    today: function() {
                        b.datepicker._gotoToday(e)
                    },
                    selectDay: function() {
                        return b.datepicker._selectDay(e, +this.getAttribute("data-month"), +this.getAttribute("data-year"), this), !1
                    },
                    selectMonth: function() {
                        return b.datepicker._selectMonthYear(e,
                            this, "M"), !1
                    },
                    selectYear: function() {
                        return b.datepicker._selectMonthYear(e, this, "Y"), !1
                    }
                }[this.getAttribute("data-handler")])
            })
        },
        _generateHTML: function(a) {
            var b, c, e, d, g, f, r, p, y, w, u, x, B, A, C, I, D, E, J, O, z, q, G, v, L, M, P, Q = new Date,
                Q = this._daylightSavingAdjust(new Date(Q.getFullYear(), Q.getMonth(), Q.getDate())),
                U = this._get(a, "isRTL");
            f = this._get(a, "showButtonPanel");
            e = this._get(a, "hideIfNoPrevNext");
            g = this._get(a, "navigationAsDateFormat");
            var V = this._getNumberOfMonths(a),
                X = this._get(a, "showCurrentAtPos");
            d = this._get(a, "stepMonths");
            var da = 1 !== V[0] || 1 !== V[1],
                ea = this._daylightSavingAdjust(a.currentDay ? new Date(a.currentYear, a.currentMonth, a.currentDay) : new Date(9999, 9, 9)),
                T = this._getMinMaxDate(a, "min"),
                Y = this._getMinMaxDate(a, "max"),
                X = a.drawMonth - X,
                H = a.drawYear;
            if (0 > X && (X += 12, H--), Y)
                for (b = this._daylightSavingAdjust(new Date(Y.getFullYear(), Y.getMonth() - V[0] * V[1] + 1, Y.getDate())), b = T && T > b ? T : b; this._daylightSavingAdjust(new Date(H, X, 1)) > b;) X--, 0 > X && (X = 11, H--);
            a.drawMonth = X;
            a.drawYear = H;
            b = this._get(a, "prevText");
            b = g ? this.formatDate(b, this._daylightSavingAdjust(new Date(H, X - d, 1)), this._getFormatConfig(a)) : b;
            b = this._canAdjustMonth(a, -1, H, X) ? "\x3ca class\x3d'ui-datepicker-prev ui-corner-all' data-handler\x3d'prev' data-event\x3d'click' title\x3d'" + b + "'\x3e\x3cspan class\x3d'ui-icon ui-icon-circle-triangle-" + (U ? "e" : "w") + "'\x3e" + b + "\x3c/span\x3e\x3c/a\x3e" : e ? "" : "\x3ca class\x3d'ui-datepicker-prev ui-corner-all ui-state-disabled' title\x3d'" + b + "'\x3e\x3cspan class\x3d'ui-icon ui-icon-circle-triangle-" + (U ? "e" : "w") +
                "'\x3e" + b + "\x3c/span\x3e\x3c/a\x3e";
            c = this._get(a, "nextText");
            c = g ? this.formatDate(c, this._daylightSavingAdjust(new Date(H, X + d, 1)), this._getFormatConfig(a)) : c;
            e = this._canAdjustMonth(a, 1, H, X) ? "\x3ca class\x3d'ui-datepicker-next ui-corner-all' data-handler\x3d'next' data-event\x3d'click' title\x3d'" + c + "'\x3e\x3cspan class\x3d'ui-icon ui-icon-circle-triangle-" + (U ? "w" : "e") + "'\x3e" + c + "\x3c/span\x3e\x3c/a\x3e" : e ? "" : "\x3ca class\x3d'ui-datepicker-next ui-corner-all ui-state-disabled' title\x3d'" + c + "'\x3e\x3cspan class\x3d'ui-icon ui-icon-circle-triangle-" +
                (U ? "w" : "e") + "'\x3e" + c + "\x3c/span\x3e\x3c/a\x3e";
            d = this._get(a, "currentText");
            c = this._get(a, "gotoCurrent") && a.currentDay ? ea : Q;
            d = g ? this.formatDate(d, c, this._getFormatConfig(a)) : d;
            g = a.inline ? "" : "\x3cbutton type\x3d'button' class\x3d'ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler\x3d'hide' data-event\x3d'click'\x3e" + this._get(a, "closeText") + "\x3c/button\x3e";
            f = f ? "\x3cdiv class\x3d'ui-datepicker-buttonpane ui-widget-content'\x3e" + (U ? g : "") + (this._isInRange(a, c) ? "\x3cbutton type\x3d'button' class\x3d'ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler\x3d'today' data-event\x3d'click'\x3e" +
                d + "\x3c/button\x3e" : "") + (U ? "" : g) + "\x3c/div\x3e" : "";
            g = parseInt(this._get(a, "firstDay"), 10);
            g = isNaN(g) ? 0 : g;
            d = this._get(a, "showWeek");
            c = this._get(a, "dayNames");
            r = this._get(a, "dayNamesMin");
            p = this._get(a, "monthNames");
            y = this._get(a, "monthNamesShort");
            w = this._get(a, "beforeShowDay");
            u = this._get(a, "showOtherMonths");
            x = this._get(a, "selectOtherMonths");
            B = this._getDefaultDate(a);
            A = "";
            for (I = 0; V[0] > I; I++) {
                D = "";
                this.maxRows = 4;
                for (E = 0; V[1] > E; E++) {
                    if (J = this._daylightSavingAdjust(new Date(H, X, a.selectedDay)), C = " ui-corner-all",
                        O = "", da) {
                        if (O += "\x3cdiv class\x3d'ui-datepicker-group", 1 < V[1]) switch (E) {
                            case 0:
                                O += " ui-datepicker-group-first";
                                C = " ui-corner-" + (U ? "right" : "left");
                                break;
                            case V[1] - 1:
                                O += " ui-datepicker-group-last";
                                C = " ui-corner-" + (U ? "left" : "right");
                                break;
                            default:
                                O += " ui-datepicker-group-middle", C = ""
                        }
                        O += "'\x3e"
                    }
                    O += "\x3cdiv class\x3d'ui-datepicker-header ui-widget-header ui-helper-clearfix" + C + "'\x3e" + (/all|left/.test(C) && 0 === I ? U ? e : b : "") + (/all|right/.test(C) && 0 === I ? U ? b : e : "") + this._generateMonthYearHeader(a, X, H, T, Y, 0 < I ||
                        0 < E, p, y) + "\x3c/div\x3e\x3ctable class\x3d'ui-datepicker-calendar'\x3e\x3cthead\x3e\x3ctr\x3e";
                    z = d ? "\x3cth class\x3d'ui-datepicker-week-col'\x3e" + this._get(a, "weekHeader") + "\x3c/th\x3e" : "";
                    for (C = 0; 7 > C; C++) q = (C + g) % 7, z += "\x3cth" + (5 <= (C + g + 6) % 7 ? " class\x3d'ui-datepicker-week-end'" : "") + "\x3e\x3cspan title\x3d'" + c[q] + "'\x3e" + r[q] + "\x3c/span\x3e\x3c/th\x3e";
                    O += z + "\x3c/tr\x3e\x3c/thead\x3e\x3ctbody\x3e";
                    z = this._getDaysInMonth(H, X);
                    H === a.selectedYear && X === a.selectedMonth && (a.selectedDay = Math.min(a.selectedDay,
                        z));
                    C = (this._getFirstDayOfMonth(H, X) - g + 7) % 7;
                    z = Math.ceil((C + z) / 7);
                    this.maxRows = z = da ? this.maxRows > z ? this.maxRows : z : z;
                    q = this._daylightSavingAdjust(new Date(H, X, 1 - C));
                    for (G = 0; z > G; G++) {
                        O += "\x3ctr\x3e";
                        v = d ? "\x3ctd class\x3d'ui-datepicker-week-col'\x3e" + this._get(a, "calculateWeek")(q) + "\x3c/td\x3e" : "";
                        for (C = 0; 7 > C; C++) L = w ? w.apply(a.input ? a.input[0] : null, [q]) : [!0, ""], P = (M = q.getMonth() !== X) && !x || !L[0] || T && T > q || Y && q > Y, v += "\x3ctd class\x3d'" + (5 <= (C + g + 6) % 7 ? " ui-datepicker-week-end" : "") + (M ? " ui-datepicker-other-month" :
                            "") + (q.getTime() === J.getTime() && X === a.selectedMonth && a._keyEvent || B.getTime() === q.getTime() && B.getTime() === J.getTime() ? " " + this._dayOverClass : "") + (P ? " " + this._unselectableClass + " ui-state-disabled" : "") + (M && !u ? "" : " " + L[1] + (q.getTime() === ea.getTime() ? " " + this._currentClass : "") + (q.getTime() === Q.getTime() ? " ui-datepicker-today" : "")) + "'" + (M && !u || !L[2] ? "" : " title\x3d'" + L[2].replace(/'/g, "\x26#39;") + "'") + (P ? "" : " data-handler\x3d'selectDay' data-event\x3d'click' data-month\x3d'" + q.getMonth() + "' data-year\x3d'" +
                            q.getFullYear() + "'") + "\x3e" + (M && !u ? "\x26#xa0;" : P ? "\x3cspan class\x3d'ui-state-default'\x3e" + q.getDate() + "\x3c/span\x3e" : "\x3ca class\x3d'ui-state-default" + (q.getTime() === Q.getTime() ? " ui-state-highlight" : "") + (q.getTime() === ea.getTime() ? " ui-state-active" : "") + (M ? " ui-priority-secondary" : "") + "' href\x3d'#'\x3e" + q.getDate() + "\x3c/a\x3e") + "\x3c/td\x3e", q.setDate(q.getDate() + 1), q = this._daylightSavingAdjust(q);
                        O += v + "\x3c/tr\x3e"
                    }
                    X++;
                    11 < X && (X = 0, H++);
                    O += "\x3c/tbody\x3e\x3c/table\x3e" + (da ? "\x3c/div\x3e" + (0 <
                    V[0] && E === V[1] - 1 ? "\x3cdiv class\x3d'ui-datepicker-row-break'\x3e\x3c/div\x3e" : "") : "");
                    D += O
                }
                A += D
            }
            return A += f, a._keyEvent = !1, A
        },
        _generateMonthYearHeader: function(a, b, c, e, d, f, t, r) {
            var g, h, k, l = this._get(a, "changeMonth"),
                n = this._get(a, "changeYear"),
                m = this._get(a, "showMonthAfterYear"),
                A = "\x3cdiv class\x3d'ui-datepicker-title'\x3e",
                C = "";
            if (f || !l) C += "\x3cspan class\x3d'ui-datepicker-month'\x3e" + t[b] + "\x3c/span\x3e";
            else {
                t = e && e.getFullYear() === c;
                g = d && d.getFullYear() === c;
                C += "\x3cselect class\x3d'ui-datepicker-month' data-handler\x3d'selectMonth' data-event\x3d'change'\x3e";
                for (h = 0; 12 > h; h++)(!t || h >= e.getMonth()) && (!g || d.getMonth() >= h) && (C += "\x3coption value\x3d'" + h + "'" + (h === b ? " selected\x3d'selected'" : "") + "\x3e" + r[h] + "\x3c/option\x3e");
                C += "\x3c/select\x3e"
            }
            if (m || (A += C + (!f && l && n ? "" : "\x26#xa0;")), !a.yearshtml)
                if (a.yearshtml = "", f || !n) A += "\x3cspan class\x3d'ui-datepicker-year'\x3e" + c + "\x3c/span\x3e";
                else {
                    r = this._get(a, "yearRange").split(":");
                    k = (new Date).getFullYear();
                    t = function(a) {
                        a = a.match(/c[+\-].*/) ? c + parseInt(a.substring(1), 10) : a.match(/[+\-].*/) ? k + parseInt(a, 10) : parseInt(a,
                            10);
                        return isNaN(a) ? k : a
                    };
                    b = t(r[0]);
                    r = Math.max(b, t(r[1] || ""));
                    b = e ? Math.max(b, e.getFullYear()) : b;
                    r = d ? Math.min(r, d.getFullYear()) : r;
                    for (a.yearshtml += "\x3cselect class\x3d'ui-datepicker-year' data-handler\x3d'selectYear' data-event\x3d'change'\x3e"; r >= b; b++) a.yearshtml += "\x3coption value\x3d'" + b + "'" + (b === c ? " selected\x3d'selected'" : "") + "\x3e" + b + "\x3c/option\x3e";
                    a.yearshtml += "\x3c/select\x3e";
                    A += a.yearshtml;
                    a.yearshtml = null
                }
            return A += this._get(a, "yearSuffix"), m && (A += (!f && l && n ? "" : "\x26#xa0;") + C), A += "\x3c/div\x3e"
        },
        _adjustInstDate: function(a, b, c) {
            var e = a.drawYear + ("Y" === c ? b : 0),
                d = a.drawMonth + ("M" === c ? b : 0);
            b = Math.min(a.selectedDay, this._getDaysInMonth(e, d)) + ("D" === c ? b : 0);
            e = this._restrictMinMax(a, this._daylightSavingAdjust(new Date(e, d, b)));
            a.selectedDay = e.getDate();
            a.drawMonth = a.selectedMonth = e.getMonth();
            a.drawYear = a.selectedYear = e.getFullYear();
            "M" !== c && "Y" !== c || this._notifyChange(a)
        },
        _restrictMinMax: function(a, b) {
            var c = this._getMinMaxDate(a, "min");
            a = this._getMinMaxDate(a, "max");
            b = c && c > b ? c : b;
            return a && b > a ? a :
                b
        },
        _notifyChange: function(a) {
            var b = this._get(a, "onChangeMonthYear");
            b && b.apply(a.input ? a.input[0] : null, [a.selectedYear, a.selectedMonth + 1, a])
        },
        _getNumberOfMonths: function(a) {
            a = this._get(a, "numberOfMonths");
            return null == a ? [1, 1] : "number" == typeof a ? [1, a] : a
        },
        _getMinMaxDate: function(a, b) {
            return this._determineDate(a, this._get(a, b + "Date"), null)
        },
        _getDaysInMonth: function(a, b) {
            return 32 - this._daylightSavingAdjust(new Date(a, b, 32)).getDate()
        },
        _getFirstDayOfMonth: function(a, b) {
            return (new Date(a, b, 1)).getDay()
        },
        _canAdjustMonth: function(a, b, c, e) {
            var d = this._getNumberOfMonths(a);
            c = this._daylightSavingAdjust(new Date(c, e + (0 > b ? b : d[0] * d[1]), 1));
            return 0 > b && c.setDate(this._getDaysInMonth(c.getFullYear(), c.getMonth())), this._isInRange(a, c)
        },
        _isInRange: function(a, b) {
            var c, e, d = this._getMinMaxDate(a, "min"),
                g = this._getMinMaxDate(a, "max"),
                f = null,
                h = null;
            a = this._get(a, "yearRange");
            return a && (c = a.split(":"), e = (new Date).getFullYear(), f = parseInt(c[0], 10), h = parseInt(c[1], 10), c[0].match(/[+\-].*/) && (f += e), c[1].match(/[+\-].*/) &&
            (h += e)), (!d || b.getTime() >= d.getTime()) && (!g || b.getTime() <= g.getTime()) && (!f || b.getFullYear() >= f) && (!h || h >= b.getFullYear())
        },
        _getFormatConfig: function(a) {
            var b = this._get(a, "shortYearCutoff");
            return b = "string" != typeof b ? b : (new Date).getFullYear() % 100 + parseInt(b, 10), {
                shortYearCutoff: b,
                dayNamesShort: this._get(a, "dayNamesShort"),
                dayNames: this._get(a, "dayNames"),
                monthNamesShort: this._get(a, "monthNamesShort"),
                monthNames: this._get(a, "monthNames")
            }
        },
        _formatDate: function(a, b, c, e) {
            b || (a.currentDay = a.selectedDay,
                a.currentMonth = a.selectedMonth, a.currentYear = a.selectedYear);
            b = b ? "object" == typeof b ? b : this._daylightSavingAdjust(new Date(e, c, b)) : this._daylightSavingAdjust(new Date(a.currentYear, a.currentMonth, a.currentDay));
            return this.formatDate(this._get(a, "dateFormat"), b, this._getFormatConfig(a))
        }
    });
    b.fn.datepicker = function(a) {
        if (!this.length) return this;
        b.datepicker.initialized || (b(document).mousedown(b.datepicker._checkExternalClick), b.datepicker.initialized = !0);
        0 === b("#" + b.datepicker._mainDivId).length &&
        b("body").append(b.datepicker.dpDiv);
        var c = Array.prototype.slice.call(arguments, 1);
        return "string" != typeof a || "isDisabled" !== a && "getDate" !== a && "widget" !== a ? "option" === a && 2 === arguments.length && "string" == typeof arguments[1] ? b.datepicker["_" + a + "Datepicker"].apply(b.datepicker, [this[0]].concat(c)) : this.each(function() {
            "string" == typeof a ? b.datepicker["_" + a + "Datepicker"].apply(b.datepicker, [this].concat(c)) : b.datepicker._attachDatepicker(this, a)
        }) : b.datepicker["_" + a + "Datepicker"].apply(b.datepicker, [this[0]].concat(c))
    };
    b.datepicker = new f;
    b.datepicker.initialized = !1;
    b.datepicker.uuid = (new Date).getTime();
    b.datepicker.version = "1.10.3"
})(jQuery);
(function(b) {
    var a = {
            buttons: !0,
            height: !0,
            maxHeight: !0,
            maxWidth: !0,
            minHeight: !0,
            minWidth: !0,
            width: !0
        },
        f = {
            maxHeight: !0,
            maxWidth: !0,
            minHeight: !0,
            minWidth: !0
        };
    b.widget("ui.dialog", {
        version: "1.10.3",
        options: {
            appendTo: "body",
            autoOpen: !0,
            buttons: [],
            closeOnEscape: !0,
            closeText: "close",
            dialogClass: "",
            draggable: !0,
            hide: null,
            height: "auto",
            maxHeight: null,
            maxWidth: null,
            minHeight: 150,
            minWidth: 150,
            modal: !1,
            position: {
                my: "center",
                at: "center",
                of: window,
                collision: "fit",
                using: function(a) {
                    var c = b(this).css(a).offset().top;
                    0 > c && b(this).css("top", a.top - c)
                }
            },
            resizable: !0,
            show: null,
            title: null,
            width: 300,
            beforeClose: null,
            close: null,
            drag: null,
            dragStart: null,
            dragStop: null,
            focus: null,
            open: null,
            resize: null,
            resizeStart: null,
            resizeStop: null
        },
        _create: function() {
            this.originalCss = {
                display: this.element[0].style.display,
                width: this.element[0].style.width,
                minHeight: this.element[0].style.minHeight,
                maxHeight: this.element[0].style.maxHeight,
                height: this.element[0].style.height
            };
            this.originalPosition = {
                parent: this.element.parent(),
                index: this.element.parent().children().index(this.element)
            };
            this.originalTitle = this.element.attr("title");
            this.options.title = this.options.title || this.originalTitle;
            this._createWrapper();
            this.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(this.uiDialog);
            this._createTitlebar();
            this._createButtonPane();
            this.options.draggable && b.fn.draggable && this._makeDraggable();
            this.options.resizable && b.fn.resizable && this._makeResizable();
            this._isOpen = !1
        },
        _init: function() {
            this.options.autoOpen && this.open()
        },
        _appendTo: function() {
            var a =
                this.options.appendTo;
            return a && (a.jquery || a.nodeType) ? b(a) : this.document.find(a || "body").eq(0)
        },
        _destroy: function() {
            var a, b = this.originalPosition;
            this._destroyOverlay();
            this.element.removeUniqueId().removeClass("ui-dialog-content ui-widget-content").css(this.originalCss).detach();
            this.uiDialog.stop(!0, !0).remove();
            this.originalTitle && this.element.attr("title", this.originalTitle);
            a = b.parent.children().eq(b.index);
            a.length && a[0] !== this.element[0] ? a.before(this.element) : b.parent.append(this.element)
        },
        widget: function() {
            return this.uiDialog
        },
        disable: b.noop,
        enable: b.noop,
        close: function(a) {
            var c = this;
            this._isOpen && !1 !== this._trigger("beforeClose", a) && (this._isOpen = !1, this._destroyOverlay(), this.opener.filter(":focusable").focus().length || b(this.document[0].activeElement).blur(), this._hide(this.uiDialog, this.options.hide, function() {
                c._trigger("close", a)
            }))
        },
        isOpen: function() {
            return this._isOpen
        },
        moveToTop: function() {
            this._moveToTop()
        },
        _moveToTop: function(a, b) {
            var c = !!this.uiDialog.nextAll(":visible").insertBefore(this.uiDialog).length;
            return c && !b && this._trigger("focus", a), c
        },
        open: function() {
            var a = this;
            return this._isOpen ? (this._moveToTop() && this._focusTabbable(), void 0) : (this._isOpen = !0, this.opener = b(this.document[0].activeElement), this._size(), this._position(), this._createOverlay(), this._moveToTop(null, !0), this._show(this.uiDialog, this.options.show, function() {
                a._focusTabbable();
                a._trigger("focus")
            }), this._trigger("open"), void 0)
        },
        _focusTabbable: function() {
            var a = this.element.find("[autofocus]");
            a.length || (a = this.element.find(":tabbable"));
            a.length || (a = this.uiDialogButtonPane.find(":tabbable"));
            a.length || (a = this.uiDialogTitlebarClose.filter(":tabbable"));
            a.length || (a = this.uiDialog);
            a.eq(0).focus()
        },
        _keepFocus: function(a) {
            function c() {
                var a = this.document[0].activeElement;
                this.uiDialog[0] === a || b.contains(this.uiDialog[0], a) || this._focusTabbable()
            }
            a.preventDefault();
            c.call(this);
            this._delay(c)
        },
        _createWrapper: function() {
            this.uiDialog = b("\x3cdiv\x3e").addClass("ui-dialog ui-widget ui-widget-content ui-corner-all ui-front " + this.options.dialogClass).hide().attr({
                tabIndex: -1,
                role: "dialog"
            }).appendTo(this._appendTo());
            this._on(this.uiDialog, {
                keydown: function(a) {
                    if (this.options.closeOnEscape && !a.isDefaultPrevented() && a.keyCode && a.keyCode === b.ui.keyCode.ESCAPE) return a.preventDefault(), this.close(a), void 0;
                    if (a.keyCode === b.ui.keyCode.TAB) {
                        var c = this.uiDialog.find(":tabbable"),
                            e = c.filter(":first"),
                            c = c.filter(":last");
                        a.target !== c[0] && a.target !== this.uiDialog[0] || a.shiftKey ? a.target !== e[0] && a.target !== this.uiDialog[0] || !a.shiftKey || (c.focus(1), a.preventDefault()) : (e.focus(1),
                            a.preventDefault())
                    }
                },
                mousedown: function(a) {
                    this._moveToTop(a) && this._focusTabbable()
                }
            });
            this.element.find("[aria-describedby]").length || this.uiDialog.attr({
                "aria-describedby": this.element.uniqueId().attr("id")
            })
        },
        _createTitlebar: function() {
            var a;
            this.uiDialogTitlebar = b("\x3cdiv\x3e").addClass("ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix").prependTo(this.uiDialog);
            this._on(this.uiDialogTitlebar, {
                mousedown: function(a) {
                    b(a.target).closest(".ui-dialog-titlebar-close") || this.uiDialog.focus()
                }
            });
            this.uiDialogTitlebarClose = b("\x3cbutton\x3e\x3c/button\x3e").button({
                label: this.options.closeText,
                icons: {
                    primary: "ui-icon-closethick"
                },
                text: !1
            }).addClass("ui-dialog-titlebar-close").appendTo(this.uiDialogTitlebar);
            this._on(this.uiDialogTitlebarClose, {
                click: function(a) {
                    a.preventDefault();
                    this.close(a)
                }
            });
            a = b("\x3cspan\x3e").uniqueId().addClass("ui-dialog-title").prependTo(this.uiDialogTitlebar);
            this._title(a);
            this.uiDialog.attr({
                "aria-labelledby": a.attr("id")
            })
        },
        _title: function(a) {
            this.options.title ||
            a.html("\x26#160;");
            a.text(this.options.title)
        },
        _createButtonPane: function() {
            this.uiDialogButtonPane = b("\x3cdiv\x3e").addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix");
            this.uiButtonSet = b("\x3cdiv\x3e").addClass("ui-dialog-buttonset").appendTo(this.uiDialogButtonPane);
            this._createButtons()
        },
        _createButtons: function() {
            var a = this,
                d = this.options.buttons;
            return this.uiDialogButtonPane.remove(), this.uiButtonSet.empty(), b.isEmptyObject(d) || b.isArray(d) && !d.length ? (this.uiDialog.removeClass("ui-dialog-buttons"),
                void 0) : (b.each(d, function(c, d) {
                var e;
                d = b.isFunction(d) ? {
                    click: d,
                    text: c
                } : d;
                d = b.extend({
                    type: "button"
                }, d);
                e = d.click;
                d.click = function() {
                    e.apply(a.element[0], arguments)
                };
                c = {
                    icons: d.icons,
                    text: d.showText
                };
                delete d.icons;
                delete d.showText;
                b("\x3cbutton\x3e\x3c/button\x3e", d).button(c).appendTo(a.uiButtonSet)
            }), this.uiDialog.addClass("ui-dialog-buttons"), this.uiDialogButtonPane.appendTo(this.uiDialog), void 0)
        },
        _makeDraggable: function() {
            function a(a) {
                return {
                    position: a.position,
                    offset: a.offset
                }
            }
            var d = this,
                e = this.options;
            this.uiDialog.draggable({
                cancel: ".ui-dialog-content, .ui-dialog-titlebar-close",
                handle: ".ui-dialog-titlebar",
                containment: "document",
                start: function(c, e) {
                    b(this).addClass("ui-dialog-dragging");
                    d._blockFrames();
                    d._trigger("dragStart", c, a(e))
                },
                drag: function(b, c) {
                    d._trigger("drag", b, a(c))
                },
                stop: function(c, f) {
                    e.position = [f.position.left - d.document.scrollLeft(), f.position.top - d.document.scrollTop()];
                    b(this).removeClass("ui-dialog-dragging");
                    d._unblockFrames();
                    d._trigger("dragStop", c, a(f))
                }
            })
        },
        _makeResizable: function() {
            function a(a) {
                return {
                    originalPosition: a.originalPosition,
                    originalSize: a.originalSize,
                    position: a.position,
                    size: a.size
                }
            }
            var d = this,
                e = this.options,
                f = e.resizable,
                h = this.uiDialog.css("position"),
                f = "string" == typeof f ? f : "n,e,s,w,se,sw,ne,nw";
            this.uiDialog.resizable({
                cancel: ".ui-dialog-content",
                containment: "document",
                alsoResize: this.element,
                maxWidth: e.maxWidth,
                maxHeight: e.maxHeight,
                minWidth: e.minWidth,
                minHeight: this._minHeight(),
                handles: f,
                start: function(c, e) {
                    b(this).addClass("ui-dialog-resizing");
                    d._blockFrames();
                    d._trigger("resizeStart", c, a(e))
                },
                resize: function(b, c) {
                    d._trigger("resize", b, a(c))
                },
                stop: function(c, f) {
                    e.height = b(this).height();
                    e.width = b(this).width();
                    b(this).removeClass("ui-dialog-resizing");
                    d._unblockFrames();
                    d._trigger("resizeStop", c, a(f))
                }
            }).css("position", h)
        },
        _minHeight: function() {
            var a = this.options;
            return "auto" === a.height ? a.minHeight : Math.min(a.minHeight, a.height)
        },
        _position: function() {
            var a = this.uiDialog.is(":visible");
            a || this.uiDialog.show();
            this.uiDialog.position(this.options.position);
            a || this.uiDialog.hide()
        },
        _setOptions: function(c) {
            var d = this,
                e = !1,
                g = {};
            b.each(c, function(b, c) {
                d._setOption(b, c);
                b in a && (e = !0);
                b in f && (g[b] = c)
            });
            e && (this._size(), this._position());
            this.uiDialog.is(":data(ui-resizable)") && this.uiDialog.resizable("option", g)
        },
        _setOption: function(a, b) {
            var c, d, f = this.uiDialog;
            "dialogClass" === a && f.removeClass(this.options.dialogClass).addClass(b);
            "disabled" !== a && (this._super(a, b), "appendTo" === a && this.uiDialog.appendTo(this._appendTo()), "buttons" === a && this._createButtons(),
            "closeText" === a && this.uiDialogTitlebarClose.button({
                label: "" + b
            }), "draggable" === a && (c = f.is(":data(ui-draggable)"), c && !b && f.draggable("destroy"), !c && b && this._makeDraggable()), "position" === a && this._position(), "resizable" === a && (d = f.is(":data(ui-resizable)"), d && !b && f.resizable("destroy"), d && "string" == typeof b && f.resizable("option", "handles", b), d || !1 === b || this._makeResizable()), "title" === a && this._title(this.uiDialogTitlebar.find(".ui-dialog-title")))
        },
        _size: function() {
            var a, b, e, f = this.options;
            this.element.show().css({
                width: "auto",
                minHeight: 0,
                maxHeight: "none",
                height: 0
            });
            f.minWidth > f.width && (f.width = f.minWidth);
            a = this.uiDialog.css({
                height: "auto",
                width: f.width
            }).outerHeight();
            b = Math.max(0, f.minHeight - a);
            e = "number" == typeof f.maxHeight ? Math.max(0, f.maxHeight - a) : "none";
            "auto" === f.height ? this.element.css({
                minHeight: b,
                maxHeight: e,
                height: "auto"
            }) : this.element.height(Math.max(0, f.height - a));
            this.uiDialog.is(":data(ui-resizable)") && this.uiDialog.resizable("option", "minHeight", this._minHeight())
        },
        _blockFrames: function() {
            this.iframeBlocks =
                this.document.find("iframe").map(function() {
                    var a = b(this);
                    return b("\x3cdiv\x3e").css({
                        position: "absolute",
                        width: a.outerWidth(),
                        height: a.outerHeight()
                    }).appendTo(a.parent()).offset(a.offset())[0]
                })
        },
        _unblockFrames: function() {
            this.iframeBlocks && (this.iframeBlocks.remove(), delete this.iframeBlocks)
        },
        _allowInteraction: function(a) {
            return b(a.target).closest(".ui-dialog").length ? !0 : !!b(a.target).closest(".ui-datepicker").length
        },
        _createOverlay: function() {
            if (this.options.modal) {
                var a = this,
                    d = this.widgetFullName;
                b.ui.dialog.overlayInstances || this._delay(function() {
                    b.ui.dialog.overlayInstances && this.document.bind("focusin.dialog", function(c) {
                        a._allowInteraction(c) || (c.preventDefault(), b(".ui-dialog:visible:last .ui-dialog-content").data(d)._focusTabbable())
                    })
                });
                this.overlay = b("\x3cdiv\x3e").addClass("ui-widget-overlay ui-front").appendTo(this._appendTo());
                this._on(this.overlay, {
                    mousedown: "_keepFocus"
                });
                b.ui.dialog.overlayInstances++
            }
        },
        _destroyOverlay: function() {
            this.options.modal && this.overlay && (b.ui.dialog.overlayInstances--,
            b.ui.dialog.overlayInstances || this.document.unbind("focusin.dialog"), this.overlay.remove(), this.overlay = null)
        }
    });
    b.ui.dialog.overlayInstances = 0;
    !1 !== b.uiBackCompat && b.widget("ui.dialog", b.ui.dialog, {
        _position: function() {
            var a, d = this.options.position,
                e = [],
                f = [0, 0];
            d ? (("string" == typeof d || "object" == typeof d && "0" in d) && (e = d.split ? d.split(" ") : [d[0], d[1]], 1 === e.length && (e[1] = e[0]), b.each(["left", "top"], function(a, b) {
                +e[a] === e[a] && (f[a] = e[a], e[a] = b)
            }), d = {
                my: e[0] + (0 > f[0] ? f[0] : "+" + f[0]) + " " + e[1] + (0 > f[1] ?
                    f[1] : "+" + f[1]),
                at: e.join(" ")
            }), d = b.extend({}, b.ui.dialog.prototype.options.position, d)) : d = b.ui.dialog.prototype.options.position;
            (a = this.uiDialog.is(":visible")) || this.uiDialog.show();
            this.uiDialog.position(d);
            a || this.uiDialog.hide()
        }
    })
})(jQuery);